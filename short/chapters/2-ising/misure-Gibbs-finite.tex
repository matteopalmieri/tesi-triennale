\section{Misure di Gibbs su volumi finiti}

Definiamo ora un modello che possa rappresentare i fenomeni di magnetizzazione di un solido. Nella
semplificazione che adottiamo, gli elettroni saranno posizionati sui vertici di un
reticolo contenuto in $\Z^D$ e potranno avere spin $+1$ o $-1$. Dato quindi $D$
un intero positivo (la dimensione dello spazio), per ogni $l$ intero positivo e $D$ coppie di interi $(a_i, b_i)$ per $i = 1, \ldots, D$
con 
\[
	l = b_1 - a_1 = b_2 - a_2 = \ldots = b_D - a_D,
\]
definiamo ipercubo di lato $l$ il sottoinsieme $\L \subset \Z^D$ dato da 
\[
	\L = \{(n_1, \ldots, n_D) \in \Z^D: a_i \leq n_i < b_i \; \text{per ogni} \;
	1 \leq i \leq D\}.
\]

Sia quindi $\L \subset \Z^D$ un ipercubo (il volume del nostro solido). Per ogni
$j\in\L$, l'elettrone in posizione $j$ potrà
avere spin $\omega_j = \pm 1$, per cui lo spazio delle configurazioni sarà
$\Omega_\L = \{-1, +1\}^\L = \{\omega = (\omega_j)_{j \in \L}:
\omega_j = \pm 1\}$.

Per definire ora l'Hamiltoniana del sistema, fissiamo:
\begin{itemize}
	\item $h \in \R$ il valore di un campo magnetico esterno;
	\item $\tilde{\omega} = \{\tilde{\omega}_j: j \in \L^c\}$ delle 
	condizioni al bordo (ovvero scegliamo il valore dello spin di tutti gli
	elettroni al di fuori di $\L$). In questo caso $\tilde{\omega}_j \in \{-1, 0,
	1\}$ dove $\tilde{\omega}_j = 0$ si può immaginare come assenza di elettroni
	nel posto $j$.
	\item Una funzione di interazione $\J:\Z^D\to [0, +\infty)$ di cui
	richiediamo che sia sommabile
	\begin{equation*}
		\sum_{k \in \Z^D}\J(k) < +\infty
	\end{equation*}
	e tale che $\J(k) = \J(-k)$.
\end{itemize}

Possiamo quindi definire l'Hamiltoniana del sistema:
\begin{equation}\label{eq:H_definition}
	\H_{\L, h, \tilde{\omega}}(\omega) = -\frac{1}{2}\sum_{i, j \in \L}\J(i -
	j)\omega_i\omega_j - \sum_{i \in \L}\left(h + \sum_{j \in
	\L^c}\J(i - j)\tilde{\omega}_j\right)\omega_i 
\end{equation}

Definiamo adesso alcune misure di probabilità sullo spazio delle configurazioni.
Sia $\rho  = \frac{1}{2}\delta_1 + \frac{1}{2}\delta_{-1}$ la misura uniforme su
$\{-1, 1\}$ e $\pi_\L P_\rho = \bigotimes_{j\in\L}\rho$ la misura prodotto su
$\Omega_\L$, per cui $\pi_\L P_\rho({\omega}) = \frac{1}{2^{|\L|}}$ per ogni
$\omega\in\Omega_\L$. Sia invece:
\begin{equation}
	P_{\L, \beta, h, \tilde{\omega}}(\d\omega) = \frac{1}{Z}\exp{\left(-\beta\H_{\L, h,
	\tilde{\omega}}(\omega)\right)}\pi_\L P_\rho(\d\omega)
\end{equation}
la misura con densità rispetto alla misura prodotto detta \textbf{misura di
Gibbs}, dove $\beta\in (0, +\infty)$ è la \textbf{temperatura inversa} e
\begin{equation}
	Z = Z(\L, \beta, h, \tilde{\omega}) = \sum_{\omega \in \Omega_\L}
	\exp{\left(-\beta\H_{\L, h,\tilde{\omega}}(\omega)\right)}\pi_\L
	P_\rho(\{\omega\})
\end{equation}
è la costante di rinormalizzazione detta \textbf{funzione di partizione}.

Si osserva che le configurazioni $\omega$ con Hamiltoniana $\H(\omega)$ più
bassa sono più probabili rispetto alla misura di Gibbs.

Nel seguito ricorreranno alcune particolari condizioni al bordo:
$\tilde{\omega}_j = +1$ per ogni $j \in \L^c$, $\tilde{\omega}_j = -1$ per ogni
$j \in \L^c$ e $\tilde{\omega}_j = 0$ per ogni $j \in \L^c$. In questi tre casi
indicheremo l'Hamiltoniana rispettivamente con $\H_{\L, h, +}, \H_{\L, h, -}$ e
$\H_{\L, h, f}$ ($f$ sta per $\emph{free}$). Le misure di Gibbs associate
saranno indicate rispettivamente con $P_{\L, \beta, h, +}, P_{\L, \beta, h, -}$
e $P_{\L, \beta, h, f}$.

\begin{es}

	Il modello fondamentale che presenta tutti i fenomeni qualitativi che
	studieremo è il modello di Ising in dimensione 2 ($D = 2$). Esso
	è dato dalla funzione di interazione
	\[
		\J(k) =
		\begin{cases}
			J_0 & \text{se}\; |k| = 1 \\
			0 & \text{altrimenti}
		\end{cases},
	\]
	che mette in relazione solamente gli spin degli elettroni che si trovano in
	posizioni adiacenti. $J_0$ è una costante positiva reale.

	In Figura~\ref{fig:ising-mcmc} ci sono quattro simulazioni del modello di
	Ising ottenute estraendo uno stato randomicamente rispetto alla misura di
	Gibbs con il metodo Monte Carlo Markov Chain. In
	tutte il campo magnetico esterno è nullo e le condizioni al bordo sono
	positive. Si può osservare come ad alte temperature gli
	effetti delle condizioni al bordo sono poco visibili e la misura di Gibbs è più
	simile alla misura prodotto uniforme. Mentre diminuiscono le temperature
	(aumenta $\beta$) il legame tra le coppie di spin adiacenti cresce e le
	configurazioni hanno la maggior parte degli elettroni con spin positivo
	(rossi in figura). 

\end{es}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/ising-sim/img9-10.png}
		\caption{$\beta = 0.1$}
		\vspace{0.5cm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/ising-sim/img11-32.png}
		\caption{$\beta = 0.32$}
		\vspace{0.5cm}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/ising-sim/img4-40.png}
		\caption{$\beta = 0.40$}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{img/ising-sim/img12-112.png}
		\caption{$\beta = 1.12$}
	\end{subfigure}
	\caption{Simulazioni del modello di Ising su una griglia di lato 200 con
	condizioni al bordo positive a diversi valori della temperatura inversa.}
	\label{fig:ising-mcmc}
\end{figure}

Il comportamento osservabile nel modello di Ising in Figura~\ref{fig:ising-mcmc}
è in realtà presente nella maggior parte dei modelli (descritti dalla funzione
di interazione $\J$) e il resto del capitolo sarà speso per descriverlo
rigorosamente e dimostrarlo. Definiamo la quantità più importanti che
studieremo.

\begin{definition}
	Indichiamo con $S_\L(\omega) = \sum_{i\in\L}\omega_i$ lo \textbf{spin totale} e
	definiamo \textbf{magnetizzazione} $M$ il suo valore atteso rispetto alla misura di
	Gibbs:
	\begin{equation}
		M = M(\L, \beta, h, \tilde{\omega}) = \int_{\Omega_\L} S_\L(\omega)P_{\L, \beta, h,
		\tilde{\omega}}(\d\omega).
	\end{equation}
	Nel seguito saremo interessati al limite della magnetizzazione per $\L \uparrow
	\Z^D$, per cui definiamo \textbf{magnetizzazione specifica} la quantità
	\begin{equation}
		m(\beta, h) = \lim_{\L \uparrow \Z^D} \frac{1}{|\L|}M(\L, \beta, h,
		\tilde{\omega}(\L)).
	\end{equation}
\end{definition}

\begin{definition}
	Uno strumento fondamentale che utilizzeremo per analizzare le proprietà della
	magnetizzazione è l'\textbf{energia libera di Gibbs} definita da 
	\begin{equation}\label{eq:psi_def}
		\Psi(\L, \beta, h, \tilde{\omega}) = -\beta^{-1}\log{Z};
	\end{equation}
	analogamente a prima definiamo l'\textbf{energia libera specifica}:
	\begin{equation}
		\psi(\beta, h) = \lim_{\L \uparrow \Z^D} \frac{1}{|\L|}\Psi(\L, \beta,
		h, \tilde{\omega}(\L)).
	\end{equation}
\end{definition}

Nel seguito del capitolo descriveremo il comportamento della
magnetizzazione specifica $m(\beta, h)$. Possiamo osservare immediatamente che
per $h = 0$, $m(\beta, 0) = 0$ per simmetria. Inoltre, dato che l'Hamiltoniana
penalizza le configurazioni in cui gli elettroni hanno spin di segno opposto a
quello del campo magnetico esterno, non è difficile aspettarsi che $m(\beta, h)$
sia non negativo per $h > 0$ e viceversa per $h < 0$ (ed anche che $m(\beta,-h)
= -m(\beta, h)$). In effetti mostreremo che per la maggior parte delle funzioni di interazione $\J$, esiste un valore
$\beta_c > 0$ detto \textbf{temperatura inversa critica} tale per cui: 
\begin{itemize}
	\item per $0 < \beta < \beta_c$ abbiamo che $m(\beta, h)$ tende a 0 per $h$ che tende a $0$;
	\item per $\beta > \beta_c$ la magnetizzazione specifica tende ad un valore $m(\beta,
		+) > 0$ per $h$ che tende a 0 da destra. 
\end{itemize}

Questo vuol dire che accendendo un campo magnetico esterno positivo e portandolo a 0,
ad alte temperature il sistema ritorna completamente disordinato, con una
magnetizzazione media nulla. Per temperature basse nel sistema permane una
magnetizzazione media non nulla, se l'interazione tra gli spin è abbastanza
forte. Avendo comportamenti macroscopici diversi al variare della temperatura,
diciamo che il sistema ha una transizione di fase (alla temperatura critica
$\beta_c$). Chiameremo \textbf{magnetizzazione spontanea} il limite $m(\beta,
+)$. Nel grafico in Figura~\ref{fig:m(beta,h)} è riassunto il comportamento di $m(\beta, h)$ a
$\beta$ fissato al variare di $h$ (le varie curve sono isoterme, a $\beta$
costante). 

La presenza di un comportamento macroscopicamente diverso sopra una certa
temperatura critica $\beta_c$ è un importante esempio di transizione di fase e
mostrarlo, ovvero far vedere che per $\beta > \beta_c$ la magnetizzazione
spontanea $m(\beta, +)$ è positiva, sarà lo scopo di questo capitolo.

\input{chapters/2-ising/magnetizzazione-plot.tex}
