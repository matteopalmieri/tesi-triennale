\section{Proprietà della magnetizzazione e dell'energia libera}

In questa sezione vogliamo dimostrare l'esistenza dei limiti $m(\beta, h)$ e
$\psi(\beta, h)$, il loro legame e le proprietà della magnetizzazione specifica
visibili in Figura~\ref{fig:m(beta,h)} (monotonia rispetto a $h$ e $\beta$,
convessità/concavità, continuità, ecc... ). 

Si enunciano adesso alcune disuguaglianze di monotonia che ci saranno utili in
seguito. La dimostrazione di
queste viene rimandata in Appendice B. Queste disuguaglianze valgono per
Hamiltoniane leggermente più generali di quella considerata precedentemente, della forma:
\begin{equation}
	\H(\omega) = -\sum_{i, j \in \L} J_{ij}\omega_i\omega_j - \sum_{i \in
	\L}h_i\omega_i \qquad J_{ij}\in\R_{\geq 0}, h_i\in\R
\end{equation}
da cui derivano le misure di Gibbs $P(\d\omega) = \exp{(-\H(\omega))}\pi_\L
P_\rho(\d\omega)$. Nel caso in cui $\H = \H_{\L, h, \tilde{\omega}}$,
allora
\[
	J_{ij} = \frac{1}{2}\J(i - j) \qquad \text{e} \qquad 
	h_i = h + \sum_{j \in \L^c} \J(i - j)\tilde{\omega}_j.
\]
Diciamo inoltre che una funzione $f:\Omega_\L\to\R$ è nondecrescente se 
\begin{equation}
	\forall i \in \L\; \omega_i \leq \omega_i' \Longrightarrow f(\omega) \leq
	f(\omega').
\end{equation}
\begin{thm}\label{thm:ising-ineq} 
	Dato $A$ sottoinsieme non vuoto di $\L$ definiamo $\omega_A = \prod_{i\in
A}\omega_i$. Nel seguito tutti i valori attesi rispetto alla misura $P$ definita
sopra $\langle.\rangle_P$ verranno indicati semplicemente con
$\langle.\rangle$.
	\begin{itemize}
		\item GKS 1 (Griffiths, Kelly e Sherman): assumiamo che $h_i\geq 0$
		per ogni $i \in \L$. Allora per ogni $A$ sottoinsieme non vuoto di $\L$ vale che
		$\langle\omega_A\rangle\geq 0$.

		\item GKS 2: assumiamo che $h_i \geq 0$ per ogni $i \in \L$. Allora
		per ogni $A, B \in \L$ sottoinsiemi non vuoti, vale che $\langle\omega_A\omega_B\rangle - \langle\omega_A\rangle\langle\omega_B \rangle\geq 0$.  \item Percus: per ogni $i, j \in \L$ vale che $\langle \omega_i\omega_j\rangle - \langle\omega_i\rangle\langle\omega_j\rangle\geq 0$ (con $h_i\in\R$
		$\forall i \in \L$).
		\item GHS (Griffiths, Hurst e Sherman): assumiamo che $h_i \geq 0$
			per ogni $i \in \L$, allora
			\begin{equation*}
				\ev{(\omega_i - \ev{\omega_i})(\omega_j -
				\ev{\omega_j})(\omega_k - \ev{\omega_k})} \leq 0 \qquad \forall
				i, j, k \in \L.
			\end{equation*}
		\item FKG (Fortuin, Kastelyn e Ginibre): date $f, g$ due funzioni non
			decrescenti, allora hanno correlazione non negativa:
			\[
				\langle fg\rangle - \langle f\rangle\langle g\rangle \geq 0.
			\]
			Inoltre se $h_i \leq h_i'$ per ogni $i \in \L$ ed $f$ è non
			decrescente, allora
			\[
				\langle f\rangle_{\{h_i\}} \leq \langle f\rangle_{\{h_i'\}}.
			\]
	\end{itemize}
\end{thm}

Con queste disuguaglianze è possibile dimostrare facilmente alcune proprietà del
nostro sistema. Definita quindi $\H_{\L, h, \tilde{\omega}}$ come in
\eqref{eq:H_definition}, abbiamo come conseguenza la seguente proposizione.

\begin{prop}\label{prop:magn_prop}
	Sia $l\in\L$ e indichiamo con $\ev{.}$ il valore atteso rispetto
	alla misura di Gibbs $P_{\L, \beta, h, \tilde{\omega}}$. Valgono le seguenti
	disuguaglianze:
	\begin{itemize}
		\item[(i)] se $h_i\geq 0$ $\forall i \in \L$, allora $\ev{\omega_l} \geq
			0$;
		\item[(ii)] se $h_i \geq 0$ $\forall i \in \L$, allora
			$\frac{\partial}{\partial\beta}\ev{\omega_l}\geq 0$;
		\item[(iii)] se $h_i \geq 0$ $\forall i \in \L$ e $\tilde{\omega}_j\geq
			0$ $\forall j \in \L^c$, allora
			$\frac{\partial}{\partial\J(k)}\ev{\omega_l} \geq 0$;
		\item[(iv)] $\forall h_i \in \R$ vale che $\frac{\partial}{\partial h_i}\ev{\omega_l}\geq 0$;
		\item[(v)] se $h_i\geq 0$ $\forall i \in \L$, allora
			$\frac{\partial^2}{\partial h_i\partial h_j}\ev{\omega_l} \leq 0$;
	\end{itemize}
\end{prop}

\begin{proof}~\begin{itemize}
		\item[(i)] vero direttamente per la disuguaglianza GKS 1.
		\item[(ii)] osserviamo che:
			\begin{equation*}
				\frac{1}{Z}\mypdv{\beta}Z =
				\frac{1}{Z}\mypdv{\beta}\int_{\Omega_\L}
				\exp{(-\beta\H(\omega))}\pi_\L P_\rho(\d\omega) =
				\int_{\Omega_\L} -\H(\omega)P_{\L, \beta, h, \tilde{\omega}} = -
				\ev{\H{}}
			\end{equation*}
			da cui:
			\begin{gather*}
				\mypdv{\beta}\ev{\omega_l} = 
				\mypdv{\beta}\left[\int_{\Omega_\L}\omega_l\exp{(-\beta\H(\omega))}\pi_\L
				P_\rho(\d\omega)\frac{1}{Z}\right] = \\
				\int_{\Omega_\L}\omega_l\H(\omega)\exp{(-\beta\H(\omega))}\pi_\L
				P_\rho(\d\omega)\frac{1}{Z} + \\ + \left(\int_{\Omega_\L}\omega_l\exp{(-\beta\H(\omega))}\pi_\L
				P_\rho(\d\omega)\frac{1}{Z}\right)\frac{1}{Z}\mypdv{\beta}Z
				= \\
				= -\ev{\omega_l\H} + \ev{\omega_l}\ev{\H{}} = \\ 
				= \sum_{i, j \in \L}\frac{1}{2}\J(i - j)(\ev{\omega_i\omega_j\omega_l} -
				\ev{\omega_i\omega_j}\ev{\omega_l}) + \sum_{i \in
				\L}h_i(\ev{\omega_i\omega_l} - \ev{\omega_i}\ev{\omega_l}) \geq
				0.
			\end{gather*}
			dove l'ultima disuguaglianza è vera per GKS 2.
		\item[(iii)] analogamente al punto (ii) abbiamo che:
			\begin{equation*}
				\mypdv{\J(k)} \ev{\omega_l} = \frac{\beta}{2}\sum_{\substack{i, j
				\in \L,\\ i - j = k}} (\ev{\omega_l\omega_i\omega_j} -
						\ev{\omega_l}\ev{\omega_i\omega_j}) +
						\beta\sum_{\substack{i \in \L, j \in \L^c, \\ i - j =
						k}}\tilde{\omega}_j(\ev{\omega_l\omega_i} -
						\ev{\omega_l}\ev{\omega_i}) \geq 0
			\end{equation*}
			sempre per GKS 2.
		\item[(iv)] per la disuguaglianza di Percus abbiamo che:
			\begin{equation*}
				\mypdv{h_i}\ev{\omega_l} = \beta(\ev{\omega_l\omega_i} -
				\ev{\omega_l}\ev{\omega_i}) \geq 0
			\end{equation*}
		\item[(v)] infine abbiamo che, per GHS:
			\begin{gather*}
				\frac{\partial^2}{\partial h_i\partial h_j}\ev{\omega_l} = 
				\beta\mypdv{h_i}(\ev{\omega_l\omega_j} -
				\ev{\omega_l}\ev{\omega_j}) =\\
				\beta^2(\ev{\omega_i\omega_j\omega_l}
				- \ev{\omega_i}\ev{\omega_j\omega_l}
				- \ev{\omega_j}\ev{\omega_i\omega_l}
				- \ev{\omega_l}\ev{\omega_j\omega_i}
				+2\ev{\omega_i}\ev{\omega_j}\ev{\omega_l}) = \\
				\ev{(\omega_i - \ev{\omega_i})(\omega_j -
				\ev{\omega_j})(\omega_l - \ev{\omega_l})} \leq 0
			\end{gather*}
	\end{itemize}
\end{proof}

Osserviamo che la magnetizzazione è definita come $M = \ev{S_\L(\omega)} =
\sum_{l \in \L}\ev{\omega_l}$ per cui sommando su $l \in \L$ nelle
disuguaglianze appena dimostrate otteniamo le proprietà di $M$ cercate.

\begin{thm}[Proprietà della magnetizzazione]\label{thm:prop_mag}
	Consideriamo il caso di condizioni al bordo libere ($\tilde{\omega}_j = 0$
	$\forall j \in \L^c$). Vale allora che:
	\begin{itemize}
		\item come funzione del reale $h$, $M$ è dispari e nondecrescente. Se
			$h\geq 0$, allora $M$ è concava e non negativa. Inoltre $|M| = |M(\L, \beta, h, f)| \leq
			|\L|$;
		\item se $h \geq 0$, allora $M$ è una funzione nondecrescente e
			non-negativa di $\beta$ e della forza di interazione $\J(k)$ per
			ogni $k$.
	\end{itemize}
\end{thm}

\begin{proof}
	La positività, monotonia e concavità nelle diverse variabili si ottengono
	direttamente dalla proposizione precedente. Si osserva che nel caso
	particolare di condizioni al bordo libere, allora $h_i = h$ $\forall i \in
	\L$, quindi $\pdv{\ev{\omega_l}}{h_i} = \pdv{\ev{\omega_l}}{h}$. L'unica è cosa da dimostrare è
	che $M(\L, \beta, h, f) = - M(\L, \beta, -h, f)$. Tuttavia questo si ottiene
	facilemente osservando che $S_\L(\omega) = -S_\L(-\omega)$ e che $P_{\L, \beta, h, f}(\{\omega\}) = -
	P_{\L, \beta, -h, f}(\{-\omega\})$.
\end{proof}

Vogliamo ora studiare il limite di $M$ per $\L \uparrow \Z^D$. Per farlo
sfruttiamo la funzione energia libera definita in \eqref{eq:psi_def}. Assumiamo
ora il seguente teorema, la cui dimostrazione è rimandata in Appendice B.

\begin{thm}\label{thm:lim_psi}
	Esiste il limite dell'energia libera con condizioni al bordo libere, ovvero
	\begin{equation}
		\psi(\beta, h) = \lim_{\L\uparrow\Z^D}\frac{\Psi(\L, \beta, h,
		f)}{|\L|}
	\end{equation}
\end{thm}

Per le proposizioni successive avremo bisogno del seguente lemma.

\begin{lemma}\label{lemma:exp-ineq}
	Siano $f, g: X \to \R$ due funzioni definite su un qualche
	spazio $X$ e $P$ una misura di probabilità su $X$. Allora:
	\begin{equation*}
		\left|\log{\int_X e^f\d P} - \log{\int_X e^g \d P}\right| \leq
		\|f-g\|_\infty
	\end{equation*}
\end{lemma}

\begin{proof}
	Osserviamo che
	\begin{equation*}
		\exp{(g - \|f - g\|_\infty)} \leq \exp{f} \leq \exp{(g + \|f - g\|_\infty)}
	\end{equation*}
	integrando i vari membri e applicando il logaritmo si ottiene la tesi.
\end{proof}

Mostriamo ora come le condizioni al bordo non influiscano sul limite
dell'energia libera specifica ed il legame con la magnetizzazione.

\begin{prop}\label{prop:psi-1}~\begin{itemize}
		\item[(i)] Per qualunque successione di ipercubi $\L\uparrow\Z^D$ con condizioni al
			bordo $\tilde{\omega}(\L)$, esiste il limite
			$\lim_{\L\uparrow\Z^D}|\L|^{-1}\Psi(\L, \beta, h, \tilde{\omega}(\L))$ e coincide
			con il limite definito nel Teorema~\ref{thm:lim_psi}.
		\item[(ii)] $M$ e $\Psi$ sono legate dalla seguente relazione:	
		\begin{equation}\label{eq:psi-m}
				\frac{\partial}{\partial h}\Psi(\L, \beta, h, \tilde{\omega}) = - M(\L,
				\beta, h, \tilde{\omega}).
			\end{equation}
	\end{itemize}
\end{prop}

\begin{proof}~\begin{itemize}
	\item[(i)] 	Applicando il Lemma~\ref{lemma:exp-ineq} nel caso in cui $f = - \beta H_{\L, h, f}$, $g = - \beta H_{\L, h,
			\tilde{\omega}}$ e $P = \pi_\L P_\rho$, si ottiene
			\begin{equation*}
				\frac{1}{|\L|}|\Psi(\L, \beta, h, f) - \Psi(\L, \beta, h,
				\tilde{\omega})| \leq \frac{1}{|\L|} \|H_{\L, h, f} - H_{\L, h,
				\tilde{\omega}}\|_\infty.
			\end{equation*}
			Sapendo il Teorema~\ref{thm:lim_psi}, basta quindi far vedere che $\|H_{\L, h, f} - H_{\L, h,
			\tilde{\omega}}\|_\infty = o(|\L|)$. Essendo la funzione di
			interazione $\J$ sommabile, esiste $M > 0$ tale che
			$\J(k)\leq M$ per ogni $k$ in $\Z^D$ ed inoltre per ogni $\epsilon > 0$ esiste
			$N_\epsilon$ tale che
			\begin{equation*}
				\sum_{k \in \Z^D, |k| \geq N_\epsilon} \J(k) < \epsilon
			\end{equation*}
			Abbiamo quindi che:
			\begin{gather*}
				|H_{\L, h, f} - H_{\L, h, \tilde{\omega}}| = \left|\sum_{i\in\L,
				j\in\L^c} \J(i - j)\omega_i\tilde{\omega_j}\right| \\ 
				\leq \sum_{i \in \L}\sum_{\substack{j \in \L^c \\ |i-j|\geq
				N_\epsilon}}\J(i-j) + \sum_{i \in \L}\sum_{\substack{j \in \L^c
				\\ |i-j|< N_\epsilon}}\J(i-j) \leq \\
				\leq \epsilon|\L| + M|\{(i, j): i \in \L, j \in \L^c,
				|i-j|<N_\epsilon\}|.
			\end{gather*}
			\'E sufficiente quindi stimare il numero di coppie $(i, j)$
			dell'ultimo termine. Tuttavia gli $i \in \L$ che contribuiscono sono
			solo quelli compresi all'interno di un ispessimento del bordo
			dell'ipercubo largo $N_\epsilon$ e sono quindi
			in numero proporzionale a $N_\epsilon|\L|^{(D-1)/D}$. Ciascuno di questi
			contribuisce per un numero di vicini che dipende solo da
			$N_\epsilon$ e da $D$ ($\lesssim N_\epsilon^D$). L'ultimo
			termine è $o(|\L|)$ e quindi per l'arbitrarietà di $\epsilon$
			abbiamo la tesi.

		\item[(ii)] L'uguaglianza segue da questo semplice calcolo:
			\begin{gather*}
				\mypdv{h}\Psi = -\frac{1}{\beta Z}\mypdv{h}Z = \\
				-\frac{1}{\beta}\int_{\Omega_\L}
				\frac{\beta S_\L(\omega)}{Z}\exp{\left(\beta\left(\sum \J(i -
				j)\omega_i\omega_j + hS_\L(\omega)+
				\sum\J(i-j)\omega_i\tilde{\omega}_j\right)\right)}\pi_\L P_\rho(\d\omega) = -M
			\end{gather*}
	\end{itemize}
\end{proof}

L'obiettivo ora è sfruttare l'esistenza del limite di $\Psi$ per dedurre
l'esistenza del limite di $M$. Concludiamo la sezione quindi con il seguente

\begin{thm}[Limite della magnetizzazione]~\begin{itemize}
		\item[(i)] $\psi(\beta, h)$ a fissato $\beta$ è una funzione pari e concava
			di $h$, derivabile per $h \neq 0$.
		\item[(ii)] per ogni $h \neq 0$, esiste la magnetizzazione specifica 
			\[
				m(\beta, h) = \lim_{\L\uparrow\Z^D} \frac{M(\L, \beta, h,
				\tilde{\omega}(\L))}{|\L|}
			\]
			e non dipende dalle condizioni al bordo. Inoltre 
			\[
				m(\beta, h) = -\frac{\partial \psi(\beta, h)}{\partial h}.
			\]

	\end{itemize}
\end{thm}

\begin{proof}~\begin{itemize}
		\item[(i)] Dato che $Z = \int \exp{(-\beta\H)}\pi_\L P_\rho$ e che
			$\H_{\L, h, f}(\omega) = \H_{\L, -h, f}(-\omega)$, allora $Z(\L,
			\beta, h, f)$ è una funzione pari di  $h$ e quindi lo è anche
			$\Psi(\L, \beta, h, f)$. Per la concavità basta osservare che, per
			Hölder,
			\begin{gather*}
				Z(\L, \beta, \lambda h_1 + (1 - \lambda)h_2, f) = \\
				\int_{\Omega_\L} 
				\left(\exp{\left(\beta\sum\J(i-j)\omega_i\omega_j + \beta
				h_1\sum \omega_i\right)}\right)^\lambda
				\left(\exp{\left(\beta\sum\J(i-j)\omega_i\omega_j + \beta
				h_2\sum \omega_i\right)}\right)^{1 - \lambda} \leq \\
				Z(\L, \beta, h_1, f)^\lambda Z(\L, \beta, h_2, f)^{1-\lambda}
			\end{gather*}
			da cui passando al logaritmo si ottiene la concavità di $\Psi$.
			Parità e concavità vengono poi preservate nel limite puntuale
			$\psi(\beta, h)$.
		\item[(ii)] Osserviamo che, dalla \eqref{eq:psi-m},
			\begin{equation}\label{eq:psi-m-2}
				\frac{1}{|\L|}(\Psi(\L, \beta, h, f) - \Psi(\L, \beta, 0, f)) =
				- \int_0^h \frac{1}{|\L|}M(\L, \beta, s, f)\d s.
			\end{equation}
			Vogliamo passare al limite in questa equazione e consideriamo solo
			il caso $h > 0$, l'altro è analogo essendo $M$ dispari. Osserviamo che per
			la proposizione~\ref{thm:prop_mag} $M(\L, \beta, 0, f) = 0$ e che
			$|M(\L, \beta, h, f)| / |\L| \leq 1$, da cui a meno di passare ad
			una sottosuccessione $\L'\uparrow \Z^D$, esiste il limite $m(\beta,
			s)$ per ogni $s \in \Q^+$, con $m(\beta, 0) = 0$. Abbiamo tuttavia
			che per $h\geq 0$ \{$M(\L', \beta, h, f)/|\L'|\}_{\L'\uparrow\Z^D}$ è una successione di
			funzioni concave che convergono su un denso, quindi convergono
			$\forall h\geq 0$. Per convergenza dominata, passando al limite per
			$\L'\uparrow \Z^D$ nella \eqref{eq:psi-m-2},
			\begin{equation}\label{eq:psi-m-3}
				\psi(\beta, h) - \psi(\beta, 0) = -\int_0^h m(\beta, s)\d s.
			\end{equation}
			Essendo $m(\beta, h)$ concava (perché limite puntuale di concave), è
			continua per $h > 0$, quindi per $h \neq 0$ abbiamo che $m(\beta,
			h) = -\partial \psi(\beta, h)/\partial h$. Questo mostra che il
			limite $m$ non dipende dalla successione $\L'$, quindi tutta la
			sequenza $M(\L, \beta, h, f)$ converge a $m(\beta, h)$.
			Osserviamo infine che nel caso di condizioni al bordo non libere, le
			funzioni $\Psi(\L, \beta, h, \tilde{\omega}) / |\L|$ sono concave e
			differenziabili e ammettono limite $\psi(\beta, h)$ (indipendente da
			$\tilde{\omega}$ per la proposizione~\ref{prop:psi-1}), che è
			differenziabile per $h \neq 0$. Quindi per un fatto generale sulle
			funzioni convesse\footnote{
				Il limite deriva dal seguente fatto che non dimostriamo: date
				$f_n, f: \R \to \R$ convesse e differenziabili con $f_n$ che
				tendono puntualmente ad $f$, allora anche $f_n'$ convergono
				puntualmente ad $f'$. 
			}
			\begin{equation*}
				\frac{M(\L, \beta, h, \tilde{\omega})}{|\L|} = -
				\frac{1}{|\L|}\pdv{\Psi(\L, \beta, h, \tilde{\omega})}{h} \to
				-\pdv{\psi(\beta, h)}{h} = m(\beta, h) \qquad \text{per } h\neq
				0
			\end{equation*}
	\end{itemize}
\end{proof}
