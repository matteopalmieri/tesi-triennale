\chapter{Dimostrazioni di teoria delle grandi deviazioni}

\section{Principio delle contrazioni}

\begin{proof}(Principio delle contrazioni~\ref{contr-princ})
	Mostriamo che $J$ è semicontinua inferiormente e ha i sottolivelli compatti,
	quindi è una possibile rate function. Ricordando che una funzione è
	semicontinua inferiormente se e solo se ha i sottolivelli chiusi, allora è
	sufficiente mostrare che per ogni $M$ reale 
	\[
		\{y \in Y: J(y) \leq M\} = T(\{x\in X: I(x) \leq M\})
	\]
	dato che la funzione $T$ è continua e quindi manda compatti in compatti.
	Osserviamo che se $I(x) \leq M$, allora per la definizione di $J$ abbiamo
	che $J(T(x)) \leq I(x) \leq M$ e questo ci dà un'inclusione. Per l'altra
	inclusione osserviamo che se $J(y) \leq M$, allora esiste una successione
	$\{x_n\}$ tale che $T(x_n) = y$ e $I(x_n) \leq M + 1/n$. Dato che l'insieme 
	su cui $I \leq M+1$ è compatto, allora esiste una sottosuccessione $x_{n_k}$ 
	che converge ad un punto $\bar{x}$ con $I(\bar{x}) \leq M$ e $T(\bar{x}) = y$, da cui abbiamo l'altra inclusione.

	Osserviamo ora che per ogni $K \subseteq Y$ chiuso:
	\begin{gather*}
		\limsup_{n \in \N}\frac{1}{a_n}\log{R_n(K)} = 
		\limsup_{n \in \N}\frac{1}{a_n}\log{Q_n(T^{-1}(K))} \leq \\
		\leq - \inf_{x \in T^{-1}(K)} I(x) = 
		- \inf_{y \in K}\left(\inf_{x \in T^{-1}(y)}I(x)\right) = -\inf_{y \in K} J(y)
	\end{gather*}
	La disuguaglianza per gli aperti è analoga e questo conclude.
\end{proof}

\comment{
	Per dimostrare il principio di grandi deviazioni sul prodotto di spazi
	($\mathcal{X} \times \mathcal{Y}$) abbiamo bisogno di un risultato preliminare,
	comunque di per sé interessante. 

\begin{thm}\label{thm:supexp-tight}
	Siano $P_n$ misure di probabilità su uno spazio $\mathcal{X}$ che soddisfano
	un principio di grandi deviazioni con rate function $I$. Allora sono
	superesponenzialmente tese, ovvero $\forall M < +\infty$, esiste un compatto
	$K_M$ per cui
	\[
		\forall n \leq 1 \qquad P_n(K_M^c) \leq e^{-Mn}
	\]
\end{thm}

\begin{proof}
	Consideriamo il sottolivello $\tilde{K}_M = \{x: I(x) \leq M + 2\}$. Questo
	è compatto e su $\tilde{K}_M^c$ vale che $I(x) > M + 2$. Intuitivamente,
	almeno nel caso in cui $I$ sia continua, avremmo:
	\[
		\limsup_{n \to \infty}\frac{1}{n}\log{P_n(\tilde{K}_M^c)} \leq 
		\limsup_{n \to \infty}\frac{1}{n}\log{P_n(\text{Clos}(\tilde{K}_M^c))} \leq 
		- \inf_{x \in \tilde{K}_M^c}I(x) \leq - (M + 2)
	\]
	quindi per $n$ grande saremmo a posto. Bisogna allargare un po'
	$\tilde{K}_M$ per includere gli $n$ piccoli e per sistemare il fatto che $I$
	è solo semicontinua inferiormente.

	Per ogni $i \leq 1$, definiamo:
	\begin{itemize}
		\item $U_i$ un intorno di $\tilde{K}_M$ che sia unione finita di palle
			di raggio $1/i$. Allora $I(x) < - (M+2)$ su $U_i^c$, che è un
			chiuso. Quindi
			\[
				\limsup_{n \to \infty}\frac{1}{n}\log{P_n(U_i^c)} \leq -(M+2)
			\]
			per cui $\forall n \geq n_0(i)$ $P_n(U_i^c) \leq e^{-n(M+1)} \leq
			e^{-i}e^{-nM}$, dove l'ultima disuguaglianza è vera a patto di
			prendere $n_0(i) \geq i$.
		\item Per aggiustare i casi piccoli, per $j = 1, \ldots, n_0(i)$ definiamo $B_{i, j}$ compatti tali
			che:
			\[
				P_j(B_{i, j}^c) \leq e^{-i}e^{-jM}
			\]
	\end{itemize}
	Concludiamo definendo
	\[
		K_M = \cap_i[U_i \cup (\cup_j B_{i,j})]
	\]
	Abbiamo che $K_M$ è chiuso e totalmente limitato (il termine $i-$esimo
	dell'intersezione è ricopribile con un numero finito di palle di raggio
	$1/i$). Inoltre
	\[
		P_n(K_M^c) \leq \left(\sum_{i = 1}^{+\infty} e^{-i}\right)e^{-nM} \leq
		e^{-nM}
	\]

\end{proof}

\begin{proof}(LDP per lo spazio prodotto~\ref{ldp-prod})

	\textbf{Upper bound per i chiusi.} La dimostrazione utilizza
	un'argomentazione classica in teoria delle grandi deviazioni e si divide in
	tre parti: trovare un bound locale; estendere la stima ai compatti grazie a
	ricoprimenti finiti; decomporre un chiuso generico in due parti di cui una
	compatta che contiene la maggior parte della massa.

	Chiamiamo $R_n = P_n \otimes Q_n$ le misure prodotto.

	\begin{itemize}
		\item[Step 1.] Sia $z = (x, y) \in \mathcal{Z} = \mathcal{X} \times
			\mathcal{Y}$ ed $\epsilon > 0$. Dato che $I$ è semicontinua
			inferiormente, allora esiste un intorno $U_1\subseteq \mathcal{X}$ di
			$x$ in cui $I(x') \geq I(x) - \epsilon / 2$ per ogni $x' \in U_1$.
			Essendo in uno spazio metrico, esiste un ulteriore intorno $U$ di
			$x$ per cui $U \subset \bar{U} \subset U_1$. Abbiamo quindi che
			\[
				\limsup_{n \to \infty} \frac{1}{n}\log{P_n(U)} \leq 
				\limsup_{n \to \infty} \frac{1}{n}\log{P_n(\bar{U})} \leq 
				-\inf_{x' \in \bar{U}}I(x') \leq - I(x) + \frac{\epsilon}{2}
			\]
			Analogamente si può trovare un intorno analogo $V \subseteq
			\mathcal{Y}$ per cui:
			\[
				\limsup_{n \to \infty} \frac{1}{n}\log{Q_n(V)} \leq - J(y) + 
				\frac{\epsilon}{2} 
			\]
			Quindi se chiamiamo $W = U \times V$ intorno di $z$, vale che
			\[
				\limsup_{n \to \infty} \frac{1}{n}\log{P_n \otimes Q_n (U \times
				V)} \leq - (I(x) + J(y)) + \epsilon \leq -K(x, y) + \epsilon
			\]
		\item[Step 2.] Dato un compatto $K \subseteq \mathcal{Z}$ consideriamo
			un ricoprimento finito di $K$ fatto da intorni $W_1, \ldots, W_m$
			come allo Step 1. Allora:
			\[
				R_n(K) \leq \sum_{i = 1}^m R_n(W_i) \leq m\max_i R_n(W_i)
			\]
			Da cui:
			\[
				\limsup_{n \to \infty} \frac{1}{n}\log{R_n(K)} \leq 
				\limsup_{n \to \infty} \frac{1}{n}(\log{m} +
				\max_i\log{R_n(W_i)}) \leq - \inf_{x \in K} K(x, y) + \epsilon
			\]
			Per arbitrarietà di $\epsilon$:
			\[
				\limsup_{n \to \infty} \frac{1}{n}\log{R_n(K)} \leq -\inf_{x \in
				K} K(x, y)
			\]
		\item[Step 3.] Sia $C$ un chiuso e $K_M\subseteq \mathcal{X}, H_M
			\subseteq \mathcal{Y}$ compatti come nel teorema
			precedente, per cui per ogni $n \geq 1$ abbiamo $P_n(K_M^c) \leq
			e^{-Mn}$ e $Q_n(H_M^c) \leq e^{-Mn}$. Scomponiamo $C$ come:
			\[
				C = (C \cap K_M \times H_M) \cup (C \backslash K_M \times H_M) =
				D \cup A
			\]
			con $D$ compatto (che contiene la maggior parte della massa di $C$).
			Per lo Step 2. possiamo applicare l'upper bound a $D$:
			\[
				\limsup_{n \to +\infty}\frac{1}{n}\log{R_n(C)} \leq 
				\limsup_{n \to +\infty}\frac{1}{n}\max(-Mn, \log{R_n(D)}) \leq 
				\max(-\inf_{z \in C}K(z), -M)
			\]
			Per arbitrarietà di $M$, questo conclude.

	\end{itemize}

	\textbf{Lower bound per gli aperti.} Per gli aperti è più semplice e basta
	un bound locale. Dato $A$ aperto, sia $z \in A$ e $U \times V \subset A$
	intorno di $z$. Allora
	\[
		\liminf_{n \to +\infty} \frac{1}{n}\log{R_n(A)} \geq 
		\liminf_{n \to +\infty} \frac{1}{n}\log{R_n(U \times V)} \geq
		- (I(x) + J(y)) = - K(z)
	\]
	Passando al $\sup$ per $z \in A$, questo conclude.

\end{proof}
}

\comment{
\section{Principio di Laplace}

\begin{proof}(Teorema di Varadhan o LDP $\impl$ LP~\ref{varadhan})
	
	Dato $E \subseteq \mathcal{X}$, definiamo $I(E) = \inf_{x \in \mathcal{X}}
	I(x)$. La dimostrazione si basa sul fatto che una somma finita di
	esponenziali si comporta come l'esponenziale con l'esponente maggiore. 
	Mostriamo una disuguaglianza per volta
	\begin{itemize}
		\item Sia $M > 0$ tale che $|F(x)| \leq M$ per ogni $x \in \mathcal{X}$.
			Suddividiamo lo spazio $\mathcal{X}$ in un numero finito di chiusi in base
			agli insiemi di livello per $F$. In particolare dati $N > 0$ e $1 \leq
			j \leq N$ interi, definiamo:
			\[
				F_{N, j} = \left\{x \in \mathcal{X}: -M + \frac{2(j-1)M}{N} \leq F(x)
				\leq -M + \frac{2jM}{N} \right\}
			\]
			Possiamo applicare l'upper bound del principio di grandi deviazioni:
			\begin{gather*}
				\limsup_{n \to +\infty}
				\frac{1}{a_n}\log{\int_\mathcal{X}e^{a_nF(x)}Q_n(\d x)} \leq
				\limsup_{n \to +\infty}
				\frac{1}{a_n}\log{\sum_{j = 1}^N\int_{F_{N,j}}e^{a_nF(x)}Q_n(\d
				x)} \leq \\
				\leq \max_{j = 1, \ldots, N} \{-M + \frac{2jM}{N} - I(F_{N,
				j})\} \leq \max_{j = 1, \ldots, N} \sup_{x \in F_{N, j}}\{F(x) -
				I(x)\} + \frac{2M}{N} = \\
				= \sup_{x \in \mathcal{X}} \{F(x) - I(x)\} + \frac{2M}{N}
			\end{gather*}
			Per arbitrarietà di $N$, questo conclude la prima disuguaglianza.
		\item Siano $x \in \mathcal{X}$ ed $\epsilon > 0$ arbitrari e sia $G =
			\{y \in \mathcal{X}: F(y) > F(x) - \epsilon\}$ aperto. Allora per il
			lower bound del principio di grandi deviazioni:
			\begin{gather*}
				\liminf_{n\to+\infty}
				\frac{1}{a_n}\int_\mathcal{X}e^{a_nF(x)}Q_n(\d x) \geq 
				\liminf_{n\to+\infty}
				\frac{1}{a_n}\int_Ge^{a_nF(x)}Q_n(\d x) \geq \\
				\geq F(x) - \epsilon - I(G) \geq F(x) - I(x) - \epsilon
			\end{gather*}
			Per arbitrarietà di $\epsilon$ ed $x$ abbiamo la tesi.
	\end{itemize}
\end{proof}

\begin{proof}(LP $\impl$ LDP~\ref{lp-impl-ldp})
	
	\textbf{Upper bound per i chiusi.} Dato un chiuso $C$, definiamo\
	\[
		\phi(x) = \begin{cases}
			0 & \text{se}\; x \in C \\
			-\infty & \text{altrimenti}
		\end{cases}
	\]
	Allora $a_n^{-1}\log{Q_n(C)} = a_n^{-1}\log{\int \phi(x)Q_n(\d x)}$. Avendo
	il principio di Laplace come ipotesi, il nostro obiettivo è quindi
	approssimare $\phi$ con funzioni continue e limitate. Definiamo:
	\[
		F_j(x) = -j \cdot (d(x, C) \wedge 1) \qquad \text{($F_j = 0$ su $C$ e poi
		scende molto velocemente fino a $-j$)}
	\]
	Allora per ogni $j \geq 1$:
	\[
		\limsup_{n \to +\infty}\frac{1}{a_n}\log{Q_n(C)} \leq 
		\lim_{n \to +\infty}\frac{1}{a_n}\int_\mathcal{X}e^{a_n F_j(x)}Q_n(\d x) =
		\sup_{x \in X}\{F_j(x) - I(x)\}
	\]
	dove l'ultima uguaglianza è data dal principio di Laplace. Basta quindi
	mostrare che
	\[
		\inf_{j \geq 1} \sup_{x \in X}\{F_j(x) - I(x)\} \leq -I(C) \qquad
		\text{o che} \qquad I(C) \leq \sup_{j \geq 1}\inf_{x \in \mathcal{X}}\{I(x) -
		F_j(x)\}
	\]
	Il $\sup$ in $j$ nell'ultima espressione può essere sostituito con un
	limite, per monotonia. Assumiamo $I(C) < +\infty$ (il caso $I(C) = +\infty$
	è analogo) e supponiamo per assurdo $\inf_{x \in \mathcal{X}} \{I(x) -
	F_j(x)\} < I(C) - 2\epsilon$ per un certo $\epsilon > 0$ e per ogni $j \geq
	1$. Quindi in particolare esiste una successione $x_j$ tale che:
	\begin{equation}\label{ineq-xj}
		I(x_j) - F_j(x_j) < I(C) - \epsilon
	\end{equation}
	Sicuramente $x_j \in C^c$ altrimenti avrei $I(x_j) \geq I(C)$. Mostriamo che
	$d(x_j, C) \to 0$. Se così non fosse avrei $F_j(x_j) \to -\infty$ che è
	impossibile per la \eqref{ineq-xj}. Infine $I(x_j) < I(C) - \epsilon$ quindi
	per compattezza dei sottolivelli di $I$ abbiamo che $x_j \to \bar{x}$ e
	$\bar{x} \in C$ dato che $d(x_j, C) \to 0$. Ma questo è assurdo dato che
	\[
		I(x_j) < I(C) -\epsilon \leq I(\bar{x}) -\epsilon
	\]
	e $I$ è semicontinua inferiormente.

	\textbf{Lower bound per gli aperti.} Sia $A \subseteq \mathcal{X}$ un
	aperto. Se $I(A) = +\infty$, allora non c'è nulla da dimostrare. Altrimenti
	sia $x \in A$ con $I(x) < +\infty$ e sia $B(x, \delta) \subseteq A$ una
	palletta. Dato $M > I(x)$ costruiamo una funzione $F$ continua e limitata che 
	faccia 0 in $x$, $-M$ fuori da $B(x, \delta)$ e $-M \leq F \leq 0$ in $B(x,
	\delta)$. Un esempio è:
	\[
		F(y) = -M\left(\frac{d(x, y)}{\delta}\wedge 1\right)
	\]
	Osserviamo adesso che:
	\[
		\int_{x \in \mathcal{X}}\exp{(nF(x))}Q_n(\d x) \leq
		Q_n(B(x, \delta)) + e^{-nM}
	\]
	Passando al limite da entrambi i lati si ottiene:
	\begin{gather*}
		\max{\left(-M, \liminf_{n \to
		\infty}\frac{1}{n}\log{Q_n(B(x,\delta))}\right)} \geq 
		\lim_{n \to \infty} \frac{1}{n}\log{\int_{y \in
		\mathcal{X}}e^{nF(y)}Q_n(\d y)} = \\
		= \sup_{y \in \mathcal{X}}\{F(y) - I(y)\} \geq F(x) - I(x) = -I(x)
	\end{gather*}
	Ma essendo $M > I(x)$, allora
	\[
		\liminf_{n \to \infty}\frac{1}{n}\log{Q_n(B(x,\delta))} \geq -I(x)
	\]
	Da cui
	\[
		\liminf_{n \to \infty}\frac{1}{n}\log{Q_n(A)} \geq -I(x)
	\]
	Per arbitrarietà di $x \in A$, questo conclude. 
\end{proof}
}

\section{Legge dei grandi numeri}

In questa sezione vogliamo dare una traccia di dimostrazione del
Teorema~\ref{thm:lln}, che è una riformulazione della Legge dei Grandi Numeri.
Non dimostreremo invece il Teorema~\ref{thm:prokh-metric} sulla distanza di
Prohorov. \'E possibile trovare una dimostrazione ad esempio in \cite{stroock}
(Teorema 9.1.11).

Per la dimostrazione abbiamo bisogno di due lemmi preliminari che forniscono
condizioni equivalenti alla convergenza debole. Non dimostriamo il primo dei
due, essendo un risultato classico (si può trovare ad esempio nel Teorema 9.1.5
in \cite{stroock}).

\begin{lemma}[Portmanteau theorem]\label{lemma:portmanteau}

	Siano $\mathcal{X}$ uno spazio metrico completo e separabile, $\{\mu_n\}_{n
	\in \N}$ una successione di misure di probabilità su $\mathcal{X}$ e $\mu$
	una misura di probabilità su $\mathcal{X}$. Sono allora fatti equivalenti:
	\begin{itemize}
		\item $\mu_n$ converge debolmente a $\mu$ ($\mu_n \Rightarrow \mu$);
		\item per ogni funzione reale $f$ limitata e uniformemente continua
			\[
				\int f \d \mu_n \to \int f \d \mu;
			\]
		\item per ogni $A \subseteq \mathcal{X}$ aperto $\liminf_{n \to \infty}
			\mu_n(A) \geq \mu(A)$;
		\item per ogni $C \subseteq \mathcal{X}$ chiuso $\limsup_{n \to \infty}
			\mu_n(C) \leq \mu(C)$;
		\item $\lim_{n \to \infty} \mu_n(A) = \mu(A)$ per ogni $A$ tale che
			$\mu(\partial A) = 0$.
	\end{itemize}

\end{lemma}

Osserviamo che in effetti non è necessario controllare la convergenza delle
misure $\mu_n$ su tutta la classe dei boreliani, ma grazie al seguente lemma è
possibile conoscere il comportamento della successione su un numero limitato di
sottoinsiemi. 

\begin{lemma}\label{lemma:weak-conv}

	Siano $\mathcal{X}, \{\mu_n\}_{n \in \N}$ e $\mu$ come sopra. Sia $\mathcal{A} \subseteq \mathcal{B}(\mathcal{X})$ una sottofamiglia dei
	boreliani di $\mathcal{X}$ chiusa per intersezione e tale che ogni aperto di
	$\mathcal{X}$ è unione numerabile di insiemi in $\mathcal{A}$. Supponiamo
	che per ogni $A \in \mathcal{A}$ si abbia $\mu_n(A) \to \mu(A)$. Allora
	$\mu_n$ converge debolmente e $\mu$.

\end{lemma}

\begin{proof}

	Dati $A_1, \ldots, A_m$ elementi di $\mathcal{A}$, allora 
	\[
		\lim_{n \to +\infty} \mu_n\left(\bigcup_{k = 1}^m A_k\right) = \mu\left(\bigcup_{k
		= 1}^m A_k\right).
	\]
	Per $m = 2$ infatti basta osservare che
	\begin{align*}
		\lim_{n \to +\infty} \mu_n(A_1 \cup A_2) & = \lim_{n \to +\infty} 
		(\mu_n(A_1) + \mu_n(A_2) - \mu_n(A_1 \cap A_2)) =  \\
												 & = \mu(A_1) + \mu(A_2) -
												 \mu(A_1 \cap A_2) = \mu(A_1
												 \cup A_2),
	\end{align*}
	dove abbiamo utilizzato che $\mathcal{A}$ è chiusa per intersezione. La
	tesi per $m$ generico segue per induzione.

	Per il Lemma~\ref{lemma:portmanteau}, la tesi è equivalente a mostrare che
	per ogni $A \subseteq \mathcal{X}$ aperto, si ha 
	\[
		\liminf_{n \to +\infty} \mu_n(A) \geq \mu(A).
	\]
	Preso un $A$ aperto, siano $\{A_m\}_{m \in \N}$ elementi di $\mathcal{A}$ la
	cui unione sia $A$. Definiamo
	\[
		B_m = \bigcup_{k = 0}^m A_k
	\]
	e osserviamo che, per quanto detto sopra, $\lim_n \mu_n(B_m) = \mu(B_m)$ per
	ogni $m$ naturale e che $B_m \uparrow A$. Concludiamo osservando che
	\[
		\mu_n(B_m) \leq \mu_n(A)
	\]
	da cui 
	\[
		\mu(B_m) = \lim_{n \to +\infty} \mu_n(B_m) \leq \liminf_{n \to +\infty}
		\mu_n(A).
	\]
	Passando al limite per $m$ che tende a infinito nell'ultima disuguaglianza si ha la tesi.

\end{proof}

Richiamiamo infine la classica legge dei grandi numeri.

\begin{thm}[Legge Forte dei Grandi Numeri (SLLN), Kolmogorov]\label{thm:slln}
	
	Sia $\{X_n\}_{n \in \N}$ una successione di variabili aleatorie reali i.i.d\
	definite su un comune spazio di probabilità $(\Omega, \mathcal{F},
	\mathbb{P})$ e dotate di momento primo. Sia $m = \mathbb{E}[X_n]$. Allora
	\[
		\lim_{n \to +\infty} \frac{X_1(\omega) + \cdots + X_n(\omega)}{n} = \mu
		\qquad \text{per quasi ogni}\; \omega \in \Omega
	\]

\end{thm}

Possiamo quindi dimostrare la riformulazione della Legge dei Grandi Numeri data
dal Teorema~\ref{thm:lln}.

\begin{proof}(Teorema~\ref{thm:lln})

	Sia $E \subseteq \mathcal{X}$ un boreliano e definiamo la successione di
	variabili aleatorie reali $Y_n = \mathbb{1}_E(X_n)$. Le $Y_n$ sono i.i.d\ e
	limitate, quindi in particolare ammettono momento primo
	\[
		m_E = \mathbb{E}[Y_n] = \mathbb{P}[X_n \in E] = \rho(E)
	\]
	dove ricordiamo che $\rho$ è la legge delle $X_n$. Per la Legge Forte dei
	Grandi Numeri (Teorema~\ref{thm:slln}), esiste un trascurabile $N_E$ tale
	per cui per ogni $\omega \in \Omega \backslash N_E$ 
	\[
		\lim_{n \to +\infty} \frac{1}{n} \sum_{k = 1}^n Y_k(\omega) = \rho(E).
	\]
	Osserviamo inoltre che
	\[
		\frac{1}{n}\sum_{k = 1}^n Y_k(\omega) = \frac{1}{n}\sum_{k = 1}^n
		\delta_{X_k(\omega)}(E).
	\]
	Ricordiamo che la tesi richiede di mostrare che per quasi ogni $\omega \in
	\Omega$
	\[
		\frac{1}{n}\sum_{k = 1}^n \delta_{X_k(\omega)} \Rightarrow \rho.
	\]
	Per il Lemma~\ref{lemma:portmanteau} e il Lemma~\ref{lemma:weak-conv} è
	sufficiente mostrare che esiste un insieme trascurabile $N \subset \Omega$
	globale ed una sottoclasse dei boreliani $\mathcal{A}$ come nel
	Lemma~\ref{lemma:weak-conv} tale per cui
	\[
		\frac{1}{n}\sum_{k = 1}^n \delta_{X_k(\omega)}(A) \to \rho(A) \qquad
		\text{per ogni}\; A \in \mathcal{A}\; , \omega \in \Omega \backslash N.
	\]
	Per quanto detto sopra, questa convergenza è vera per una qualunque classe
	$\mathcal{A}$, prendendo $N = \cup_{A \in \mathcal{A}} N_A$ con $N_A$
	definiti sopra. Affinché $N$ resti trascurabile (e misurabile) è sufficiente
	che la classe $\mathcal{A}$ sia numerabile. Ci siamo quindi ridotti a
	dimostrare che esiste una classe di boreliani numerabile, chiusa per
	intersezione e tale che ogni aperto si possa scrivere come unione
	(eventualmente numerabile) di insiemi in questa classe. Sia $D \subset
	\mathcal{X}$ un denso numerabile e definiamo
	\[
		\tilde{\mathcal{A}} = \{B(x, 1/n): x \in D, n \in \N^+\}.
	\]
	La classe $\tilde{\mathcal{A}}$ è numerabile ed ogni aperto si scrive come
	unione di elementi in $\tilde{\mathcal{A}}$, ma non è chiusa per
	intersezione. Completiamola definendo
	\[
		\mathcal{A} = \{\text{intersezioni finite di elementi di}\;
		\tilde{\mathcal{A}}\}.
	\]
	$\mathcal{A}$ ha tutte le proprietà richieste (in particolare è ancora
	numerabile) e questo conclude.

\end{proof}

\section{Teorema di Sanov}

\comment{
{\color{red} La dimostrazione è nel caso di spazio di arrivo finito. Per la
dimostrazione nel caso generale vedere "Large Deviations", Deutschel, Jean-Dominique ; Stroock, Daniel W.}
\begin{lemma}
	Dato $n \in \N$ e $\textbf{k} = (k_1, \ldots, k_m)$ con $\sum_{i=1}^m k_i =
	n$, sia $C(n, \textbf{k}) = n!/(k_1!\cdots k_m!)$ il coefficiente multinomiale. Allora
	\begin{equation}
		\frac{1}{n}\log{C(n, \textbf{k})} = 
		-\sum_{i = 1}^n\frac{k_i}{n}\log{\frac{k_i}{n}} + 
		O\left(\frac{\log{n}}{n}\right)
	\end{equation}
\end{lemma}

\begin{proof}
	Utilizziamo la stima di Stirling $\log{n!} = n\log{n} - n + O(\log{n})$.
	Allora
	\begin{gather*}
		\log{C(n, \textbf{k})} = \log{n!} - \sum_{i = 1}^m \log{k_i!} = \\
		n\log{n} - n - \sum_{i=1}^m(k_i\log{k_i}-k_i) + O(\log{n}) = \\
		\sum_{i = 1}^m k_i(\log{n} - \log{k_i}) + O(\log{n}) = 
		- \sum_{i = 1}^m k_i \log{\frac{k_i}{n}} + O(\log{n})
	\end{gather*}
	Dividendo per $n$ si ottiene la tesi.
\end{proof}

\begin{proof}(Sanov~\ref{sanov})
	Fissiamo $m \geq 1$ la cardinalità di $\Gamma$. Utilizzando 
	l'identificazione \eqref{eq:simpl}, dato $n \geq 1$ e
	$\textbf{k} = (k_1, \ldots, k_m)$ con $\sum_{i = 1}^m k_i$, definiamo
	$\mu_{\frac{\textbf{k}}{n}} = \phi^{-1}(\textbf{k}/n)$. Definiamo inoltre
	\[
		\mathcal{R}_n = \left\{\mu_{\frac{\textbf{k}}{n}} |\textbf{k} = (k_1, \ldots,
			k_m), k_i \geq 0, \sum_{i = 1}^m k_i = n\right\}
	\] 
	Osserviamo che per ogni $n \geq 1$ e per ogni $\omega \in \Omega$,
	$L_n(\omega, \cdot) \in \mathcal{R}_n$. Se inoltre definiamo $\rho_i =
	\P(X_1 = x_i)$, allora $\P(L_n(\omega, \cdot) = \mu_{\frac{\textbf{k}}{n}})
	= C(n, \textbf{k})\rho_1^{k_1}\cdots \rho_m^{k_m}$. Ma allora:
	\begin{gather*}
		\frac{1}{n}\log{\P(L_n(\omega, \cdot) = \mu_{\frac{\textbf{k}}{n}})} = 
		-\sum_{i = 1}^m \frac{k_i}{n}\left(\log{\frac{k_i}{n}} -
		\log{\rho_i}\right) + O\left(\frac{\log{n}}{n}\right) = \\ = 
		- D_{KL}\left(\mu_{\frac{\textbf{k}}{n}} || \rho\right) + O\left(\frac{\log{n}}{n}\right)
	\end{gather*}
	Ma allora dato $E \subseteq \mathcal{M}(\Gamma)$
	\[
		\frac{1}{n}\log{Q_n^{(2)}(E)} = \sum_{\mu \in \phi^{-1}(\mathcal{R}_n)\cap E}
		\left(- D_{KL}(\mu || \rho) + O\left(\frac{\log{n}}{n}\right) \right)
	\]
	Da qua è semplice ottenere le due disuguaglianza del principio di grandi
	deviazioni. Infatti dato che $|\mathcal{R}_n| = (n + 1)^m$, allora per ogni
	$C \subseteq \mathcal{M}(\Gamma)$ chiuso, 
	\begin{gather*}
		\frac{1}{n}\log{Q_n^{(2)}(C)} \leq \frac{\log{(n+1)^m}}{n} + \max_{\mu \in
		\phi^{-1}(\mathcal{R}_n)\cap C}{- D_{KL}(\mu || \rho) + 
		O\left(\frac{\log{n}}{n}\right)} \leq \\ \leq - \min_{\mu \in
		C}{I_\rho^{(2)}(\mu)} + O\left(\frac{\log{n}}{n}\right)
	\end{gather*}
	e passando al $\limsup$ si ottiene la prima disuguaglianza. Se invece $A
	\subseteq \mathcal{M}(\Gamma)$ è aperto:
	\[
		\frac{1}{n}\log{Q_n^{(2)}(A)} \geq - \min_{\mu \in
		\phi^{-1}(\mathcal{R}_n)\cap C}{- D_{KL}(\mu || \rho) + 
		O\left(\frac{\log{n}}{n}\right)} \to - \inf_{\mu \in A} I_\rho^{(2)}(\mu)
	\]
	Dove nell'ultimo passaggio al limite si è utilizzato che $\mathcal{R}_n$
	diventa sempre più denso in $\Delta$ e che $I_\rho^{(2)}$ è continua.
	Passando al $\liminf$ si ottiene la seconda disuguaglianza e questo
	conclude.
\end{proof}

{\color{red} ---------------------------------------------------------------}
}

Per mostrare il teorema di Sanov ci servirà un lemma preliminare che fornisce
una caratterizzazione variazionale della divergenza di Kullback-Leibler.

\begin{lemma}[Formula variazionale di Donsker-Varadhan]\label{thm:DonskVar}
	Siano $\mu, \nu$ due misure su uno spazio di probabilità $(\mathcal{X},
	\mathcal{B})$, $B(\mathcal{X})$ le funzioni misurabili limitate su
	$\mathcal{X}$ e $D_{KL}$ la divergenza di Kullback-Leibler. Allora vale che
	\[
		D_{KL}(\mu \| \nu) = \sup_{f \in B(\mathcal{X})}
		\left\{\int_{\mathcal{X}}f(x) \mu(\d x) -
		\log{\int_{\mathcal{X}}e^{f(x)}\nu(\d x)} \right\}
	\]
\end{lemma}

\begin{proof}

	Mostriamo intanto che se il sup è finito allora $\mu \ll \nu$. Assumendo
	infatti che per ogni $f$ limitata si abbia $\int f(x) \d \mu - \log \int
	e^{f(x)} \d \nu \leq C < +\infty$, ponendo $f = \lambda \mathbb{1}_A$ abbiamo
	che
	\[
		\lambda \mu(A) \leq C + \log{(e^\lambda \nu(A) + (1 - \nu(A))}
	\]
	Ponendo $\lambda = \log{(1 / \nu(A))}$, si ha
	\begin{equation}\label{dkl-ineq}
		\mu(A) \leq \frac{C + 2}{\log{\frac{1}{\nu(A)}}}
	\end{equation}
	che mostra che $\nu(A) = 0$ implica $\mu(A) = 0$. Assumiamo quindi $\mu \ll \nu$ e denotiamo con $g(x) = \frac{\d \mu}{\d \nu}$ la
	densità di una rispetto all'altra.

	Osserviamo che le funzioni $\phi(x) = x\log{x} - x$ e $\psi(x) = e^x$ sono una la trasformata di Legendre dell'altra, ovvero
	\[
		\phi(x) = \sup_y\{xy - \psi(y)\} \qquad \psi(y) = \sup_{x}\{xy - \phi(y)\}
	\]
	Ne discende che, per ogni $f \in B(\mathcal{X})$, 
	\begin{align}
		\int f(x)\mu(\d x) & = \int f(x) g(x) \nu(\d x) \leq \int (\phi(g(x)) + \psi(f(x)))\nu(\d x)\\
						   & = D_{KL}(\mu \| \nu) - 1 + \int e^{f(x)}\nu(\d x) 
	\end{align}
	Sostituendo quindi $f(x)$ con $f(x) + c$ e ottimizzando in $c$ si ottiene
	\begin{gather*}
		\int f(x) \mu(\d x) \leq \inf_{c \in \R}\{-c-1 + D_{KL}(\mu \| \nu) + \int e^{f(x) +
		c}\nu(\d x)\} = \\
		= D_{KL}(\mu \| \nu) + \log{\int e^{f(x)}\nu(\d x)}
	\end{gather*}
	Da cui
	\[
		D_{KL}(\mu \| \nu) \geq \sup_{f \in B(\mathcal{X}} \left\{\int_{\mathcal{X}}f(x) \mu(\d x) -
		\log{\int_{\mathcal{X}}e^{f(x)}\nu(\d x)}\right\}
	\]
	Per mostrare l'altra disuguaglianza, osserviamo che sostituendo $f(x) = 
	\log{g(x)}$ si ha
	\[
		\int f(x)\mu(\d x) - \log \int e^{f(x)} \nu(\d x) = D_{KL}(\mu \| \nu)
	\]
	Questa sostituzione potrebbe non essere permessa se $\log g(x)$ non è
	limitata. Tuttavia chiamando $A_n = \{x: |\log{g(x)}| \leq n\}$ e
	considerando $f_n(x) = \log{g(x)} \mathbb{1}_{A_n}(x)$ si ha che 
	\[
		\int f_n(x) \mu(\d x) - \log{\int e^{f_n(x)} \nu(\d x)} \to D_{KL}(\mu
		\| \nu)
	\]
	Questo mostra la seconda disuguaglianza.

\end{proof}

Possiamo quindi dimostrare il teorema di Sanov.

\begin{proof}

	La convessità di $I(\nu)$ è diretta conseguenza della convessità di $t
	\log{t}$, da cui segue anche la semicontinuità inferiore (la funzione
	$I(\nu)$ è semicontinua inferiormente rispetto alla convergenza forte delle
	misure).
	Mostriamo quindi che i sottolivelli 
	\[
		D_l = \{\nu: I(\nu) \leq l\}
	\]
	sono relativamente compatti. Per il teorema di Prohorov è sufficiente
	mostrare che per ogni $\epsilon > 0$, esiste un compatto $K_\epsilon
	\subseteq \mathcal{X}$ tale che per ogni $\nu$ in $D_l$ abbiamo
	$\nu(K_\epsilon^c) \leq \epsilon$. Fissato $\epsilon$, scegliamo
	$K_\epsilon$ in modo che 
	\[
		\alpha(K_\epsilon^c) \leq e^{-\frac{l+2}{\epsilon}}
	\]
	Per quanto visto nel lemma precedente, vale che
	\[
		\beta(K_\epsilon^c) \leq \frac{l +
		2}{\log{\frac{1}{\alpha(K_\epsilon^c)}}} \leq \epsilon
	\]
	Da cui segue la relativa compattezza.

	Per mostrare le due disuguaglianze seguiamo lo stesso schema proposto nella
	dimostrazione della LDP per lo spazio prodotto.

	\textbf{Upper bound per i chiusi.} Iniziamo con una stima locale. Fissiamo
	$\nu$ ed $\epsilon > 0$ e mostriamo che esiste un intorno $U_\nu$ di $\nu$
	tale che
	\[
		\limsup_{n \in N} \frac{1}{n} \log{P_n(U_\nu)} \leq - I(\nu) + 2 \epsilon
	\]
	Prendiamo intanto $f \in B(\mathcal{X})$ tale che $\int f(x) \nu(\d x) -
	\log{\int e^{f(x)} \rho(\d x)} \geq I(\nu) - \epsilon$. Allora
	\[
		\mathbb{E}\left[\exp{n\int f(x) L_n(\d x)}\right] = 
		\mathbb{E}\left[\exp{\sum_{i = 1}^n f(X_i)}\right] = 
		\left(\int f(x) \rho(\d x)\right)^n
	\]
	Definiamo quindi
	\[
		U_\nu = \left\{\mu: \left|\int f(x) \mu(\d x) - \int f(x) \nu(\d x)\right|
		< \epsilon\right\}
	\]
	e otteniamo che
	\begin{gather*}
		\limsup_{n \in \N} \frac{1}{n}\log{P_n(U_\nu)} = 
		\limsup_{n \in \N} \frac{1}{n} \log{\mathbb{P}\left[\left|
				\int f(x) L_n(\d x) - \int f(x) \nu(\d x) \right| <
		\epsilon\right]} \leq \\
		\leq \limsup_{n \in \N} \frac{1}{n} \log{\mathbb{P}\left[
				\int f(x) L_n(\d x) - \int f(x) \nu(\d x)  >
		- \epsilon\right]} = \\
		= \limsup_{n \in \N} \frac{1}{n} \log{\mathbb{P}\left[
				\exp{n\left(\int f(x) L_n(\d x) - \int f(x) \nu(\d x)\right)}  >
		\exp(-n\epsilon)\right]} \overset{(*)}{\leq} \\
		\leq \epsilon - \int f(x) \nu(\d x) +
		\frac{1}{n}\log{\mathbb{E}\left[\exp{n\int f(x) L_n(\d x)}\right]} = \\
		= \epsilon - \int f(x) \nu(\d x) + \int e^{f(x)}\rho(\d x) \leq -I(\nu)
		+ 2\epsilon\\
	\end{gather*}
	Dove in $(*)$ si è applicata la disuguaglianza di Markov. Utilizzando un
	numero finito di questi intorni per ricoprire un generico compatto $K$ si
	ottiene (come nella dimostrazione per lo spazio prodotto) che
	\[
		\limsup_{n \in \N} \frac{1}{n}\log{P_n(K)} \leq -\inf_{\nu \in K} I(\nu)
		+ 2 \epsilon
	\]
	e per arbitrarietà di $\epsilon$ abbiamo ottenuto la tesi per i compatti.
	Per passare ai chiusi è sufficiente mostrare che per ogni $l < +\infty$
	esiste $K_l$ compatto tale che
	\[
		\limsup_{n \in \N} \frac{1}{n}\log{P_n(K_l^c)} \leq -l
	\]
	Ovvero che la famiglia $\{P_n\}_{n \in \N}$ è superesponenzialmente tesa
	(cosa che abbiamo visto essere necessaria per soddisfare una LDP nel
	Teorema~\ref{thm:supexp-tight}).
	Scomponendo quindi ogni chiuso $C$ come $(K_l\cap C) \cup (K_l^c \cap C)$ si
	ottiene
	\[
		\limsup_{n \in \N} \frac{1}{n} \log{P_n(C)} \leq \max{\{-\inf_{\nu \in C}
		I(\nu), -l\}}
	\]
	e prendendo $l$ abbastanza grande si ottiene la tesi anche per i chiusi.

	Procediamo quindi alla dimostrazione dell'esistenza di un tale $K_l$ per
	ogni $l$. Consideriamo tre sequenze di reali positivi $\epsilon_j, \delta_j$ e
	$\theta_j$ (per i quali imporremo successivamente che $\epsilon_j\downarrow
	0, \delta_j \downarrow 0$ e $\theta_j \uparrow +\infty$). Scegliamo intanto
	una successione di compatti $H_j$ tali che $\rho(H_j) \leq \epsilon_j$ e
	scegliamo 
	\[
		K_l = \{\nu: \nu(H_j^c) \leq \delta_j \; \text{per ogni} \; j \geq 1\}
	\]
	Con questa scelta $K_l$ è compatto. Otteniamo che
	\begin{gather}
		P_n(K_l^c) \leq \sum_{j = 1}^{+\infty} \P[L_n(H_j^c) > \delta_j] = \\
				   = \sum_{j = 1}^{+\infty}\P\left[\sum_{i = 1}^n
				   \delta_{X_i}(H_j^c) > n \delta_j\right] = 
				   \sum_{j = 1}^{+\infty}\P\left[\exp\left(\theta_j\sum_{i = 1}^n
				   \delta_{X_i}(H_j^c)\right) > \exp(n \theta_j\delta_j)\right]
				   \leq \\
				   \leq \sum_{j = 1}^{+\infty}\exp(- n \theta_j\delta_j)
				   \mathbb{E}\left[\prod_{i =
					   1}^n\exp\left(\theta_j\delta_{X_i}(H_j^c)\right)\right]
					   \leq \sum_{j = 1}^{+\infty} [\epsilon_je^{\theta_j} + (1 -
					   \epsilon_j)]e^{-n\theta_j\delta_j}
	\end{gather}
	Scegliendo ad esempio $\delta_j = \frac{1}{j}, \theta_j = j(l + j +
	\log{2}), \epsilon_j = e^{-\theta_j}$, si ottiene $P_n(K_l^c) \leq e^{-nl}$,
	come voluto.

	\textbf{Lower bound per gli aperti.} Per mostrare il lower bound, sia
	$U_\nu$ un intorno di $\nu$ e $g(x) = \frac{\d \nu}{\d \rho}$. Osserviamo
	che 
	\[
		P_n(U_\nu) = \int_{L_n \in U_\nu}\rho(x_1)\cdots \rho(x_n) \geq
		\int_{L_n \in U_\nu} [g(x_1)\cdots g(x_n)]^{-1}\nu(x_1) \cdots \nu(x_n)
	\]
	Dove avremo l'uguaglianza nel caso in cui l'insieme $\{g = 0\}$ abbia misura
	nulla per $\rho$. Definiamo 
	\[
		A_n = \left\{(x_1, \ldots, x_n): L_n \in U_\nu, \left|\int \log{g(x)}L_n(\d x) -
		D_{KL}(\mu \| \rho)\right| < \epsilon\right\}
	\]
	Allora per $(x_1, \ldots, x_n) \in A_n$ abbiamo che 
	\[
		\frac{\log{g(x_1)} +\cdots+ \log{g(x_n)}}{n} - D_{KL}(\mu \| \rho)
		\leq \epsilon
	\]
	Ovvero $g(x_1)\cdots g(x_n) \leq \exp{n(D_{KL}(\mu \| \rho) + \epsilon)}$.
	Abbiamo quindi che
	\begin{gather*}
		P_n(U_\nu) \geq \int_{A_n}[g(x_1)\cdots g(x_n)]^{-1}\mu(\d x_1)\cdots
		\mu(\d x_n) \\
		\geq \exp{[-n(D_{KL}(\mu \| \rho) + \epsilon)]}\int_{A_n} \mu(\d x_1)\cdots
		\mu(\d x_n) = \\
		= \exp{[-n(D_{KL}(\mu \| \rho) + \epsilon)]}(1 - o(1))
	\end{gather*}
	Dove l'ultima uguaglianza è vera per la legge dei grandi numeri
	(Teorema~\ref{thm:lln}) essendo $A_n$ un intorno di $\mu$. Prendendo il
	logaritmo e passando al limite si ottiene il lower bound.

\end{proof}

\begin{proof}(Sanov condizionato~\ref{sanov-cond})
	Osserviamo che, dato che la parte interna di $E$ è non vuota, allora
	\[
		\liminf_{n \to +\infty} \frac{1}{n}\log{\P(L_n \in E)} \geq 
		\liminf_{n \to +\infty} \frac{1}{n}\log{\P(L_n \in \overset{\circ}{E})} \geq 
		- \inf_{x \in \overset{\circ}{E}} I(x) = - \inf_{x \in E}I(x) > - \infty
	\]
	quindi $\P(L_n \in E)$ è definitivamente non nulla per cui per $n$ sufficientemente
	grande $Q_n^E$ è ben definita. Verifichiamo quindi l'upper bound.

	Sia $C\subseteq \mathcal{M}(\mathcal{X})$ un chiuso, allora $C \cap E$ è chiuso
	per cui vale la seguente disuguaglianza:
	\begin{gather*}
		\limsup_{n \in \N} \frac{1}{n}\log{Q_n^{E}(C)} = 
		\limsup_{n \in \N} \frac{1}{n}\log{\P(L_n \in C | L_n \in E)} = \\ =
		\limsup_{n \in \N} \frac{1}{n}\log{\P(L_n \in C\cap E)} - 
		\lim_{n \in \N} \frac{1}{n}\log{\P(L_n \in E)} \leq \\ \leq 
		- \inf_{x \in C \cap E}\left(I(x) - \inf_{x' \in E}
		I(x')\right) = -\inf_{x \in C} I^E(x)
	\end{gather*}

	\comment{
		\textbf{Lower bound per gli aperti.} Analogamente per $A \subseteq
		\mathcal{M}(\mathcal{X})$ aperto:
		\begin{gather*}
			\liminf_{n \in \N} \frac{1}{n}\log{Q_n^{E}(A)} \geq  
			\liminf_{n \in \N} \frac{1}{n}\log{\P(L_n \in A\cap \overset{\circ}{E})} - 
			\lim_{n \in \N} \frac{1}{n}\log{\P(L_n \in E)} \geq \\ \geq 
			- \inf_{x \in A \cap \overset{\circ}{E}}\left(I(x) - \inf_{x' \in E}
			I(x')\right) = -\inf_{x \in A} I^E(x)
		\end{gather*}
		dove nell'ultima uguaglianza abbiamo usato che, dato che $I$ è
		continua (nei punti in cui è finita) ed $A$ è aperto, allora 
		\[
			\inf_{x \in A\cap\overset{\circ}{E}}I(x) = \inf_{x \in A \cap E} I(x)
		\] 
	}
	Osserviamo che la funzione $I^E$ è ancora semicontinua
	inferiormente dato che è l'estensione a $\infty$ fuori da un chiuso di una
	funzione semicontinua inferiormente. Inoltre i suoi sottolivelli sono
	compatti essendo intersezione dei sottolivelli di $I(x)$ con il
	chiuso $E$. Quindi $I^E$ è una rate function ammissibile e questo conclude.
\end{proof}
	
