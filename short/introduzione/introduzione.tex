\chapter*{Introduzione}

La meccanica statistica si occupa di studiare sistemi fisici con un numero molto
elevato di particelle e di dedurne il comportamento macroscopico dalla
conoscenza di un numero limitato di grandezze. Pensiamo ad un esempio ad un gas.
Un approccio possibile per studiarlo potrebbe essere risolvere completamente le
equazioni del moto (deterministiche) per il sistema di particelle e a questo
punto si avrebbe una conoscenza completa del fenomeno: sapremmo dove si trova
ciascuna particella ad ogni istante. Questo non è l'approccio
che seguiremo per due motivi: come prima cosa questo tipo di soluzione richiede
di conoscere la posizione e la velocità iniziale di ciascuna particella, cosa
nella pratica infattibile; in secondo luogo nel tipo di problemi che
considereremo non si è quasi mai interessati a raggiungere un tale livello di
precisione, ma è sufficiente saper descrivere macroscopicamente il sistema.

Restringeremo la nostra attenzione a sistemi all'equilibrio e utilizzeremo
come base la teoria delle grandi deviazioni. Questa si occupa di studiare sistemi
non deterministici in cui gli eventi rari hanno probabilità esponenzialmente
basse di accadere. L'esempio più importante che affronteremo è il seguente:
consideriamo una sequenza di variabili aleatorie i.i.d. $X_1, X_2, \ldots$ a
valori in un certo spazio $\mathcal{X}$ con legge $\rho$. Un corollario classico
della legge dei grandi numeri è la convergenza debole (in probabilità) delle
misure empiriche alla legge $\rho$, ovvero
\[
	\frac{1}{n}\sum_{i = 1}^n \delta_{X_i}(\d x) \Rightarrow \rho(\d x).
\]
Il teorema di Sanov, nella teoria delle grandi deviazioni, mostra come questa
convergenza sia in realtà esponenziale, ovvero presa un'altra misura di
probabilità $\mu$ su $\mathcal{X}$, avremo che
\[
	\mathbb{P}\left(\frac{1}{n}\sum_{i = 1}^n \delta_{X_i} \sim \mu\right) \sim
	e^{-n D_{KL}(\mu \| \rho)},
\]
dove con $D_{KL}(\mu \| \rho)$ indichiamo l'entropia relativa o divergenza di
Kullback-Leibler di $\mu$ rispetto a $\rho$.

Per studiare sistemi di meccanica statistica accetteremo quindi di non conoscere
tutte le condizioni iniziali, che saranno quindi aleatorie, ma solo l'energia
media. Ricaveremo il comportamento tipico all'equilibrio del sistema e con la
teoria delle grandi deviazioni anche le probabilità (esponenzialmente basse) di
avere un comportamento diverso da quello tipico. 

La tesi è divisa in due capitoli.

Nel primo verranno richiamiati i fatti principali di teoria delle grandi
deviazioni, le cui dimostrazioni si trovano in Appendice. Viene inoltre
mostrato il legame con la meccanica statistica discutendo un modello di gas con
particelle non interagenti. Supponendo che l'energia media delle particelle $U$ sia
nell'intervallo $[u - \epsilon, u + \epsilon]$ con $u$ un certo valore reale
fissato, è abbastanza naturale considerare sullo spazio degli stati $\R^{6N}$ la
misura uniforme tra le configurazioni nella fetta di energia considerata
\[
	\mathbb{P}_{N,u} \propto \mathbb{1}_{\{U \in [u - \epsilon, u + \epsilon]\}}\d x_1
	\cdots \d x_{6N}.
\]
Mostreremo, attraverso il Teorema di Sanov, che per $N$ grande questa misura di
probabilità è equivalente alla \emph{misura di Gibbs}
\[
	\mathbb{P}_{N, \beta} \propto e^{-\beta U}\d x_1 \cdots \d x_{6N},
\]
dove $\beta$ è un parametro reale (legato all'energia fissata $u$). Questa
misura di probabilità è uno dei concetti più importanti di meccanica statistica
che affronteremo.

Il secondo capitolo è invece dedicato allo studio di modelli di spin su
reticoli, il più famoso dei quali è il modello di Ising (Figura~\ref{fig:ising-mcmc-intro}). Questi descrivono in
modo semplice il comportamento di solidi che, se immersi in un campo magnetico,
si magnetizzano parzialmente. Un esempio può essere una graffetta di ferro messa
in contatto con una calamita. Questa si magnetizza e rimane parzialmente
magnetizzata per un po' di tempo anche dopo l'allontanamento della calamita.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\textwidth]{img/ising-sim/img4-40.png}
	\caption{Simulazione del modello di Ising su un reticolo quadrato di lato
	200. I punti rossi sono elettroni con lo spin orientato verso l'alto, quelli
	blu con lo spin orientato verso il basso.}
	\label{fig:ising-mcmc-intro}
\end{figure}

In analogia con i gas, studieremo questi modelli rispetto alla misura di Gibbs,
sostituendo all'energia $U$ una nuova funzione $H$, detta Hamiltoniana, che
tiene conto dell'interazione tra gli elettroni. Uno dei fenomeni più interessanti che vedremo nei modelli ferromagnetici è la
transizione di fase. Pensiamo nuovamente alla graffetta: per basse temperature
avviene quanto descritto sopra e parleremo di \emph{magnetizzazione spontanea};
ad elevate temperature invece, dopo aver allontanato la calamita dalla
graffetta, prevalgono le fluttazioni caotiche all'interno della graffetta e
questa avrà magnetizzazione media nulla. In effetti la transizione da uno stato
all'altro avviene ad una certa temperatura critica $1 / \beta_c$ che dipende
solamente dal tipo di reticolo e il secondo capitolo sarà incentrato sul
caratterizzare in modi equivalenti questa temperatura critica. Nella conclusione
utilizzeremo una delle formulazioni equivalenti per mostrare che essa è
effettivamente positiva, ovvero a temperature abbastanza basse si ha
magnetizzazione spontanea. La dimostrazione, di carattere combinatorico, si deve
a Peierls (1936) e dà una stima dal basso per la temperatura critica.

Mi piacerebbe ringraziare infine il professor Flandoli e Alessandra per la splendida
matematica che mi hanno raccontato in questo piacevole anno e per avermi
dedicato più attenzione e cura di quanta potessi immaginare.
