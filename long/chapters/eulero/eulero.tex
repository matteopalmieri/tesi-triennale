\chapter{Fluidi incomprimibili in due dimensioni}

Nei prossimi capitoli cercheremo di capire qualcosa su fenomeni di turbolenza
che si manifestano nei fluidi. In particolare restringeremo la nostra attenzione
ai fluidi in due dimensioni e cercheremo di capire come mai si formino
vortici stabili osservabili sia in natura, sia attraverso simulazioni numeriche. 

Riprendiamo prima alcune considerazioni generali sulle equazioni di Eulero, che
descrivono la dinamica dei fluidi incomprimibili.

\section{Equazioni di Eulero, vorticità e stream function}

Nel seguito consideriamo un dominio aperto connesso e limitato $\Omega \subset \R^2$,
all'interno del quale avverrà il moto del nostro fluido. Chiamiamo $u(t, x)$ il
campo di velocità del fluido (che indicheremo anche con $u_t(x)$), per cui
\begin{equation}
	u:[0, T] \times \Omega \to \R^2 \qquad (t, x) \mapsto (u_1(t, x), u_2(t, x))
\end{equation}
Sia inoltre $p:[0, T] \times \Omega \to \R$ la pressione. Le equazioni di
Eulero per fluidi incomprimibili (a densità costante) sono
\begin{equation}\label{eq:euler_u}
	\begin{cases}
		\displaystyle\frac{\partial u}{\partial t} + (u \cdot \nabla)u + \nabla p = 0 \\
		\text{div}\;u = 0
	\end{cases}.
\end{equation}
Osserviamo che se $\Omega$ è semplicemente connesso (cosa che assumeremo d'ora
in avanti), dato che $\div{u} = 0$ allora esiste una funzione
$\psi:[0, T] \times \Omega \to\R$ definita a meno di costante per cui $u = (\partial_2\psi,
-\partial_1\psi)$, dove $\partial_1, \partial_2$ indicano rispettivamente la
derivata parziale rispetto alla prima e alla seconda componente. In particolare $\nabla_x \psi \perp u$, quindi $\nabla_x \psi
\parallel n$ sul bordo e allora ad un fissato tempo $\psi(\cdot, t)$ è costante
lungo $\partial \Omega$, quindi possiamo porla nulla sul $\partial\Omega$.

Chiameremo $\psi$ \textbf{stream function}. La funzione $\omega:[0,T] \times
\Omega \to \R$ definita da 
\[
	\omega = \curl{u} = \partial_1u_2 - \partial_2u_1
\]
è detta \textbf{vorticità} e verrà indicata in modo equivalente con
$\omega(t,x)$ e $\omega_t(x)$. L'equazione \eqref{eq:euler_u} può essere riscritta (ammettendo abbastanza
regolarità) in termini della vorticità usando la definizione e sommando
opportunamente le equazioni sulle due componenti della $u$, in
\begin{equation}\label{eq:euler_omega}
	\frac{\partial\omega}{\partial t} + u\cdot\nabla\omega = 0.
\end{equation}

Mostriamo adesso come questa equazione si possa vedere come un'equazione nella
sola variabile $\omega$. La stream function e la vorticità sono legate dall'equazione di Poisson
\begin{equation}\label{eq:poisson}
	\begin{cases}
		-\Delta_x \psi = \omega & \text{su}\;\Omega \\
		\psi = 0 & \text{su}\;\partial\Omega
	\end{cases}
\end{equation}

Questa equazione si può risolvere con il metodo delle funzioni di Green. In
particolare, se $\Omega \subset \R^2$ è regolare,
allora esiste una funzione di Green $V:\Omega^2 \to [0, +\infty]$
\[
	V(x, y) = -\frac{1}{2 \pi}\log{|x-y|} + \tilde{\gamma}(x, y)
\]
(con $\tilde{\gamma}$ una funzione dipendente dal dominio, simmetrica ed armonica
in entrambe le variabili) per cui vale che
\[
	\psi(t, x) = \int_\Omega V(x, y) \omega(t, y) \d y 
\]
e quindi
\[
	u(t, x) = \nabla_x^\perp \psi(t, x) = \int_\Omega K(x, y) \omega(t, y) \d y
\]
dove
\[
	K(x, y) = -\frac{1}{2\pi}\frac{(x - y)^\perp}{|x - y|^2} + \nabla_x^\perp
	\tilde{\gamma}(x, y)
\]
Siamo quindi riusciti a scrivere il campo di velocità in termini della sola
vorticità. Scriveremo la relazione tra $u$ e $\omega$ anche come $u = K \ast
\omega$.

\section{Esistenza e unicità globale}

L'obiettivo di questa sezione è mostrare un risultato di esistenza ed unicità
per ogni tempo dell'equazione di Eulero nella vorticità
(Equazione~\ref{eq:euler_omega}). Come vedremo nel
seguito, una soluzione in senso classico della~\ref{eq:euler_omega} conserva nel
tempo tutte le norme $L^p$
\[
	\int_\Omega |\omega_t(x)|^p \d x = \int_\Omega |\omega_0(x)|^p
\]
quindi in particolare partendo da un dato iniziale $\omega_0 \in
L^\infty(\Omega)$, anche la norma $L^\infty$ di $\omega_t$ sarà conservata. \'E
quindi abbastanza naturale cercare soluzioni $\omega_t \in L^\infty$, essendo il
dominio limitato. Il problema che vogliamo risolvere è quindi il seguente:
\[
	\text{\textbf{Problema 1}} \qquad 
	\begin{cases}
		\displaystyle\frac{\partial \omega}{\partial t}(x, t) + u(x, t) \cdot \nabla
		\omega(x, t) = 0 \\
		u(x, t) = \displaystyle\int_\Omega K(x, y) \omega(y, t) \d y \\
		\omega(x, 0) = \omega_0(x) \in L^{\infty}
	\end{cases}
\]

In generale tuttavia partendo da un dato iniziale solo
$L^\infty$, non è vero che esiste una soluzione abbastanza regolare da
soddisfare del Problema 1, per cui abbiamo bisogno di cercarne una forma più
debole.

Supponiamo per adesso di avere una soluzione $(\omega, u)$ regolare del Problema
1. Il campo di velocità $u$ ci permette di definire un flusso $\Phi:[0,T] \times [0,T]\times
\Omega \to \Omega$ dato dalla soluzione del problema di Cauchy:
\begin{equation}\label{eq:flux}
	\begin{cases}
		\displaystyle\frac{\d}{\d t}\Phi_{s,t}(x) = u(\Phi_{s,t}(x), t) \\
		\Phi_{s, s}(x) = x
	\end{cases}
\end{equation}
Questo flusso ci dice fisicamente (al variare di $t$) il percorso di una piccola porzione di fluido
che al tempo $s$ si trova attorno ad $x \in \Omega$ e che viene trasportata dal
campo di velocità $u$. Il flusso gode di una buona proprietà di composizione,
ovvero
\[
	\Phi_{t, u}(\Phi_{s, t}(x)) = \Phi_{s, u}(x),
\]
che deriva direttamente dall'unicità del problema di Cauchy~\ref{eq:flux}.
In particolare $\Phi_{s, t}(x)$ ha come inversa $\Phi_{t,s}(x)$.
Osserviamo adesso che $\omega_t(\Phi_{s,t}(x))$ è costante
in $t$ per ogni $x \in \Omega$ ed $s \in [0,T]$, infatti:
\begin{gather*}
	\frac{\d}{\d t}\omega(\Phi_{s, t}(x), t) = \frac{\partial \omega}{\partial
	t}(\Phi_{s, t}(x), t) + \nabla \omega(\Phi_{s, t}(x), t) \cdot \frac{\d}{\d
t}\Phi_{s, t}(x) =
	\\
	\frac{\partial \omega}{\partial t}(\Phi_{s, t}(x), t) + \nabla
	\omega(\Phi_{s, t}(x), t) \cdot u(\Phi_{s, t}(x), t) = 0
\end{gather*}
Quindi abbiamo ottenuto che una soluzione regolare del Problema 1 soddisfa il
seguente:
\[
	\text{\textbf{Problema 2}} \qquad 
	\begin{cases}
		\displaystyle\frac{\d}{\d t}\Phi_{s, t}(x) = u(\Phi_{s, t}(x), t) \\
		\Phi_{s,s}(x) = x \\
		u(x, t) = \displaystyle\int_\Omega K(x, y) \omega(y, t) \d y \\
		\omega(x, t) = \omega_0(\Phi_{t, 0}(x)) \qquad \omega_0 \in L^{\infty}
	\end{cases}
\]
Il Problema 2 ha il vantaggio, rispetto al Problema 1, di non contenere derivate
di $\omega_t$ e che quindi possa avere una possibile soluzione solo $L^\infty$.
Abbiamo quindi il seguente risultato:

\begin{thm}[Yudovich]\label{yudovich}
	Per ogni $\omega_0 \in L^\infty(\Omega)$ esiste una sola soluzione
	$(\Phi_{s,t}, \omega_t, u_t)$ nell'intervallo di tempi $[0, T]$ che soddisfa il \textbf{Problema 2}, dove $\omega \in
	L^\infty([0, T], L^\infty(\Omega))$.
\end{thm}

Prima di mostrare il teorema, osserviamo che se $\omega \in L^\infty$, non
sappiamo ancora che il campo $u = K \ast \omega$ sia sufficientemente regolare
da avere una soluzione unica e per ogni tempo della \eqref{eq:flux}. Con i
seguenti due lemmi daremo una risposta affermativa a questo problema.

\begin{lemma}
	Sia $|\Omega|$ la misura di $\Omega$. Allora esiste una costante $C > 0$
	tale che:
	\begin{gather}
		|u(x)| \leq \|K \ast \omega\|_\infty \leq C \|\omega\|_\infty \\
		\int_{\Omega} |K(x, y) - K(x', y)| \d y \leq C(1 + |\Omega|)\phi(|x - x'|)
	\end{gather}
	dove
	\[
		\phi(r) = 
		\begin{cases}
			r(1 - \log{r}) & r < 1 \\
			1 & r \geq 1
		\end{cases}
	\]
\end{lemma}

\begin{proof}
	Ricordiamo che la funzione di Green $V(x, y) = -\frac{1}{2\pi} \log{|x-y|} +
	\tilde{\gamma}(x, y)$ con $\tilde{\gamma}(x, y)$ armonica in entrambe le
	variabili. Quindi in particolare l'andamento della funzione e delle sue
	derivate è dato dal termine logaritmico, per cui valgono le seguenti stime
	\begin{gather*}
		|V(x, y)| \leq C(\log{|x - y|} + 1) \\
		|K(x, y)| \leq C|x - y|^{-1} \\
		\|\nabla K(x, y)\| \leq C|x - y|^{-2}
	\end{gather*}
	Da cui
	\[
		|u(x)| \leq \|\omega\|_\infty \int_\Omega |K(x, y)|\d y \leq
		\|\omega\|_\infty \int_\Omega \frac{C}{|x - y|}\d y \leq C' \|\omega\|_\infty
	\]
	Per quanto riguarda la seconda stima, assumiamo $r = |x - x'| < 1$ (in caso
	contrario la stima segue dalla disuguaglianza sopra). Definiamo
	\[
		A = \{y \in \Omega: |x - y| \leq 2r\}
	\]
	Vogliamo stimare
	\[
		\int_{\Omega\cap A}|K(x, y) - K(x', y)|\d y + \int_{\Omega\cap A^c}|K(x, y) - K(x', y)|\d y
	\]
	Il primo integrale si stima per disuguaglianza triangolare con
	\[
		C\int_{\Omega \cap A} \left(\frac{1}{|x - y|} + \frac{1}{|x' -
		y|}\right)\d y \leq C'\int_{|x - y| \leq 3}\frac{\d y}{|x - y|} \leq
		C''r
	\]
	Mentre per il secondo integrale osserviamo che esiste $x''$ sul segmento tra
	$x$ e $x'$ tale che 
	\begin{gather*}
		\int_{\Omega\cap A^c}|K(x, y) - K(x', y)|\d y \leq 
		Cr\int_{\Omega \cap A^c} |\nabla K(x'', y)|\d y \leq  \\
		\leq C'r \int_{D \cap A^c} \frac{\d y}{|x'' - y|^2} \leq
		C'r\left(\int_{2r < |x - y| < 2}\frac{\d y}{|x - y|^2} + 
		|\Omega|\right)
	\end{gather*}
	Dove l'ultima disuguaglianza dipende dal fatto che $|x'' - y| \geq
	\frac{1}{2}|x - y|$. Svolgendo l'ultimo integrale, si ottiene la stima.

\end{proof}

Da questo lemma segue che se $\omega \in L^\infty$, allora $u = K \ast \omega$ è
un campo uniformemente limitato e continuo in $x$, ma che non è necessariamente lipschitziano.
Infatti
\begin{equation}
	|u(x) - u(y)| = |K \ast \omega(x) - K \ast \omega(y)| \leq C
	\|\omega\|_\infty \phi(|x - y|)
\end{equation}
Un campo $u$ che soddisfa questa disuguaglianza diremo che è
\textbf{quasi-Lipschitz}. Il seguente lemma è un rafforzamento del classico teorema di
Cauchy-Lipschitz.

\begin{lemma}\label{quasi-Lip}
	Consideriamo il problema
	\begin{equation}\label{eq:quasi-Lip}
		\begin{cases}
			\displaystyle\frac{\d}{\d t}x = b(x, t) \\
			x(0) = x_0
		\end{cases}
	\end{equation}
	con $b \in C(\R^n \times [0, \infty))$ uniformemente limitato e
	quasi-Lipschitz, ovvero esiste una costante $C > 0$ indipendente dal tempo
	per cui
	\[
		|b(x, t) - b(y, t)| \leq C\phi(|x - y|)
	\]
	Allora questo problema ammette una soluzione unica per ogni tempo.
\end{lemma}

\begin{proof}
	Mostriamo l'esistenza utilizzando un classico metodo iterativo. Definiamo la
	successione di soluzioni approssimate per ricorrenza:
	\[
		\begin{cases}
			x_0(t) = x_0 \\
			x_{n+1}(t) = \displaystyle\int_0^t b(x_n(s), s)\d s
		\end{cases}
	\]

	Dimostriamo ora che per $T > 0$ abbastanza piccolo, la successione di
	funzioni $\{x_n(t)\}_{n \in \N}$ è di Cauchy rispetto alla convergenza
	uniforme sull'intervallo $[0,T]$.

	Abbiamo che
	\[
		|x_{n+1}(t) - x_n(t)| \leq \int_0^t |b(x_n(s), s) - b(x_{n-1}(s), s)|\d
		s \leq C\int_0^t \phi(|x_n(s) - x_{n-1}(s)|)\d s
	\]

	Si osserva che possiamo approssimare dall'alto $\phi$ con funzioni affini
	sempre più pendenti. In particolare prendendo la tangente a $\phi$ in
	$\epsilon$ si ottiene
	\[
		\phi(r) \leq -\log{(\epsilon)}r + \epsilon = L_\epsilon r + \epsilon
	\]

	Quindi sostituendo nella disuguaglianza sopra
	\[
		\|x_{n+1} - x_n\|_{L^\infty([0, T])} \leq CL_\epsilon \|x_n -
		x_{n-1}\|_{L^\infty([0,T])} + CT\epsilon
	\]
	
	Da cui, iterando:
	\[
		\|x_n - x_{n-1}\|_{L^\infty([0, T])} \leq 
		CT\epsilon \sum_{k = 0}^{n-2}\frac{C^k L_\epsilon^k T^k}{k!} + 
		\frac{C^{n-1}L_\epsilon^{n-1}T^{n-1}}{(n-1)!}\|x_1 - x_0\|_{L^\infty([0,
		T])}
	\]
	
	Sfruttando che il campo $b$ è uniformemente limitato, allora $\|x_1 - x_0\|
	\leq CT$, da cui
	\[
		\|x_n - x_{n-1}\|_{L^\infty([0,T])} \leq CT\epsilon
		\exp{(-CT\log{\epsilon})} + \frac{C^nL_\epsilon^{n-1}T^n}{(n-1)!}
	\]

	Scegliendo $T$ piccolo in modo che $1 - CT > \frac{1}{2}$ e $\epsilon =
	\exp{(-n)}$, otteniamo
	\[
		\|x_n - x_{n-1}\|_{L^\infty([0,T])} \leq
		CT\exp{\left(-\frac{n}{2}\right)} +
		\frac{C^nL_\epsilon^{n-1}T^n}{(n-1)!} \leq 
		CT\exp{\left(-\frac{n}{2}\right)} + (CT)^n\exp{(Cn)}
	\]

	Scegliendo $T$ ancora abbastanza piccolo, otteniamo una stima di
	$\|x_{n+1}-x_n\|_\infty$ data da una serie geometrica convergente, quindi le
	funzioni $\{x_n\}_{n \in \N}$ convergono uniformemente sull'intervallo $[0,
	T]$ e quindi il limite $x(t)$ soddisfa l'equazione in forma integrale (e
	quindi in forma differenziale per la continuità di $b$).

	Osserviamo infine che, essendo $T$ indipendente dal tempo, possiamo
	reiterare il procedimento e avere quindi esistenza globale. Lo stesso
	ragionamento dà anche l'esistenza per tempi negativi e l'unicità.

\end{proof}

Siamo pronti per dimostrare il teorema~\ref{yudovich}.
\begin{proof}(Yudovich)
	Mostriamo l'esistenza. Come prima costruiamo una serie di soluzioni
	approssimate che convergeranno in un senso opportuno alla soluzione del
	problema. Definiamo le successioni per ricorrenza
	\[
		\begin{cases}
			\omega_t^0(x) = \omega_0(x) \\
			u_t^n(x) = K \ast \omega_t^{n-1} \\
			\displaystyle\frac{\d}{\d t} \Phi_{s,t}^n(x) = u_t^n(\Phi_{s,
			t}^n(x)), \; \Phi_{s,s}^n(x) = x \\
			\omega_t^n(x) = \omega_0(\Phi_{0,t}^n(x))
		\end{cases}
	\]

	Dove $\Phi_{s,t}^n$ è ben definito per il Lemma~\ref{quasi-Lip}. Vogliamo
	mostrare la convergenza della tripla $(\Phi^n, u^n, \omega^n)$. Mostriamo
	intanto la convergenza del flusso. Abbiamo che
	\begin{equation}\label{stima-phi}
		\begin{gathered}
			\Phi_{s,t}^n(x) - \Phi_{s,t}^{n-1}(x) = \int_s^t \{u^n(\Phi_{s,
				r}^n(x), r) -
				u^n(\Phi_{s, r}^{n-1}(x), r)\}\d r + \\ + \int_s^t
				\{u^n(\Phi_{s, r}^{n-1}(x), r) - u^{n-1}(\Phi_{s, r}^{n-1}(x),
				r)\}\d r
		\end{gathered}
	\end{equation}
	
	Il primo termine si riesce a limitare facilmente essendo $u^n$
	quasi-Lipschitz con le stesse costanti. Cerchiamo una stima per il secondo
	termine
	\begin{equation}\label{stima-u}
		\begin{gathered}
			\int_\Omega|u_t^n(x) - u_t^{n-1}(x)| \d x = 
			\int_\Omega \d x \left|\int_\Omega \d y K(x, y)
			\{\omega^{n-1}(y, t) - \omega^{n-2}(y, t)\}\right| = \\
			\int_\Omega \d x \left|\int_\Omega \d y \{
			K(x, \Phi_{0,t}^{n-1}(y)) - K(x, \Phi_{0,t}^{n-2}(y))\}\omega_0(y)\right| \leq
			\\ C\|\omega_0\|_\infty \int_\Omega \phi(|\Phi_{0,t}^{n-1}(y) -
			\Phi_{0,t}^{n-2}(y)|)\d y
		\end{gathered}
	\end{equation}

	Dove la seconda uguaglianza dipende dal fatto che i flussi $\Phi^n$ sono
	diffeomorfismi che preservano le aree (questo è vero per il Teorema di
	Liouville che dimostreremo nella prossima sezione).

	Definiamo l'errore
	\[
		\delta^n(t) = \frac{1}{|\Omega|}\int_\Omega \d x \sup_{s \in [0, T]}|\Phi_{s,t}^n(x) -
		\Phi_{s,t}^{n-1}(x)|
	\]
	Mettendo insieme le equazioni \eqref{stima-phi} e \eqref{stima-u} per
	disuguaglianza triangolare e Jensen si ottiene
	\[
		\delta^n(t) \leq C \left\{\int_0^t\phi(|\delta^n(s)|)\d s +
		\int_0^t\phi(|\delta^{n-1}(s)|)\d s\right\}
	\]
	
	Definendo quindi
	\[
		\rho^N(t) = \sup_{n > N} \delta^n(t)
	\]
	otteniamo che, per ogni $n > N$
	\[
		\delta^n(t) \leq C\int_0^t \phi(\rho^N(s)) \d s
	\]
	e quindi
	\[
		\rho^N(t) \leq C\int_0^t \phi(\rho^N(s))\d s
	\]

	Procedendo come nella dimostrazione del Lemma~\ref{quasi-Lip} si ottiene
	\[
		\lim_{N \to \infty} \rho^N(t) = 0
	\]
	uniformemente in $t \in [0, T]$ per $T$ sufficientemente piccolo. Dato che
	$T$ dipende solo da $\|\omega_0\|_\infty$ e $|\Omega|$, si può reiterare il
	procedimento, ottenendo la convergenza a 0 per ogni tempo.

	Abbiamo quindi un flusso $\Phi$ misurabile ad ogni tempo tale che
	\[
		\lim_{n \to \infty} \int_\Omega \d x |\Phi_{s,t}(x) - \Phi_{s,t}^n(x)| = 0
	\]
	Definiamo quindi
	\[
		\omega_t(x) = \omega_0(\Phi_{t,0}(x))
	\]
	Mostriamo che $\omega_t^n$ convergono debole a $\omega_t$ nel senso delle
	misure. Presa $f \in C^1(\Omega)$ 
	\begin{gather*}
		\left|\int_\Omega f(x)\{\omega_t(x) - \omega_t^n(x)\}\d x\right| = 
		\left|\int_\Omega \{f(\Phi_{0,t}(x)) - f(\Phi_{0,t}^n(x))\}\omega_0(x)\d
		x\right| \leq  \\
		\|\nabla f\|_\infty \|\omega_0\|_\infty \int_\Omega |\Phi_{0,t}(x) -
		\Phi_{0,t}^n(x)|\d x \to 0
	\end{gather*}
	e per densità di $C^1(\Omega)$ in $C^0(\Omega)$ abbiamo che la convergenza
	vale per ogni $f$ continua.

	Definiamo infine
	\[
		u_t(x) = (K \ast \omega_t)(x)
	\]
	e mostriamo la convergenza di $u_t^n$ a $u_t$
	\begin{gather*}
		\int_\Omega |u_t(x) - u_t^n(x)|\d x \leq 
		\int_\Omega \d x\int_\Omega \d y |\omega_0(y)\{
		K(x, \Phi_{0,t}(y)) - K(x, \Phi_{0,t}^n(y))\}| \leq \\
		C \|\omega_0\|_\infty \int_\Omega \phi(|\Phi_{0,t}(y) - \Phi_{0,t}^n(y)|)\d y
		\leq C \|\omega_0\|_\infty \int_\Omega (\epsilon + L_\epsilon
		|\Phi_{0,t}(y)
		- \Phi_{0,t}^n(y)|)\d y \to C'\epsilon
	\end{gather*}
	Per arbitrarietà di $\epsilon$ abbiamo la convergenza a 0 in $L^1$. Dato che
	$\omega_t \in L^\infty$, allora $u_t$ è continuo e quasi-Lip. Per ogni $n$
	vale che
	\[
		\Phi_{s,t}^n(x) = x + \int_s^t u_r^n(\Phi_{s,r}^n(x))\d r
	\]
	quindi per convergenza puntuale e dominata abbiamo
	\[
		\Phi_{s, t}(x) = x + \int_s^t u_r(\Phi_{s,r}(x)) \d r
	\]
	Abbiamo quindi ottenuto che la tripla $(\Phi, u, \omega)$ è soluzione del
	Problema 2 e questo conclude. Le medesime disuguaglianze danno anche
	l'unicità.

\end{proof}

Osserviamo infine che ha senso chiamare la soluzione trovata al Problema 2, una
soluzione debole del Problema 1, in virtù del seguente risultato.

\begin{thm}
	Sia $\omega_t \in L^\infty$ la soluzione costruita nel
	teorema~\ref{yudovich}. Allora per ogni $f \in C^1(\Omega)$
	\[
		\frac{\d}{\d t}\int_\Omega \omega_t(x)f(x)\d x = \int_\Omega
		\omega_t(x)u_t(x)\cdot \nabla f(x) \d x
	\]
	dove la derivata nel termine di sinistra esiste in senso classico.
\end{thm}

Enunciamo infine un risultato di regolarità
delle soluzioni in base alla regolarità del dato iniziale. 

\begin{thm}\label{thm:regularity}
	Sia $\omega_0 \in C^k(\Omega)$ con $k \leq 1$. Allora la soluzione debole
	$\omega_t$ del Problema 2 è anche una soluzione classica (nel senso del
	Problema 1) e inoltre per ogni $t$ $\omega_t \in C^k(\Omega)$.
\end{thm}

Per una dimostrazione del Teorema~\ref{thm:regularity} rimandiamo a
\cite{marchioro_pulvirenti}.

\section{Leggi di conservazione}

Mostriamo adesso che per ogni $F:\R \to \R$ misurabile e limitata, allora la
quantità
\[
	\int_{\Omega} F(\omega_t(x))\d x
\]
è una costante nel tempo. Questo dipende solamente del fatto che il campo $u$ è
a divergenza nulla e che la vorticità viene trasportata tramite le linee di
flusso, grazie al seguente teorema di Liouville. Le costanti del moto di questo
tipo sono dette \textbf{enstrofie}.

\begin{thm}[Liouville]
	Sia $\Phi(x, t)$ un flusso $C^1$ in entrambe le variabili e $u(x, t)$ definito da
	\[
		u(\Phi(x, t), t) = \frac{\d}{\d t}\Phi(x, t)
	\]
	Allora sono fatti equivalenti:
	\begin{enumerate}
		\item $\Phi_t(x)$ è \textbf{incomprimibile}, ovvero è un diffeomorfismo
			che preserva le aree.
		\item $\text{div}(u) = 0$.
	\end{enumerate}
\end{thm}

\begin{proof}
	Per ogni tempo $t$ definiamo $J_t(x)$ il Jacobiano della mappa $x \mapsto
	\Phi_t(x)$. Dimostriamo ora che
	\[
		\frac{\partial}{\partial t}J_t(x) = J_t(x) \nabla \cdot u(\Phi_t(x), t)
	\]
	e da questo segue facilmente la tesi.

	Chiamiamo $S(x, t) = (\partial_j\Phi_t(x)_i)_{i, j}$ la matrice Jacobiana di $x \mapsto \Phi_t(x)$ (quindi
	$J(x, t) = \det{S(x, t)}$). Osserviamo che
	\[
		\frac{\partial}{\partial t} \frac{\partial}{\partial x_j} \Phi_t(x)_i = 
		\frac{\partial}{\partial x_j} u_i(\Phi_t(x), t) = 
		\nabla u_i(\Phi_t(x), t) \cdot \frac{\partial \Phi_t(x)}{\partial x_j}
	\]
	Da cui
	\[
		\frac{\partial}{\partial t}S(x, t) = \nabla u(\Phi_t(x), t) S(x, t)
	\]
	dove $\nabla u = (\partial_j u_i)_{i, j}$. L'ultima equazione si può
	riscrivere come
	\[
		S(x, t+h) = S(x, t) + h \nabla u(\Phi_t(x), t) S(x, t) + o(h)
	\]
	
	Moltiplicando per $S(x, t)^{-1}$ e prendendo il determinante
	\[
		J(x, t+h)J(x, t)^{-1} = \det(\text{Id} + h \nabla u(\Phi_t(x), t)) +
		o(h) = 1 + h \text{Tr}(\nabla u(\Phi_t(x), t)) + o(h)
	\]
	Quindi $J(x, t+h) = J(x, t) + hJ(x, t) \nabla \cdot u(\Phi_t(x), t) + o(h)$
	da cui la tesi.

\end{proof}

Se a questo punto definiamo $\mu_t = \mathcal{L}_\#\omega_t$ la misura
push-forward di Lebesgue tramite $\omega_t$, abbiamo che
\[
	\int_\Omega F(\omega_t(x)) \d x = \int_\R F(y) \mu_t(\d y)
\]
Quindi è sufficiente dimostrare che tutta la misura $\mu_t$ si conserva nel
tempo. D'altra parte, ricordando che $\omega_t(\Phi_{0,t}(x)) = \omega_0(x)$, allora
per ogni $a \in \R$
\[
	\{x: \omega_t(x) < a\} = \Phi_{0,t}(\{x: \omega_0(x) < a\})
\]
Da cui $\mu_t((-\infty, a)) = |\{x: \omega_t(x) < a\}| = |\{x: \omega_0(x) <
a\}| = \mu_0((-\infty, a))$. Quindi $\mu_t = \mu_0$ per cui tutte le enstrofie
si conservano.

Un'altra importante quantità conservata durante il moto è l'\textbf{energia}
definita da
\[
	H = \frac{1}{2}\int_\Omega |u_t(x)|^2\d x
\]

\begin{thm}
	Supponiamo che $u \in C^1$ sia una soluzione dell'equazione di Eulero.
	Allora l'energia si conserva.
\end{thm}

\begin{proof}
	\[
		\frac{\d}{\d t}H = \int_\Omega u \cdot \partial_t u = - \int_\Omega
		\{u\cdot (u \cdot \nabla)u + u \cdot \nabla p\}
	\]
	Ricordiamo l'identità $\text{div}(fv) = v \cdot \nabla f + f \text{div}(v)$,
	dove $f$ e $v$ sono una funzione ed un campo vettoriale $C^1$. Usando il
	teorema della divergenza e che $\text{div}(u) = 0$ si ottiene
	\[
		\int_\Omega u \cdot \nabla p = \int_\Omega \text{div}(pu) =
		\int_{\partial \Omega} pu \cdot n = 0
	\]
	Dove l'ultima uguaglianza dipende dalle condizioni al bordo. Analogamente
	\[
		\int_\Omega u\cdot (u \cdot \nabla)u = \sum_i \int_\Omega u_iu\cdot
		\nabla u_i = -\sum_i \frac{1}{2}\int_\Omega u_i^2\text{div}(u) + \sum_i \frac{1}{2}
		\int_{\partial \Omega} u_i^2u\cdot n = 0
	\]

\end{proof}

Osserviamo infine che possiamo riscrivere l'energia in termini della sola
vorticità come
\begin{gather*}
	H = \frac{1}{2}\int_{\Omega} |u_t(x)|^2 = \frac{1}{2}\int_\Omega |\nabla_x
	\psi_t(x)|^2 = \frac{1}{2}\int_\Omega (-\Delta_x\psi_t(x))\psi_t(x)\d x = \\
	\frac{1}{2} \int_{\Omega^2} V(x, y) \omega_t(x)\omega_t(y)\d x \d y.
\end{gather*}


