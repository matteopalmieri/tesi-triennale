\section{Un modello semplice dei gas e ensemble microcanonico}

In questa sezione costruiremo un semplice modello di gas e deriveremo,
utilizzando la teoria delle grandi deviazioni, il suo comportamento a energia
fissata nel limite in cui il numero di particelle diventa molto grande. In
particolare questo dipende da una distribuzione di probabilità sullo spazio
degli stati molto naturale, detta \emph{ensemble microcanonico}. Successivamente
vedremo come, sempre dalla teoria delle grandi deviazioni, questa probabilità
sia equivalente (nel limite in cui il numero di particelle diventa molto
grande) ad un'altra distribuzione, detta \emph{misura di Gibbs} o \emph{ensemble
canonico}.

\subsection{Il modello}

Supponiamo che il nostro gas sia confinato a muoversi in uno strettissimo tubo
di lunghezza $l$ (che identifichiamo con un intervallo $I\subset\R$), per cui il 
moto delle particelle sarà sostanzialmente
unidimensionale. Supponiamo inoltre che le particelle siano tra loro non
interagenti e che il modulo della velocità di ciascuna di esse sia costante. Una
molecola del gas si muoverà quindi a velocità costante $v$ lungo il tubo, fino a
sbattere contro le parente in fondo con un urto elastico e riprenderà quindi il
moto rettilineo in verso opposto con velocità $-v$.

Un'ulteriore semplificazione che facciamo è che le velocità ammissibili delle
varie particelle siano in numero finito, prese dall'insieme:
\begin{equation}
	\Gamma = \{v_{-r}, \ldots, v_{-1}, v_1, \ldots, v_r\}
\end{equation}
con $v_{-r} < v_{-(r-1)} < \ldots < v_{r-1} < v_r$ e $v_{-i} = - v_i$. 
Sia $n$ il numero di particelle, allora lo spazio delle fasi è:
\begin{equation}
	\Omega_n = I^n\times \Gamma^n = \{\omega = (x_1, \ldots, x_n, y_1, \ldots,
	y_n)|x_i \in I, y_i \in \Gamma\}
\end{equation}
con $x_i, y_i$ rispettivamente la posizione e la velocità dell'$i-$esima
particella. L'evoluzione temporale del sistema è molto semplice, tuttavia per
conoscere lo stato del sistema al tempo $t$ è necessario conoscere lo stato
iniziale (posizione e velocità) di ogni particella. Essendo interessati solo al
comportamento macroscopico del sistema, questo non è strettamente necessario e
diventa in effetti impraticabile per un altissimo numero di particelle. Un
altro modo per trattare il problema è considerare lo stato iniziale $\omega$
scelto a caso secondo una qualche distribuzione di probabilità e guardare alle
quantità macroscopiche in base a questa distribuzione.

Definiamo l'energia del sistema:
\begin{equation}
	U_n(t) = \sum_{i = 1}^n \frac{1}{2}y_i(t)^2
\end{equation}
Questa è banalmente conservata durante la dinamica e vale che $U_n/n \in
[v_1^2/2, v_r^2/2]$. Idealmente vorremmo imporre
$U_n$ uguale ad un valore fissato in questo intervallo. Fisicamente, tuttavia, non potremo conoscere
con arbitraria precisione $U_n$, per cui assumeremo che $U_n/n \in A$ per un
qualche intervallo chiuso $A\subset (v_1^2/2, v_r^2/2)$ anche molto piccolo. Vedremo come questa
scelta sarà supportata dalla descrizione probabilistica che daremo. Lo spazio
delle fasi si è quindi ridotto alla fetta ad energia fissata:
\begin{equation}
	D_n(A) = \{\omega \in X: U_n(\omega)/n \in A\}
\end{equation}
La misura di probabilità più naturale da imporre è quella uniforme su $D_n(A)$.
Per precisione siano:
\begin{itemize}
	\item[-] $\mathcal{B}(I)$ la $\sigma-$algebra dei boreliani su $I$ e sia
		$\mathcal{L}$ la misura di Lebesgue uniforme su $I$ normalizzata, in
		modo che $\mathcal{L}(I) = 1$;
	\item[-] $\mathcal{B}(\Gamma)$ l'insieme delle parti di $\Gamma$ e $\rho$ la
		misura uniforme su $\Gamma$ ($\rho = (\sum_{i =
		-r}^r\delta_{v_i})/(2r)$); 
	\item[-] $\pi_{n, \leb, \rho}(\d\omega) = \leb(\d x_1)\cdots\leb(\d
		x_n)\rho(\d y_1)\cdots\rho(\d y_n)$ la misura prodotto $\Omega_n$,
		dotato della $\sigma-$algebra prodotto.
\end{itemize}
\begin{definition}
	Con le definizioni di sopra, definiamo \textbf{ensemble microcanonico} $P_n$ la
	misura di probabilità su $\Omega_n$ data dalla restrizione di
	$\pi_{n, \leb, \rho}$ alla fetta di energia $D_n(A)$
	\begin{equation}
		P_n(\d \omega) = \pi_{n, \leb,\rho}(\d\omega\;|\; U_n/n \in A)
	\end{equation}
\end{definition}
Osserviamo che questa misura è invariante durante la dinamica, ovvero detto
$\omega_0$ lo stato iniziale del sistema e $\omega(t, \omega_0)$ lo stato al
tempo $t$, allora per ogni $B\subseteq \Omega_n$ misurabile
\begin{equation}
	P_n(\{\omega_0: \omega(t, \omega_0) \in B\}) = P_n(B)
\end{equation}
quindi d'ora in avanti non indicheremo più la dipendenza dal tempo. Indichiamo
infine con $X_i, Y_i:\Omega_n\to\R$ le proiezioni $X_i(\omega) = x_i$ e
$Y_i(\omega) = y_i$ (con $\omega = (x_1, \ldots, x_n, y_1, \ldots, y_n)$).

\subsection{Limite termodinamico}

Definiamo ora le quantità macroscopiche andremo a studiare.
\begin{definition}
	Data $f:I\times\Gamma\to\R$ una funzione misurabile limitata, chiamiamo
	\textbf{osservabili} le funzioni $F_n:\Omega_N\to\R$ delle forma
	\begin{equation*}
		F_n(\omega) = \sum_{i = 1}^n f(X_i(\omega), Y_i(\omega))
	\end{equation*}
	nel caso in cui $f$ sia anche continua, diremo che $F_n$ è un
	\textbf{osservabile continuo}.
\end{definition}
L'obiettivo sarà studiare il comportamento degli osservabili rispetto al
microcanonico nel limite $n\to+\infty$, detto \textbf{limite termodinamico}.
Questo in effetti discende direttamente dalla teoria delle grandi deviazioni.

Consideriamo come spazio $\mathcal{X} = I \times \Gamma$ e come successione di
variabili aleatorie i.i.d.\ a valori in $\mathcal{X}$ le coppie $(X_i, Y_i)$.
Come nel setting del teorema di Sanov, definiamo le misure empiriche:
\[
	L_n(\omega, \d x \times \d y) = \frac{1}{n}\sum_{i = 1}^n
	\delta_{X_i(\omega)}(\d x) \delta_{Y_i(\omega)}(\d y)
\]
Grazie al teorema di Sanov~\ref{sanov} abbiamo un principio di grandi deviazioni
su $\mathcal{M}(I\times \Gamma)$. Osserviamo adesso che:
\[
	\int_\mathcal{X}\frac{1}{2}y^2 L_n(\omega, \d x \times \d y) = 
	\frac{1}{n}\sum_{i = 1}^n \frac{1}{2}Y_i(\omega)^2 = \frac{1}{n}U_n(\omega)
\]
Quindi se definiamo $\Psi_A = \{\mu \in \mathcal{M}(\mathcal{X}):
\int\frac{1}{2}y^2\mu(\d x\times \d y) \in A\}$, abbiamo che:
\[
	\left\{\omega \in \Omega_n: \frac{1}{n}U_n(\omega) \in A\right\} = \left\{\omega \in
	\Omega_n: L_n(\omega, \cdot) \in \Psi_A\right\}
\]
Applicando il teorema di Sanov condizionato~\ref{sanov-cond}, abbiamo che le
misure $P_n(L_n \in \cdot | U_n \in A) = P_n(L_n \in \cdot | L_n \in \Psi_A)$
ammettono un upper bound del principio di grandi deviazioni, i cui punti di equilibrio sono i
minimi della funzione $D_{KL}(\cdot\,|| \mathcal{L}\otimes\rho)$ su $\Psi_A$. La
seguente proposizione caratterizza questi minimi.

\begin{definition}
	Dato $\beta \in \R$ definiamo 
	\begin{equation}\label{gibbs-1}
		\rho_\beta(\d x \times \d y) =
		\frac{\exp{(-\frac{1}{2}y^2\beta)}}{\int_\mathcal{X}
		\exp{(-\frac{1}{2}y'^2\beta)}\mathcal{L}(\d x')\rho(\d y')}
		\mathcal{L}(\d x) \otimes \rho(\d y)
	\end{equation}
\end{definition}

\begin{prop}~\begin{itemize}
		\item Dato $u \in (v_1^2/2, v_r^2/2)$ esiste un unico $\beta(u) \in \R$ tale che
			\[
				\int_\mathcal{X} \frac{1}{2}y^2\rho_{\beta(u)}(\d x \times \d y) = u
			\]
		\item Dato $u \in (v_1^2 /2, v_r^2/ 2)$, sia $\beta = \beta(u)$ del
			punto precedente e sia $\Psi_u = \{\mu \in \mathcal{M}(\mathcal{X}):
			\int\frac{1}{2}y^2\mu(\d x\times \d y) = u\}$. Allora $\rho_\beta$ è
			l'unico minimo della funzione $D_{KL}(\cdot || \mathcal{L}\otimes
			\rho)$ su $\Psi_u$. Inoltre vale che
			\[
				D_{KL}(\rho_\beta || \mathcal{L}\otimes\rho) = - \beta u -
				\log{\int_\mathcal{X}\exp{\left(-\frac{1}{2}y^2\beta\right)}\mathcal{L}(\d
				x')\rho(\d y')}
			\]
	\end{itemize}
\end{prop}

\begin{proof}
	Per il primo punto osserviamo che la funzione
	\[
		\beta \mapsto \int_{\mathcal{X}} \frac{1}{2}y^2 \rho_\beta(\d x\times \d
		y) = \frac{1}{r}\sum_{i = 1}^r \frac{1}{2}v_i^2
		e^{-\frac{1}{2}v_i^2\beta} \cdot
		\left(\frac{1}{r}\sum_{i = 1}^r e^{-\frac{1}{2}v_i^2}\right)^{-1}
	\]
	è una funzione strettamente crescente (è una media pesata
	di $\frac{1}{2}y^2$ con pesi che si spostano verso le velocità più alte
	all'aumentare di $\beta$). Per $\beta \to +\infty$ i pesi tendono al vettore
	in $\R^r$ $(0,\ldots, 0, 1)$, quindi la media tende a $\frac{1}{2}v_r^2$.
	Analogamente per $\beta \to -\infty$ la media tende a $\frac{1}{2}v_1^2$.

	Per il secondo punto, sia $\mu \in \Psi_u$ con $D_{KL}(\mu ||
	\mathcal{L}\otimes \rho) < +\infty$. Allora:
	\begin{gather*}
		D_{KL}(\mu || \mathcal{L}\otimes \rho) =
		\int_{\mathcal{X}}\log{\frac{\d \mu}{\d (\mathcal{L}\otimes\rho)}}\mu(\d
		x \times \d y) = \\
		\int_{\mathcal{X}}\log{\frac{\d \mu}{\d \rho_\beta}}\mu(\d
		x \times \d y) + 
		\int_{\mathcal{X}}\log{\frac{\d \rho_\beta}{\d (\mathcal{L}\otimes\rho)}}\mu(\d
		x \times \d y) = \\
		D_{KL}(\mu || \rho_\beta) - \beta \int_{\mathcal{X}}\frac{1}{2}y^2
		\mu(\d x \times \d y) -
		\log{\int_\mathcal{X}\exp{\left(-\frac{1}{2}y^2\beta\right)}\mathcal{L}(\d
		x')\rho(\d y')} 
	\end{gather*}
	Questo in particolare è vero per $\mu = \rho_\beta$, da cui:
	\[
		D_{KL}(\mu || \mathcal{L}\otimes\rho) = D_{KL}(\mu || \rho_\beta) +
		D_{KL}(\rho_\beta || \mathcal{L}\otimes\rho)
	\]
	con
	\[
		D_{KL}(\rho_\beta || \mathcal{L}\otimes\rho) = - \beta u -
		\log{\int_\mathcal{X}\exp{\left(-\frac{1}{2}y^2\beta\right)}\mathcal{L}(\d
		x')\rho(\d y')}
	\]
\end{proof}

Osservando ora che $\Psi_A = \cup_{u \in A} \Psi_u$, abbiamo che
necessariamente il minimo di $D_{KL}(\cdot || \mathcal{L}\otimes\rho)$ su
$\Psi_A$ deve essere della forma \eqref{gibbs-1} per un certo $\beta$ (il minimo
è unico essendo $D_{KL}$ strettamente convessa).

Possiamo tornare a caratterizzare il limite degli osservabili. Consideriamo $f:I
\times \Gamma \to \R$ continua (quindi limitata essendo $I\times \Gamma$
compatto). Questa induce la mappa continua:
\[
	\tilde{f}:\mathcal{M}(I\times \Gamma)\to\R \qquad \mu \mapsto
	\int_\mathcal{X}f(x, y) \mu(\d x \times \d y)
\]
Grazie al principio delle contrazioni~\ref{contr-princ} possiamo trasportare su
$\R$ tramite $\tilde{f}$ la proprietà delle grandi deviazioni che abbiamo su
$\mathcal{M}(I \times \Gamma)$ data da Sanov condizionato. Riassumiamo nel
seguente teorema quello che abbiamo ottenuto:

\begin{thm}\label{micro-convergenza}
	Sia $f:I \times \Gamma \to \R$ continua e $F_n$ gli osservabili associati.
	Allora:
	\begin{itemize}
		\item Esiste una unica $u \in A$ e $\beta = \beta(u) \in \R$ tale per
			cui
			\[
				u = \int_\mathcal{X}\frac{1}{2}y^2\rho_\beta(\d x \times \d y)
				\qquad D_{KL}(\rho_\beta || \mathcal{L}\otimes \rho) = 
				\min_{\mu \in \Psi_A} D_{KL}(\mu || \mathcal{L}\otimes \rho)
			\]
		\item Condizionatamente all'evento $\{U_n/n \in A\}$ la successione di
			variabili aleatorie $F_n/n$ soddisfa l'upper bound del principio di grandi deviazioni.
		\item Esiste un unico punto di equilibrio della successione $F_n/n$ che
			è:
			\[
				\langle f \rangle_{\text{micro}} = \int_\mathcal{X}
				\frac{1}{2}y^2\rho_\beta(\d x \times \d y)
			\]
	\end{itemize}  
\end{thm}

\begin{proof}
	Il primo punto lo abbiamo già dimostrato. Per quanto riguarda il secondo
	basta osservare che:
	\[
		\int_{\mathcal{X}}f(x, y)L_n(\omega, \d x \times \d y) = 
		\frac{1}{n}\sum_{i = 1}^n f(X_i(\omega), Y_i(\omega)) = \frac{1}{n}F_n(\omega)
	\]
	Da cui:
	\[
		P_n(F_n / n \in E | U_n / n \in A) = P_n(L_n \in \tilde{f}^{-1}(E) | L_n
		\in \Psi_A)
	\]
	quindi effettivamente la tesi è data dal principio delle contrazioni
	utilizzando la mappa $\tilde{f}$ e il principio delle grandi deviazioni di
	$L_n$.

	Per l'ultimo punto basta osservare che i punti di equilibrio per $F_n/n$
	sono immagine di punti di equilibrio per le $L_n$ che abbiamo dimostrato
	essere solo $\rho_\beta$ per un certo $\beta$ fissato descritto al primo
	punto.
\end{proof}


\section{Misura di Gibbs ed equivalenza con l'ensemble microcanonico}

Nella sezione precedente abbiamo caratterizzato il limite di osservabili
rispetto alla misura di probabilità data dal microcanonico. Ovvero assumendo di
sapere solo che $U_n/n \sim u$, abbiamo trovato un $\beta = \beta(u)$ per cui
per $n$ grande
\[
	\frac{F_n}{n} = \frac{1}{n}\sum_{i = 1}^n f(X_i, Y_i) \sim \langle f
	\rangle_{\text{micro}} = \int_\mathcal{X}f(x, y)\rho_{\beta(u)}(\d x \times \d y)
\]

Definiamo adesso per ogni $n$ una nuova misura di probabilità, l'ensemble
canonico, meno intuitiva del microcanonico, ma più semplice da gestire e
dimostriamo che sono equivalenti nel limite in cui $n$ tende a infinito.

\begin{definition}
	Come prima consideriamo lo spazio $\Omega_n = (I \times \Gamma)^n$ con la
	misura prodotto $(\mathcal{L}\otimes \rho)^{\otimes n}$. Fissato un
	parametro $\beta > 0$, avevamo anche definito la misura $\rho_\beta$ con densità rispetto
	a $\mathcal{L}\otimes \rho$. Definiamo \textbf{ensemble canonico} o
	\textbf{misura di Gibbs} la misura di probabilità su $\Omega_n$ data da:
	\[
		P_{n, \beta} = \rho_\beta^{\otimes n}
	\]
\end{definition}

Anche in questo caso ci si può chiedere a cosa tendano gli osservabili $F_n/n$,
questa volta rispetto alla misura di Gibbs.

\begin{thm}[Equivalenza tra canonico e microcanonico]
	Sia $\beta \in \R$ un parametro fissato, $f$ limitata è misurabile ed $F_n$ l'osservabile associato. Allora
	$F_n/n$ soddisfa un principio di grandi deviazioni (rispetto alle misure di
	Gibbs con parametro $\beta$) con un unico punto di equilibrio
	\[
		\langle f \rangle_{\text{can}} = \int_\mathcal{X} f(x, y)\rho_\beta(\d x
		\times \d y)
	\]
\end{thm}

\begin{proof}
	Le variabili $(X_i, Y_i)$ al variare di $i$ sono i.i.d.\ con legge
	$\rho_\beta$ quindi per il teorema di Sanov le misure empiriche $L_n$
	ammettono un principio di grandi deviazioni con unico punto di equilibrio
	$\rho_\beta$. Integrando $f$ rispetto alle $L_n$ e applicando il principio
	delle contrazioni come in~\ref{micro-convergenza} si ottiene la tesi.
\end{proof}

Ricapitolando abbiamo due possibili successioni di misure di probabilità:
\begin{itemize}
	\item l'ensemble microcanonico definito a partire da un intervallo chiuso
		$A \subset (v_1^2/2, v_r^2/2)$ attorno ad un certo valore $u$. Per
		questo $u$ esiste un valore $\beta(u)$ per cui gli osservabili tendono a
		$\langle f \rangle_{\text{micro}, A}$.
	\item l'ensemble canonico definito a partire da un parametro $\beta$ e
		rispetto al quale gli osservabili tendono a $\langle f
		\rangle_{\text{can}, \beta}$.
\end{itemize}
Le due mappe $u \mapsto \beta(u)$ e $\beta \mapsto u =
\int\frac{1}{2}y^2\rho_\beta$ sono una l'inversa dell'altra e danno
l'equivalenza tra i due ensemble per quanto riguarda i limiti di osservabili per
$n \to +\infty$.

Concludiamo il capitolo riscrivendo la misura di Gibbs in una forma più comune.
\begin{prop}
	L'ensemble canonico si può anche scrivere come:
	\[
		P_{n, \beta}(\d \omega) = \frac{\exp{(-\beta U_n(\omega))}}{
		\int_{\Omega_n}\exp{(-\beta U_n(\omega'))}(\mathcal{L}\otimes\rho)^{\otimes 
		n}(\d \omega ')}(\mathcal{L}\otimes\rho)^{\otimes n}(\d \omega)
	\]
	Ovvero la misura di Gibbs ha densità proporzionale a $\exp{(-\beta U)}$
	rispetto alla misura prodotto.
\end{prop}

\begin{proof}
	Utilizzando la definizione di $\rho_\beta$:
	\[
		P_{n, \beta}(\d \omega) = (\rho_\beta)^{\otimes n}(\d \omega) \propto 
		\bigotimes_{i = 1}^n \exp{\left(-\frac{1}{2}\beta y_i^2\right)}\mathcal{L}(\d
		x_i)\rho(\d y_i) = \exp{(-\beta
		U_n(\omega))}(\mathcal{L}\otimes\rho)^{\otimes n}(\d \omega)
	\]
	Dove la costante di proporzionalità è quella che rende $P_{n, \beta}$ una
	misura di probabilità.
\end{proof}
