La teoria delle grandi deviazioni si occupa di studiare il comportamento
asintotico di successioni di variabili aleatorie, come il Teorema del
Limite Centrale e la Legge dei Grandi Numeri. Essa in particolare descrive in
modo preciso situazioni in cui ci sono \emph{eventi rari} che hanno probabilità
esponenzialmente piccole di accadere e dà un valore preciso per il tasso con cui
esse decadono. Nella prima parte del capitolo richiameremo rapidamente le
definizioni e i fatti generali, enunciando come risultato più importante il
teorema di Sanov. Tutte le dimostrazioni delle prime tre sezioni sono rimandate
in appendice. Nella seconda parte presenteremo un semplice
modello di gas ideale e vedremo come la teoria delle grandi deviazioni ne
descriva perfettamente il comportamento nel limite di un numero infinito di
particelle. In questa situazione introdurremo l'ensemble microcanonico e la
misura di Gibbs e ne mostreremo l'equivalenza.

\section{Principi generali}

\begin{definition}
	Sia $\mathcal{X}$ uno spazio metrico completo separabile dotato della $\sigma-$algebra
	dei boreliani $\mathcal{B}(\mathcal{X})$ e sia $\{Q_n\}_{n\in\N}$ una successione di
	misure di probabilità su $\mathcal{B}(\mathcal{X})$. Diciamo che $\{Q_n\}_{n
	\in \N}$ soddisfa la
	\textbf{proprietà delle grandi deviazioni} (LDP) se esistono
	\begin{itemize}
		\item una successione $\{a_n\}_{n\in\N}$ di reali positivi, con
			$\lim_{n\to\infty}a_n = +\infty$;
		\item un funzione $I:\mathcal{X}\to[0, +\infty]$ semicontinua inferiormente e con
			i sottolivelli compatti detta \emph{rate function}.
	\end{itemize}
	tali per cui valgano le due seguenti disuguaglianze:
	\begin{itemize}
		\item (\textbf{large deviation upper bound}) per ogni $C\in \mathcal{X}$ chiuso 
			\[
				\limsup_{n\to + \infty} a_n^{-1}\log{Q_n(C)}\leq -\inf_{x\in C}I(x)
			\]
		\item (\textbf{large deviation lower bound}) per ogni $A\in \mathcal{X}$ aperto 
			\[
				\liminf_{n\to + \infty} a_n^{-1}\log{Q_n(A)}\geq -\inf_{x\in A}I(x)
			\]
	\end{itemize}
\end{definition}

Nel seguito risulterà molto importante il caso in cui le misure $Q_n$ sono leggi
di variabili aleatorie a valori in $\mathcal{X}$. Per cui estendiamo la
definizione nel modo seguente:

\begin{definition}
	Data una successione di spazi di probabilità $(\Omega_n, \mathcal{F}_n,
	P_n)$ ed una di variabili aleatorie $X_n:\Omega_n \to \mathcal{X}$ con
	$\mathcal{X}$ spazio metrico completo e separabile, diremo che le $X_n$
	soddisfano una proprietà di grandi deviazioni se la soddisfano le loro leggi
	$Q_n = (X_n)_\#P_n$.
\end{definition}

Osserviamo che la richiesta che la rate function $I$ abbia i sottolivelli
compatti implica la semicontinuità inferiore e che $I$ assuma minimo su ogni
chiuso (quindi in particolare $I$ ha un minimo globale).

Ponendo $C = X$ nella prima disuguaglianza otteniamo che in effetti il minimo di
$I$ è 0. Chiamiamo \textbf{punti di equilibrio} gli zeri della rate function.

In questa definizione bisogna pensare $I(x)$ come una misura di quanto $x$ sia
lontano dall'essere un punto di equilibrio. Una successione di misure ammette 
un principio di grandi deviazioni
tipicamente quando tende a concentrarsi in alcuni punti, gli zeri della rate
function, e la convergenza a 0 delle misure lontano da questi punti avviene
esponenzialmente, con tasso dato da $I$. Un esempio di questo viene spiegato
dalla seguente proposizione.

\begin{prop}\label{prop:dec-chiusi}
	Sia $E \subseteq \mathcal{X}$ un boreliano tale che $\bar{E}$ non contiene zeri di $I$. Allora esiste una
	costante $c > 0$ tale che definitivamente $Q_n(E) \leq e^{-a_nc}$.
\end{prop}

\begin{proof}
	Dato che $I$ è semicontinua inferiormente e ammette sottolivelli compatti,
	allora ha minimo $m$ in $\bar{E}$ e questo è strettamente positivo per ipotesi.
	Quindi 
	\[
		\limsup_{n \in N}a_n^{-1}\log{Q_n(\bar{E})} \leq -m
	\]
	Per cui definitivamente $a_n^{-1}\log{Q_n(E)} \leq -c$ per ogni $0 < c < m$,
	da cui la tesi.
\end{proof}

Illustriamo adesso due risultati che permettono di trasferire la LDP da uno
spazio ad un altro tramite mappe continue e da due spazi allo spazio prodotto.

\begin{thm}[Principio di Contrazione]\label{contr-princ}
	Siano $\mathcal{X}, \mathcal{Y}$ spazi metrici completi separabili dotati delle rispettive
	$\sigma-$algebre dei boreliani e $\{Q_n\}_{n\in\N}$ una
	successione di misure di probabilità su $\mathcal{X}$. Supponiamo che
	$\{Q_n\}_{n\in\N}$ soddisfi un principio di grandi deviazioni con
	coefficienti $a_n$ e rate function $I$. Sia inoltre $T:\mathcal{X}\to
	\mathcal{Y}$ una funzione
	continua. Definiamo $J:\mathcal{Y}\to[0,+\infty]$:
	\[
		J(y) = \inf_{x \in T^{-1}(y)}I(x)
	\]
	e siano $R_n = T_\#Q_n$ le misure di probabilità push forward su
	$\mathcal{Y}$.
	Allora $J$ è una rate function su $\mathcal{Y}$ e $R_n$ soddisfa un principio di
	grandi deviazioni con rate function $J$ e coefficienti $a_n$. 
\end{thm}

\begin{thm}[LDP per lo spazio prodotto]\label{ldp-prod}
	Siano $P_n$ e $Q_n$ due successioni di misure di probabilità sugli spazi
	$\mathcal{X}$ e $\mathcal{Y}$ che soddisfano un principio di grandi deviazioni con rate function $I$
	e $J$ rispettivamente e tassi comuni $a_n$. Allora la sequenza di misure prodotto $R_n = P_n
	\otimes Q_n$ su $\mathcal{X} \times \mathcal{Y}$ soddisfa un principio di grandi deviazioni con
	rate function $K(x, y) = I(x) + J(y)$ e tassi $a_n$.
\end{thm}

\section{Stime asintotiche di integrali e formulazione equivalente}

In \cite{Varadhan1966AsymptoticPA} Varadhan ha mostrato che se $Q_n$ soddisfa un
principio di grandi deviazioni, allora è possibile dedurne stime asintotiche per
integrali della forma
\[
	\int_\mathcal{X} e^{a_nF(x)}Q_n(\d x)
\]
e che questi dipendono sostanzialmente dal massimo di $F(x) - I(x)$. 

\begin{thm}[Varadhan]\label{varadhan}
	Sia $Q_n$ una successione di misure di probabilità su $\mathcal{X}$ che soddisfa
	una LDP con rate function $I$ e tassi $a_n$. Allora per ogni $F \in
	\mathcal{C}_b(\mathcal{X})$ continua e limitata vale:
	\[
		\lim_{n \to +\infty} \frac{1}{a_n} \log{\int_\mathcal{X} e^{a_nF(x)}Q_n(\d x)} =
		\sup_{x \in \mathcal{X}}{F(x) - I(x)}
	\]
\end{thm}

Anche per questo risultato rimandiamo per la dimostrazione in appendice.
Almeno come euristica, interpretando formalmente la proprietà di grandi deviazioni come $Q_n(\d x)
\asymp \exp{(-a_nI(x))}\d x$, abbiamo che:
\[
	\frac{1}{a_n}\log{\int_\mathcal{X} e^{a_nF(x)}Q_n(\d x)} \asymp
	\frac{1}{a_n}\log{\int_\mathcal{X} e^{a_n(F(x) - I(x))}\d x} \to \sup_{x \in
		\mathcal{X}}{F(x) - I(x)}
\]

Bisogna osservare che questo argomento è puramente intuitivo, non avendo
significato la misura $\d x$ su un generico spazio $\mathcal{X}$.
Queste stime asintotiche saranno fondamentali nel seguito per cui assegnamo loro
un nome.

\begin{definition}
	Sia $\mathcal{X}$ uno spazio metrico completo separabile dotato della $\sigma-$algebra
	dei boreliani $\mathcal{B}(\mathcal{X})$ e sia $\{Q_n\}_{n\in\N}$ una successione di
	misure di probabilità su $\mathcal{B}(\mathcal{X})$. Diciamo che $\{Q_n\}_{n
	\in \N}$ soddisfa il 
	\textbf{principio di Laplace} (LP) con rate function $I$ e tassi $a_n$ se
	per ogni $F \in \mathcal{C}_b(\mathcal{X})$ continua e limitata vale
	\[
		\lim_{n \to +\infty} \frac{1}{a_n} \log{\int_X e^{a_nF(x)}Q_n(\d x)} =
		\sup_{x \in \mathcal{X}}{F(x) - I(x)}
	\]
\end{definition}

Il teorema di Varadhan ci dice che avere una proprietà di grandi deviazioni
implica soddisfare un principio di Laplace. In effetti vale anche il viceversa e
i due principi sono equivalenti:

\begin{thm}[LP $\impl$ LDP]\label{lp-impl-ldp}
	Supponiamo che $\{Q_n\}_{n \in \N}$ soddisfi un principio di Laplace con rate function
	$I$ e tassi $a_n$. Allora soddisfa anche la proprietà di grandi deviazioni
	con la stessa rate function e gli stessi tassi.
\end{thm}

\section{Teorema di Sanov}

Ci interessa adesso studiare un esempio fondamentale in cui vale il principio di
grandi deviazioni. 
Come prima sia $\mathcal{X}$ uno spazio metrico completo separabile e
$X_n:\Omega\to\mathcal{X}$ con $n\geq 1$ una sequenza di variabili aleatorie
i.i.d.\ su un comune spazio di probabilità $(\Omega, \mathcal{F},
\P)$. Sia $\mathcal{M}(\mathcal{X})$ lo spazio delle misure di
probabilità su $\mathcal{X}$ e $\rho\in\mathcal{M}(\mathcal{X})$ la legge di $X_1$. 
Lo spazio $\mathcal{M}(\mathcal{X})$ è metrizzabile tramite la \textbf{distanza
di Prohorov} $\pi$ definita da 
\[
	\pi(\mu, \nu) = \inf\{\epsilon > 0: \forall A \subseteq \mathcal{X} \;
	\text{vale che} \; \mu(A) \leq \nu(A_\epsilon) + \epsilon \; \text{e} \; 
	\nu(A) \leq \mu(A_\epsilon) + \epsilon\}
\]
dove $A_\epsilon = \cup_{x \in A} B(x, \epsilon)$ è l'intorno di punti di $A$ a
distanza minore di $\epsilon$.

Enunciamo quindi le proprietà fondamentali di questa distanza.

\begin{thm}
	La distanza di Prohorov rende $\mathcal{M}(\mathcal{X})$ anch'esso uno
	spazio metrico completo e separabile. Inoltre la topologia indotta è la
	stessa della convergenza debole di misure.
\end{thm}

Essendo $\mathcal{M}(\mathcal{X})$ uno spazio metrico, possiamo dotarlo della
$\sigma$-algebra dei boreliani. Dati allora $n$ punti $x_1, \ldots, x_n \in \mathcal{X}$, possiamo assegnare la misura
di probabilità $\mu \in \mathcal{M}(\mathcal{X})$:
\[
	\mu = \frac{1}{n}\sum_{i = 1}^n \delta_{x_i}
\]
e questo mi definisce una mappa continua (e quindi misurabile) da $\mathcal{X}^n \to \mathcal{M}(\mathcal{X})$.
Tramite questa mappa, pre-componendo con il vettore aleatorio $(X_1, \ldots, X_n)$,
posso definire la \textbf{misura empirica} $L_n:\Omega\to
\mathcal{M}(\mathcal{X})$ data dalle prime $n$ variabili:
\[
	L_n(\omega, \d x) = \frac{1}{n}\sum_{i = 1}^n \delta_{X_i(\omega)}
\]
Denotiamo quindi con $P_n$ la legge di $L_n$, misura su $\mathcal{M}(\mathcal{X})$. 
Una conseguenza interessante della legge dei grandi numeri è la seguente:

\begin{thm}\label{thm:lln}

	Siano $\{L_n\}_{n \in \N}$ le misure definite sopra, derivanti dalla
	sequenza di variabili aleatorie $X_n$, e sia $\rho$ la legge di ciascuna
	delle $X_n$. Allora per quasi ogni $\omega \in \Omega$:
	\[
		\lim_{n \to \infty} \pi(L_n(\omega), \rho) = 0
	\]
	Questo implica la più debole convergenza in probabilità, ovvero
	\[
		\forall \epsilon > 0 \qquad \lim_{n \to \infty} \P(\pi(L_n, \rho) >
		\epsilon) = 0
	\]

\end{thm}

Il teorema di Sanov che stiamo per illustrare si occupa di descrivere
l'andamento esatto con cui decadono le code di probabilità $\P(\pi(L_n, \rho) >
\epsilon)$. Prima di enunciarlo ricordiamo un'importante quantità per discriminare tra misure di probabilità:

\begin{definition}

	Date due misure $\mu, \nu \in \mathcal{M}(\mathcal{X})$ con $\nu \ll \mu$ e
	tali che
	\[
		\int_\mathcal{X}\left|\log{\frac{\d \nu}{\d \mu}}\right|\d \nu < +\infty
	\]
	definiamo \textbf{divergenza di Kullback-Leibler}
	\begin{equation}\label{eq:KL}
		D_{KL}(\nu || \mu) = \int_{\mathcal{X}}\log{\frac{\d\nu}{\d\mu}}\d\nu 	
	\end{equation}
	Negli altri casi definiamo $D_{KL}(\nu || \mu) = +\infty$.

\end{definition}

Possiamo quindi enunciare il teorema di Sanov, che ci dà un principio di grandi
deviazioni per le $P_n$.

\begin{thm}[Sanov]\label{sanov}
	Consideriamo le $\rho, L_n$ definite in precedenza. Sia 
	\[
		I(\nu) = D_{KL}(\nu\|\rho)
	\]
	e $a_n = n$. Allora le misure $P_n$ ammettono un principio di grandi deviazioni con
	rate function $I$ e coefficienti $a_n$. In particolare $I$ ammette $\rho$ come unico zero.
\end{thm}

Dal teorema di Sanov e dalla proposizione~\ref{prop:dec-chiusi} discende
immediatamente il seguente corollario che rafforza il Teorema~\ref{thm:lln}.

\begin{cor}

	Consideriamo sempre $\rho$ ed $L_n$ come sopra. Dato $\epsilon > 0$ consideriamo il chiuso $C_\epsilon = \{\mu \in
	\mathcal{M}(\mathcal{X}): \pi(\mu, \rho) \geq \epsilon\} \subset
	\mathcal{M}(\mathcal{X})$. Su questo chiuso $I$ è strettamente positiva,
	quindi esiste una costante $c_\epsilon > 0$ tale per cui
	\[
		\P(\pi(L_n, \rho) \geq \epsilon) \leq e^{-c_\epsilon n}
	\]

\end{cor}

Vogliamo concludere la sezione mostrando come il teorema di Sanov si adatti
molto bene alla probabilità condizionale. In particolare sapendo che $L_n \in E$
con $E\subseteq \mathcal{M}(\mathcal{X})$ misurabile, riusciamo a dare un principio
di grandi deviazioni per le probabilità $\P(\cdot | L_n \in E)$? Con qualche
leggera ipotesi su $E$ questo è vero ed è una diretta conseguenza del teorema di
Sanov.

\begin{definition}
	Data una rate function $I$ su uno spazio metrico $\mathcal{X}$, diciamo che $E
	\subseteq \mathcal{X}$ è un insieme di $I-$continuità se
	\[
		\inf_{x \in \bar{E}} I(x) = \inf_{x \in \overset{\circ}{E}} I(x)
	\]
\end{definition}

Osserviamo che, in generale:
\begin{gather*}
	- \inf_{x \in \overset{\circ}{E}} I(x)\leq 
	\liminf_{n \in \N}\frac{1}{n}\log{Q_n(\overset{\circ}{E})}
	\leq \limsup_{n \in N} \frac{1}{n}\log{Q_n\left(\bar{E}\right)} \leq 
	-\inf_{x \in \bar{E}} I(x)
\end{gather*}
Ma se $E$ è di $I-$continuità, allora il primo e l'ultimo termine
sono uguali (ed in particolare sono per monotonia sono uguali a $-\inf_{x \in
E}I(x)$), da cui otteniamo:
\[
	\lim_{n \in \N} \frac{1}{n}\log{Q_n(E)} = -\inf_{x \in E}I(x)
\]

\begin{thm}[Sanov condizionato]\label{sanov-cond}

	Sia $E \subseteq \mathcal{M}(\mathcal{X})$ un chiuso (con parte interna non
	vuota) e di $I-$continuità (dove $I$ è la rate function associata alle
	$L_n$). Supponiamo che $I$
	non sia costantemente $+\infty$ su $E$. In questo caso
	definitivamente $\P(L_n \in E) > 0$ e siano $\P(\cdot | L_n \in
	E)$ le probabilità condizionali. Siano quindi (per $n$ sufficientemente
	grande) $Q_n^{E}$ le misure immagine
	su $\mathcal{M}(\mathcal{X})$ di queste probabilità condizionali. In
	particolare
	\[
		\forall A\subseteq \mathcal{M}(\mathcal{X}) \ \text{misurabile}\ 
		Q_n^{E}(A) = \P(L_n \in A | L_n \in E)
	\] 
	Allora questa nuova successione di misure ammette l'upper bound per i chiusi
	del principio di grandi deviazioni con $a_n = n$ e rate function
	\[
		I^E(x) = 
		\begin{cases}
			I(x) - \inf_{x' \in E} I(x') \ & \text{se}\  x \in E \\
			+ \infty \ & \text{altrimenti}
		\end{cases}
	\]
	ovvero per ogni $C \subset \mathcal{M}(\mathcal{X})$ chiuso 
	\[
		\limsup_{n \in \N} \frac{1}{n}\log{Q_n^E(C)} \leq - \inf_{x \in C}
		I^E(x)
	\]


\end{thm}

Per concludere, un'osservazione importante è che gli zeri di $I^E$,
ovvero i punti attorno ai quali si concentrano le misure $Q_n^E$, sono
esattamente i punti di minimo di $I$ su $E$ (che esistono essendo $E$
chiuso) e che la Proposizione~\ref{prop:dec-chiusi} si basa solo sull'upper
bound per i chiusi, che quindi può essere applicato anche in questo caso.
