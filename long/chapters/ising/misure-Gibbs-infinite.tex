\section{Misure di Gibbs su volumi infiniti}

L'obiettivo di questa sezione è di costruire misure di Gibbs per modelli
ferromagnetici su tutto $\Z^d$. Gli ingredienti chiave che abbiamo usato nel
caso di volumi $\L$ finiti erano un misura di probabilità a priori (la misura
prodotto $\pi_\L P_\rho$) e l'Hamiltoniana. Nel caso di volume infinito non
abbiamo problemi nella definizione di una misura prodotto, ma sia nella
definizione dell'Hamiltoniana, sia in quella della funzione di partizione $Z$
abbiamo un numero infinito e non sommabile di termini. Procediamo quindi nel
modo seguente:

\begin{itemize}
	\item Lo spazio delle configurazioni è $\Omega = \{-1, 1\}^{\Z^d}$. Su
		$\{-1,1\}$ consideriamo la topologia discreta, per cui $\Omega$ eredita
		la topologia prodotto che lo rende uno spazio metrico compatto. Come
		spazio metrico, $\Omega$ è dotato della $\sigma-$algebra dei boreliani
		$\mathcal{B}(\Omega)$ che coincide con quella generata dagli insiemi
		cilindrici.
	\item Al variare di $\L\subset \Z^d$ finito, consideriamo le misure di Gibbs
		$P_{\L, \beta, h, \tilde{\omega}}$ tali che $\tilde{\omega}_j = \pm 1$
		per ogni $j \in \L^c$. Queste si possono vedere facilmente come misure
		di probabilità su tutto $\Z^d$. Dato infatti $\omega \in \Omega$,
		definiamo:
		\[
			\pi_\L:\Omega = \{-1, 1\}^{\Z^d} \to \{-1, 1\}^\L \qquad
			\omega = (\omega_j)_{j \in \Z^d} \mapsto (\omega_j)_{j \in \L}
		\]
		la proiezione. Sia inoltre
		\[
			B_{\L, \tilde{\omega}} = \{\omega \in \Omega: \omega_j =
			\tilde{\omega}_j \forall j \in \L^c\}
		\]
		A questo punto estendiamo $P_{\L, \beta, h, \tilde{\omega}}$ ad una
		misura su $\Omega$ $\bar{P}_{\L, \beta, h, \tilde{\omega}}$ a supporto
		in $B_{\L, \tilde{\omega}}$ definita come:
		\[
			\bar{P}_{\L, \beta, h, \tilde{\omega}}(A) = P_{\L, \beta, h,
			\tilde{\omega}}(\pi_\L(A \cap B_{\L, \tilde{\omega}})) \qquad 
			\forall A \in \mathcal{B}(\Omega)
		\]
		Assendo questa associazione canonica, chiameremo l'estensione ancora
		$P_{\L, \beta, h, \tilde{\omega}}$ senza la barra.
	\item Data una successione di misure di probabilità $(\mu_n)_{n = 1}^\infty$
		su $\Omega$ ricordiamo che $\mu$ è il limite debole delle $\mu_n$ se per
		ogni $f \in \mathcal{C}(\Omega)$ funzione continua (e quindi limitata
		essendo $\Omega$ compatto) vale
		\[
			\lim_{n \to +\infty} \int_\Omega f\d \mu_n = \int_{\Omega}f \d \mu
		\]
		e scriviamo $\mu = w-\lim_{n \to +\infty}\mu_n$.
		Definiamo quindi
		\[
			\mathcal{G}_{\beta, h}^0 = \{P \in \mathcal{M}(\Omega):\exists \L_n \uparrow \Z^d\;\text{tale che}\;
			P = w-\lim P_{\L_n, \beta, h, \tilde{\omega}(\L_n)}\}
		\]
		ovvero l'insieme di limiti deboli di misure di Gibbs su volumi finiti
		che tendono a tutto lo spazio. Definiamo infine le \textbf{misure di Gibbs a
		volume infinito} $\mathcal{G}_{\beta, h}$ come la chiusura dell'inviluppo convesso
		di $\mathcal{G}_{\beta, h}^0$.
\end{itemize}

L'obiettivo è trovare qualcosa sulla struttura dell'insieme $\mathcal{G}_{\beta,
h}$, in particolare arriveremo a dimostrare il seguente teorema.

\begin{thm}[Struttura di $\mathcal{G}_{\beta, h}$]\label{Gbh}~\begin{itemize}
		\item Per ogni $\beta > 0$ e $h \in \R$, esistono i limiti deboli
			\[
				P_{\beta, h, +} = w-\lim_{\L \to \Z^d}P_{\L, \beta, h, +} \qquad
				P_{\beta, h, -} = w-\lim_{\L \to \Z^d}P_{\L, \beta, h, -} 			
			\]
			e sono invarianti per traslazione.
		\item $P_{\beta, h, +} = P_{\beta, h, -}$ se e solo esiste
			$\partial\psi(\beta, h) / \partial h$. Quindi in particolare per $h
			\neq 0$ e per $h = 0$ e $0 < \beta < \beta_c$. In questo caso
			definiamo $P_{\beta, h} = P_{\beta, h, +} = P_{\beta, h, -}$. 
		\item Se esiste $\partial \psi(\beta, h) / \partial h$, allora
			$\mathcal{G}_{\beta, h}$ è composto di una sola misura $P_{\beta,
			h}$.
		\item Se $\partial \psi(\beta, h) / \partial h$ non esiste (quindi ad
			esempio se $h = 0$ e $\beta > \beta_c$) allora $P_{\beta, 0, +} \neq
			P_{\beta, 0, -}$, infatti
			\[
				\int_\Omega \omega_0P_{\beta, 0, +} = m(\beta, +) > 0 > m(\beta,
				-) = \int_\Omega\omega_0P_{\beta, 0, -}
			\]
			quindi in particolare $\mathcal{G}_{\beta, h}$ contiene tutte le
			misure $P_{\beta, \lambda} = \lambda P_{\beta, 0, +} + (1 -
			\lambda)P_{\beta, 0, -}$ con $\lambda \in [0,1]$.
	\end{itemize}
\end{thm}

Osserviamo che l'ultimo punto del teorema lega la struttura di
$\mathcal{G}_{\beta, 0}$ con le transizioni di fase. In particolare avviene
magnetizzazione spontanea (nel senso di $m(\beta, +) > 0$) se e solo se siamo a
basse temperature ($\beta > \beta_c$), che è equivalente all'esistenza di più di
un limite debole in $\mathcal{G}_{\beta, 0}$. 

Dimostreremo questo teorema un pezzo alla volta, attraverso una serie di lemmi.
Il primo dà un criterio affinché una successione di misure su
$\Omega$ ammetta limite debole.

\begin{lemma}\label{conv-mis}
	Sia $(\mu_n)_{n = 1}^{+\infty}$ una successione di misure di probabilità su
	$\Omega$ tale che, per ogni funzione $f_B \in \mathcal{C}(\Omega)$ della forma
	\[
		f_B(\omega) = \prod_{i \in B} \frac{1}{2}(1 + \omega_i)
	\]
	esiste il limite $\lim_n \int f_B\d\mu_n$. Allora $\mu_n$ ammette limite
	debole $\mu$.
\end{lemma}

\begin{proof}
	Supponiamo di avere una successione
	$(\mu_n)_{n = 1}^{+\infty}$ di misure di probabilità su $\Omega$ tali che per
	ogni $f \in \mathcal{C}(\Omega)$ esiste 
	\[
		L(f) = \lim_{n \to \infty} \int_{\Omega}f \d \mu_n
	\]
	Questi limiti definiscono quindi un operatore lineare e continuo $L$ sullo
	spazio delle funzioni continue e limitate con le norma del $\sup$. Per il
	teorema di rappresentazione di Riesz abbiamo che esiste una misura $\mu$ di
	probabilità per cui
	\[
		\int_\Omega f \d\mu = L(f)
	\]
	Quindi in particolare $\mu = w-\lim \mu_n$.

	Vogliamo adesso restringere la classe di funzioni su cui controllare che esiste
	il limite di $\int f\d \mu_n$. Osserviamo che per definire l'operatore $L$ è
	sufficiente farlo su un denso (essendo lineare e continuo, è anche uniformemente
	continuo, quindi si estende alla chiusura) e anzi, su un insieme di generatori
	il cui span è denso.

	Per il teorema di Stone-Weierstrass le funzioni della forma:
	\[
		f_B'(\omega) = \prod_{i \in B} w_i
	\]
	generano un denso (lo spazio vettoriale generato è un'algebra e contiene le
	costanti). Ma lo spazio vettoriale generato dalle funzioni $f_B'$ e dalle
	funzioni $f_B$ è lo stesso e questo conclude. 
\end{proof}

Possiamo ora utilizzare questo lemma per controllare la convergenza delle misure
$P_{\L, \beta, h, \pm}$. La proprietà fondamentale della classe di funzioni
$f_B$ è di essere non decrescenti rispetto all'ordinamento parziale su $\Omega$
($\omega \leq \omega'$ se e solo se $\omega_i \leq \omega_i' \; \forall i \in
\Z^d$) e questo permetterà di applicare la disuguaglianza FKG.

\begin{lemma}\label{gibbs1}
	Sia $\L_n \uparrow \Z^d$ una successione crescente di domini finiti. Dato
	$B \subset \Z^d$ finito, sia $n(B)$ intero tale che $B \subset \L_n$ per
	ogni $n \geq n(B)$. Siano inoltre $\beta > 0$ e $h \in \R$. Allora per $n
	\geq n(B)$ le successioni $\langle f\rangle_{\L_n, \beta, h, +}$ e $\langle 
	f\rangle_{\L_n, \beta, h, -}$ sono rispettivamente non crescente e non
	decrescente, quindi in particolare ammettono entrambe limite.
\end{lemma}

\begin{proof}
	Sia $\L \subset \L'$. Allora $\langle f_B\rangle_{\L, \beta, h, +}$ si ottiene
	da $\langle f_B\rangle_{\L', \beta, h, +}$ facendo tendere il campo nei punti
	$i \in \L'\backslash\L$ a $+\infty$ e lasciandolo invariato in $\L$. Quindi per la
	disuguaglianza FKG il valore atteso aumenta, essendo $f_B$ non decrescente.
	La dimostrazione è analoga per condizioni al bordo negative.
\end{proof}

Abbiamo quindi mostrato che le misure $P_{\beta, h, +}$ e $P_{\beta, h, -}$
esistono e sono in $\mathcal{G}_{\beta, h}$. Mostriamo gli ultimi due lemmi da
cui seguirà il Teorema~\ref{Gbh}.

\begin{lemma}\label{gibbs2}
	Siano $\beta > 0$, $h \in \R$ e $B\subset \Z^d$ finito. 
	\begin{itemize}
		\item Sia $k \in \Z^d$. Allora $\langle f_B\rangle_{\beta, h, +} = 
			\langle f_{B + k} \rangle_{\beta, h, +}$ e analogamente per
			condizioni al bordo negative.
		\item Il valore atteso $\langle f_B \rangle_{\beta, h, +}$ è continuo a
			destra in $h$, mentre $\langle f_B \rangle_{\beta, h, -}$ è continuo
			a sinistra in $h$.
		\item Per ogni ipercubo simmetrico $\L \supset B$ e qualunque
			condizione esterna $\tilde{\omega}$ vale che
			\[
				\langle f_B \rangle_{\L, \beta, h, -} \leq 
				\langle f_B \rangle_{\L, \beta, h, \tilde{\omega}} \leq 
				\langle f_B \rangle_{\L, \beta, h, +} 
			\]
		\item $0 \leq \langle f_B \rangle_{\beta, h, +} - \langle f_B \rangle_{
			\beta, h, -} \leq |B|(\langle \omega_0 \rangle_{\beta, h, +} -
			\langle \omega_0 \rangle_{\beta, h, -})$
	\end{itemize}
\end{lemma}

\begin{proof}
	~\begin{itemize}
		\item Per quanto dimostrato nel lemma precedente esistono i limiti
			\[
				\langle f_B\rangle_{\beta, h, +} = \lim_{\L \uparrow \Z^d} \langle
				f_B \rangle_{\L,\beta,h,+} \qquad 
				\langle f_{B+k} \rangle_{\beta,h,+} = \lim_{\L \uparrow \Z^d} \langle
				f_{B+k} \rangle_{\L,\beta,h,+}
			\]
			
			Inoltre, essendo l'interazione $\J(i - j)$ invariante per
			traslazione vale che
			\[
				\langle f_B\rangle_{\L, \beta, h, +} = \langle
				f_{B+k}\rangle_{\L + k, \beta, h, +}
			\]
			per ogni $\L$ che contiene $B$. Ma allora consideriamo una sequenza
			di ipercubi $\L_n \uparrow \Z^d$ tali che
			\[
				\bar{\L}_1 \subseteq \bar{\L}_2 + k \subseteq \bar{\L}_3
				\subseteq \bar{\L}_4 + k \subseteq \ldots
			\]
			e definiamo $\L_n = \bar{L}_n$ se $n$ dispari e $\L_n = \bar{L}_n + k$
			se $n$ pari. Allora $\lim_{n \to \infty} \langle f_B\rangle_{\L_n,
			\beta, h, +}$ esiste e, confrontando questo limite lungo i pari ed i
			dispari si ottiene proprio
			\[
				\langle f_B\rangle_{\beta, h, +} = \langle f_{B + k} \rangle_{\beta, h, +}
			\]
			Per condizioni al bordo negative la dimostrazione è analoga.

		\item Sia $B \subset \L$. Per quanto visto nello scorso lemma $\langle
			f_B \langle_{\beta, h, +} \leq \langle f_B \rangle_{\L, \beta, h,
			+}$, quindi
			\[
				\limsup_{h \to h_0^+} \langle f_B \rangle_{\beta, h, +} \leq
				\lim_{h \to h_0^+} \langle f_B \rangle_{\beta, h, +} = \langle
				f_B \rangle_{\L, \beta, h_0, +}
			\]
			Passando al limite per $\L \to \Z^d$ si ottiene una disuguaglianza.
			Per l'altra disuguaglianza abbiamo che, per FKG $\langle f_B
			\rangle_{\beta, h_0, +} \leq \langle f_B \rangle_{\beta, h, +}$ per
			ogni $h \geq h_0$. Da cui
			\[
				\liminf_{h\to h_0^+} \langle f_B \rangle_{\beta, h, +} \geq
				\langle f_B \rangle_{\beta, h_0, +}
			\]
			Mettendo insieme le due disuguaglianze otteniamo
			\[
				\lim_{h \to h_0^+} \langle f_B \rangle_{\beta, h, +} = \langle
				f_B\rangle_{\beta, h_0, +}
			\]

		\item Essendo $f_B$ crescente, basta applicare la disuguaglianza FKG,
			dopo aver osservato che il campo su ogni vertice $i \in \L$ è
			\[
				h_i = h + \sum_{j \in \L^c} \J(i - j)\tilde{\omega}_j
			\]
			che è crescente in $\omega$.
		
		\item Consideriamo la funzione 
			\[
				f(\omega) = \sum_{i \in B}\omega_i - f_B(\omega)
			\]
			Questa è ancora crescente, quindi per FKG se $\L \supset B$ allora
			\[
				0 \leq \langle f_B \rangle_{\L, \beta, h, +} - \langle f_B \rangle_{
				\L, \beta, h, -} \leq \sum_{i \in B} \{\langle
			\omega_i\rangle_{\L, \beta, h, +} - \langle
			\omega_i\rangle_{\L, \beta, h, -}\}
			\]
			e questo conclude passando al limite per $\L \uparrow \Z^d$
			osservando che $\langle \omega_i \rangle_{\beta, h, \pm} = \langle
			\omega_0\rangle_{\beta, h, \pm}$.

	\end{itemize}
\end{proof}

\begin{lemma}\label{gibbs3}~\begin{itemize}
		\item Se $\beta > 0$ e $h \neq 0$, allora
			\[
				\langle \omega_0 \rangle_{\beta, h, +} = m(\beta, h) = 
				-\frac{\partial \psi(\beta, h)}{\partial h} = 
				\langle \omega_0 \rangle_{\beta, h, -}
			\]
		\item Se $\beta > 0$ e $h = 0$, allora
			\[
				\langle \omega_0 \rangle_{\beta, h, +} = m(\beta, +) = 
				-\frac{\partial \psi(\beta, h)}{\partial h^+} \geq 0 \qquad
				\langle \omega_0 \rangle_{\beta, h, -} = m(\beta, -) = 
				-\frac{\partial \psi(\beta, h)}{\partial h^-} \leq 0 \qquad
			\]
	\end{itemize}
	Per cui si ha magnetizzazione spontanea se e solo se $\langle \omega_0
	\rangle_{\beta, h, +} = m(\beta, +) > 0$.
\end{lemma}

\begin{proof}
	Per ogni $\epsilon > 0$ esiste ipercubo $\L_\epsilon$ simmetrico rispetto all'origine
	abbastanza grande tale per cui $\langle \omega_0 \rangle_{\L_\epsilon, \beta, h, +} \leq 
	\langle \omega_0 \rangle_{\beta, h, +} + \epsilon$. 
	Consideriamo quindi un altro ipercubo simmetrico $\L \supset \L_\epsilon$
	ancora simmetrico e definiamo $B_\epsilon(\L) = \{i \in \L: \L_\epsilon + i
	\subset \L\}$. Fissato $i$, la sequenza $\{\langle \omega_0 \rangle_{\L,
	\beta, h, +}\}_\L$ è non crescente per $\L \uparrow \Z^d$. Preso quindi $i
	\in B_\epsilon(\L)$ abbiamo le seguenti disuguaglianze:
	\[
		\langle \omega_0\rangle_{\beta, h, +} =  
		\langle \omega_i\rangle_{\beta, h, +} \leq 
		\langle \omega_i\rangle_{\L, \beta, h, +} \leq 
		\langle \omega_i\rangle_{\L_\epsilon + i, \beta, h, +} =
		\langle \omega_0\rangle_{\L_\epsilon, \beta, h, +} \leq
		\langle \omega_0\rangle_{\beta, h, +} + \epsilon
	\]
	Sommando su $B_\epsilon(\L)$ si ottiene
	\[
		\langle \omega_0\rangle_{\beta, h, +} \leq 
		\frac{1}{|B_\epsilon(\L)|}\sum_{i \in B_\epsilon(\L)} 
		\langle \omega_i\rangle_{\L, \beta, h, +} \leq 
		\langle \omega_0\rangle_{\beta, h, +} + \epsilon
	\]
	Osservando adesso che $|\L|^{-1}|B_\epsilon(\L)| \to 1$, abbiamo che
	\[
		\lim_{\L \uparrow \Z^d}\frac{1}{|\L|}M(\L, \beta, h, +) = \lim_{\L \uparrow \Z^d}\frac{1}{|\L|}\sum_{i \in \L} \langle
		\omega_i\rangle_{\L, \beta, h, +} = \langle \omega_0 \rangle_{\beta, h,
		+}
	\]
	per ogni $h \in \R$. Se adesso $h \neq 0$, allora il limite sopra è proprio
	$m(\beta, h)$ e questo dimostra il primo punto. Per dimostrare il secondo
	punto è sufficiente sfruttare che $\langle \omega_0\rangle_{\beta, h, +}$ è
	continuo a destra in $h$:
	\[
		m(\beta, +)  = \lim_{h \to 0^+} m(\beta, h) = \lim_{h \to 0^+} \langle
		\omega_0\rangle_{\beta, h, +} = \langle \omega_0\rangle_{\beta, 0, +}
	\]
	Il caso opposto è analogo.

\end{proof}

Possiamo ora concludere la dimostrazione del Teorema~\ref{Gbh}.

\begin{proof}~\begin{itemize}
		\item Per la convergenze delle misure $P_{\L, \beta, h, \pm}$ è sufficiente
			utilizzare il criterio del lemma~\ref{conv-mis} insieme alla verifica del
			lemma~\ref{gibbs1}. L'invarianza per traslazione segue dal primo punto del
			lemma~\ref{gibbs2} (è sufficiente verificarla su un denso).
		\item Se $\partial \psi(\beta, h) / \partial h$ esiste, allora per il
			lemma~\ref{gibbs3} vale che $\langle \omega_0\rangle_{\beta, h, +} =
			\langle \omega_0\rangle_{\beta, h, -}$, quindi per l'ultimo punto
			del lemma~\ref{gibbs2} $\langle f_B\rangle_{\beta, h, +} =
			\langle f_B\rangle_{\beta, h, -}$ quindi le misure coincidono.
			Viceversa se le due misure coincidono, allora in particolare $\langle \omega_0\rangle_{\beta, h, +} =
			\langle \omega_0\rangle_{\beta, h, -}$ quindi per il
			lemma~\ref{gibbs3} $\partial \psi(\beta, h) / \partial h$ esiste.
		\item Abbiamo mostrato che se $\partial \psi(\beta, h) / \partial h$
			esiste allora $P_{\beta, h, +} = P_{\beta, h, -}$ quindi per il
			terzo punto del lemma~\ref{gibbs2}, $\mathcal{G}_{\beta, h}$ è
			composto di una sola misura (tutti i limiti coincidono).
		\item Per l'ultimo punto non c'è nulla di nuovo da dimostrare.
	\end{itemize}
\end{proof}
