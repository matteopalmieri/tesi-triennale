\section{Magnetizzazione spontanea: dimostrazione di Peierls}

Nelle sezioni precedenti abbiamo ricavato condizioni equivalenti affinché
avvenga magnetizzazione spontanea, ma non è stato detto nulla sul fatto che la
temperatura critica $\beta_c$ sia finita o meno. Concludiamo il capitolo quindi
mostrando come al variare dell'interazione $\J$ per una larga parte dei modelli
ci sia effettivamente magnetizzazione spontanea (quindi $\beta_c < +\infty$).

\begin{thm}
	Sia $\J$ una funzione di interazione sommabile su $\Z^d$ con $d \geq 2$ e
	$\J_0 = \sum_{k \in \Z^d}\J(k)$. Definiamo \textbf{temperatura inversa critica}
	\[
		\beta_c = \sup\{\beta > 0: m(\beta, +) = 0\}
	\]
	Assumiamo esistano due vettori unitari indipendenti $i, j$ tali
	che $\J(i)$ e $\J(j)$ siano positivi. Allora $\beta_c < +\infty$.
\end{thm}

Un esempio fondamentale è quello in dimensione due in cui $\J(i) = \tilde{J}$
costante se $i$ ha norma uno, zero altrimenti. Questo è detto \textbf{modello di
Ising} e rappresenta il caso in cui l'interazione si ha solo tra coppie di siti
vicini. Riportiamo di seguito una dimostrazione combinatorica del teorema di sopra dovuta a
Peierls che riconduce ogni modello con interazioni non nulla lungo almeno due
direzioni al modello di Ising.  

\begin{proof}
	Sia $\tilde{J} \leq \min{(\J(i), \J(j))}$ una costante positiva.
	Consideriamo il modello di Ising nel piano generato da $i, j$ con intensità
	$\tilde{J}$. Questo ha una funzione di interazione puntualmente minore o
	uguale a $\tilde{J}$, quindi se dimostriamo che $\beta_c < +\infty$ per
	il modello di Ising, otteniamo come conseguenza che $\beta_c < +\infty$
	anche per il modello con $\tilde{J}$ (Proposizione~\ref{prop:magn_prop}).

	Chiamiamo $m_2(\beta, h)$ la magnetizzazione relativa al modello di Ising.
	Per il lemma precedente sappiamo che 
	\[
		m_2(\beta, +) = \langle \omega_0 \rangle_{\beta, 0, +} = \lim_{\L
		\uparrow \Z^d} \langle \omega_0 \rangle_{\L, \beta, 0, +}
	\]
	Basta quindi mostrare che $\langle \omega_0 \rangle_{\L, \beta, 0, +}$ è
	limitato dal basso da una costante positiva. Osserviamo che
	\[
		\langle \omega_0 \rangle_{\L, \beta, 0, +} = P_{\L, \beta, h,
		+}(\omega_0 = +1) - P_{\L, \beta, h, +}(\omega_0 = -1) = 1 - 2P_{\L, \beta, h, +}(\omega_0 = -1)
	\]
	Basta quindi far vedere che esiste $\delta > 0$ tale per cui
	\[
		P_{\L, \beta, h, +}(\omega_0 = -1) \leq \frac{1}{2} - \delta
	\]
	L'idea fondamentale è di associare ad ogni configurazione $\omega \in
	\Omega_\L$ con $\omega_0 = -1$, un cammino chiuso di cui stimare la
	probabilità. Dato $\L \subset \Z^2$ un quadrato simmetrico rispetto
	all'origine, indichiamo con $\partial \L$ l'insieme dei siti $j \in \L^c$
	che sono a distanza 1 da $\L$. A causa delle condizioni al bordo positive,
	abbiamo che $\omega_j = +1$ per ogni $j \in \L^c$. Tracciamo quindi dei
	segmenti orizzontali o verticali che separano i siti con spin positivo da
	quelli con spin negativo in $\L \cup \partial \L$, in modo da ottenere
	percorsi chiusi. Nei punti in cui si incotrano 4 segmenti, è sufficiente
	"smussare gli angoli" in corrispondenza dei due siti con spin negativo, come
	in figura.

	\input{chapters/ising/peierls-draw.tex}

	A causa delle condizioni al bordo, se $\omega_0 = -1$, allora il sito
	centrale sarà racchiuso da un certo cammino $\gamma$ di lunghezza $l$.
	Indichiamo con $\Gamma_l$ l'insieme di tutti i possibili cammini chiusi
	attorno all'origine di lunghezza $l$. Allora
	\begin{equation}\label{ineq:peierls}
		P_{\L, \beta, 0, +}(\omega_0 = -1) \leq \sum_{l =
		1}^{+\infty}\sum_{\gamma \in \Gamma_l} P_{\L, \beta, 0, +}(\omega \;
		\text{contiene}\; \gamma)
	\end{equation}
	
	Mostriamo ora due disuguaglianze che ci permetteranno di concludere.
	\begin{itemize}
		\item La prima è una stima del numero di cammini di una lunghezza
			fissata. In particolare $|\Gamma_l| \leq l 3^l$. Infatti preso un
			cammino che contiene l'origine, o questo contiene il segmento
			verticale subito a destra dell'origine o una sua traslazione
			orizzontale lo contiene (quindi ho un fattore $l$). Inoltre i
			percorsi di lunghezza $l$ che passano subito a destra dell'origine
			sono sicuramente al più $3^l$ dato che per ogni nuovo tratto del
			percorso ci sono 3 possibili direzioni tra cui scegliere.
		\item La seconda disuguaglianza mostra quanto è poco probabile trovare
			un percorso fissato $\gamma$ di lunghezza $l$. Vale infatti che 
			\[
				P_{\L, \beta, 0, +}(\omega \; \text{contiene}\; \gamma) \leq
				\exp{(-l\beta\tilde{J})}
			\]
			Per mostrarlo confrontiamo $\omega$ con un'altra configurazione
			$\bar{\omega}$ di energia più bassa, quindi più probabile. Definiamo
			\[
				\bar{\omega}_i = 
				\begin{cases}
					\omega_i & \text{se} \;i\; \text{è al di fuori di}\; \gamma
					\\
					- \omega_i & \text{se} \;i\; \text{è all'interno di}\; \gamma
				\end{cases}
			\]
			Ricordando che l'hamiltoniana vale
			\[
				H_{\L, 0, \beta, +}(\omega) = -\frac{1}{2}\sum_{i, j \in \L}
				\J(i-j)\omega_i\omega_j - \sum_{i \in \L, j \in \L^c}
				\J(i-j)\omega_i \tilde{\omega}_j
			\]
			Modificando $\omega$ in $\bar{\omega}$, per ogni $i, j$ separati da
			un segmento di $\gamma$ il termine $\omega_i\omega_j$ aumenta di 2,
			quindi complessivamente l'hamiltoniana diminuisce di $\tilde{J}l$.
			Se ora definiamo $\Omega_{\L, \gamma}$ l'insieme degli $\omega$ che
			contengono il cammino $\gamma$, otteniamo che 
			\begin{align*}
				P_{\L, \beta, 0, +}(\omega \;\text{contiene}\; \gamma) &  = 
				\frac{\sum_{\omega \in \Omega_{\L, \gamma}} \exp(-\beta
				H(\omega))}{\sum_{\omega \in \Omega_\L} \exp(-\beta
				H(\omega))} \leq  \\
				& \leq \exp{(-\beta \tilde{J}l)}
				\frac{\sum_{\omega \in \Omega_{\L, \gamma}} \exp(-\beta
				H(\bar{\omega}))}{\sum_{\omega \in \Omega_\L} \exp(-\beta
				H(\omega))} \leq 
				\exp{(-\beta \tilde{J}l)}
			\end{align*}
		
		Grazie alla \eqref{ineq:peierls} e alle due disuguaglianze appena
		mostrate possiamo concludere che
		\[
			P_{\L, \beta, 0, +}(\omega_0  = -1) \leq \sum_{l = 1}^{+\infty}
			\sum_{\gamma \in \Gamma_l} P_{\L, \beta, 0, +}(\omega \;
			\text{contiene}\; \gamma) \leq 
			\sum_{l = 1}^{+\infty} l3^l \exp{(-\beta \tilde{J} l)}
		\]
		e per $\beta$ abbastanza alto (quindi a temperature abbastanza basse)
		l'ultima serie è minore di $1/2$ e questo conclude.

	\end{itemize}

\end{proof}
