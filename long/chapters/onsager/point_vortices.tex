\section{Dinamica dei vortici puntiformi}

Quello che facciamo adesso è considerare la vorticità come concentrata in un
numero finito di punti distinti $x_i(t)\in\Omega$ che chiameremo \emph{vortici
puntiformi} e sfrutteremo le equazioni della sezione precedente per ricavare una dinamica di
questi punti. Consideriamo quindi la vorticità della forma $\omega =
\sum_{i=1}^N \lambda_i\delta_{x_i(t)}$. Abbiamo che il flusso generato è
\begin{equation}
	\psi(x, t) = \int_\Omega V(x, y)\omega(y)dy = \sum_{i = 1}^N \lambda_i V(x,
	x_i(t))
\end{equation}
da cui ricaviamo che il campo di velocità è
\begin{equation}\label{eq:vel}
	u(x) = \nabla^\perp_x \psi(x) = \sum_{i = 1}^n \lambda_i
	\begin{pmatrix}
		-\partial_2V(x, x_i) \\
		\partial_1V(x, x_i)
	\end{pmatrix}
\end{equation}
\begin{es}
	Consideriamo $\Omega = \R^2$ ed un unico punto di vorticità in 0 di
	intensità 1. In questo caso 
	\begin{equation}
		u(x) = \nabla^\perp \left(\frac{1}{2\pi}\log{|x|}\right) = \frac{(-x_2,
		x_1)}{x_1^2 + x_2^2}
	\end{equation}
	\comment{
		\begin{center}
			\begin{tikzpicture}
				\begin{axis}[
					xmin = -1, xmax = 1,
					ymin = -1, ymax = 1,
					zmin = 0, zmax = 1,
					axis equal image,
					% xtick distance = 1,
					% ytick distance = 1,
					view = {0}{90},
					scale = 1.25,
					title = {\bf Vector Field $F = [-y,x]$},
					height = 7cm,
					xlabel = {$x$},
					ylabel = {$y$},
					% colormap/viridis,
					% colorbar,
					% colorbar style = {
					% 	ylabel = {Vector Length}
					% }
				]
					\addplot3[
						point meta = {sqrt(x^2+y^2)},
						quiver = {
							u = {-y/(x^2+y^2)},
							v = {x/(x^2+y^2)},
							scale arrows = 0.025,
						},
						% quiver/colored = {mapped color},
						-stealth,
						domain = -1:1,
						domain y = -1:1,
					] {0};
				\end{axis}
			\end{tikzpicture}
		\end{center}
	}
\end{es}
Vorremmo calcolare la velocità dei singoli vortici come $\dot{x}_i(t) = u(x_i)$,
ma questa non è ben definita dalla \eqref{eq:vel} a causa del termine di
autointerazione $-1/(2\pi\log{|x_i - x_i|})$ in $V(x_i, x_i)$. Trascuriamo
quindi questo termine, mentre manteniamo il termine $\tilde{\gamma}(x_i, x_i)$
({\color{red} dovrebbe dare l'interazione del vortice con il bordo}). Abbiamo
quindi le equazioni della dinamica dei vortici:
\begin{equation}\label{eq:point_din}
	\dot{x}_i 
	= \sum_{j\neq i} \lambda_j
	\begin{pmatrix}
		-\partial_2V(x_i, x_j) \\
		\partial_1V(x_i, x_j)
	\end{pmatrix}
	+ \lambda_i
	\begin{pmatrix}
		-\partial_2\tilde{\gamma}(x_i, x_i) \\
		\partial_1\tilde{\gamma}(x_i, x_i)
	\end{pmatrix}
	= \sum_{j\neq i} \lambda_j
	\begin{pmatrix}
		-\partial_2V(x_i, x_j) \\
		\partial_1V(x_i, x_j)
	\end{pmatrix}
	+ \lambda_i
	\begin{pmatrix}
		-\partial_2\gamma(x_i) \\
		\partial_1\gamma(x_i)
	\end{pmatrix}
\end{equation}
dove definiamo $\gamma(x) = \tilde{\gamma}(x,x) / 2$. Consideriamo il caso in
cui i vortici abbiano tutti la stessa intensità $\lambda_j = \lambda$ e
definiamo
\begin{equation}
	\H(x_1, \ldots, x_N) = \frac{1}{2}\sum_{i\neq j}^N V(x_i, x_j) + \sum_{i =
	1}^N \gamma(x_i)
\end{equation}
con questa è possibile riscrivere la \eqref{eq:point_din} come 
\begin{equation}
	\begin{pmatrix}
		(x_i)_1(t) \\
		(x_i)_2(t)
	\end{pmatrix}' = 
	\begin{pmatrix}
		-\partial (\lambda\H)/\partial (x_i)_2 \\
		\partial (\lambda\H)/\partial (x_i)_1
	\end{pmatrix} 
\end{equation}
Abbiamo quindi ottenuto un sistema Hamiltoniano con Hamiltoniana $\lambda\H$.
Quello che vorremo fare è studiare limiti di $\omega = \omega_N$ per $N$ molto
grande e con lo spirito di studiare proprietà statistiche del moto di questi
vortici definiamo la misura di Gibbs a fissata temperatura inversa
$\tilde{\beta}\in\R$
\begin{equation}
	\mu^N(x_1, \ldots, x_N) = \frac{1}{Z}\exp{(-\tilde{\beta}\lambda\H(x_1,
	\ldots, x_N))}
\end{equation}
con $Z$ la funzione di partizione
\begin{equation}
	Z(\tilde{\beta}) = \int_{\Omega^N}\exp{(-\tilde{\beta}\lambda\H)}\d
	x_1\ldots\d x_N
\end{equation}
Restringiamoci ora al caso di domini $\Omega$ limitati. Osserviamo che saremo
interessati ai $\tilde{\beta}$ per cui $Z < +\infty$ ed in
tal caso $\mu^N$ è una densità di probabilità invariante dato che dipende solo
dall'Hamiltoniana ({\color{red} devo dimostrarlo?}). Questo sarà vero anche per
$\tilde{\beta} < 0$ quindi $\tilde{\beta}$ perde il significato fisico di
inverso della temperatura. Vale in effetti il seguente
\begin{thm}\label{thm:partition_function}
	~\begin{itemize}
		\item[(i)] $Z < +\infty$ se e solo se
			$\tilde{\beta}\lambda\in(-\frac{8\pi}{N}, 4\pi)$. 
		\item[(ii)] Fissato $N\tilde{\beta}\lambda\in(-8\pi, +\infty)$,
			$Z<+\infty$ definitivamente ed in particolare $Z^{\frac{1}{N}}$ è
			definitivamente limitato.
	\end{itemize}
\end{thm}

Questo teorema suggerisce di mantenere costante $N\tilde{\beta}\lambda = \beta$ quando
passeremo al limite per $N\to+\infty$ con $\beta \in (-8\pi, +\infty)$. Una
scelta naturale per fare questo è porre $\tilde{\beta} = \beta$ ed $\lambda =
1/N$, mantenendo in qualche modo l'intensità totale della vorticità $N\lambda =
1$ costante.

Quello che andiamo a studiare adesso è fissare un $j$ naturale e studiare la
distribuzione $\rho_j^N$ dei primi $j$ vortici con $N \gg j$ (detta
\emph{funzione di correlazione}). Questa è definita
da integrando $\mu^N$ rispetto alle ultime $N-j$ componenti
\begin{equation}
	\rho_j^N(x_1, \ldots, x_j) = \int_{\Omega^{N-j}}\d x_{j+1}\ldots\d x_N
	\mu^N(x_1, \ldots, x_N)
\end{equation}
Nel seguito sarà importante la seguente stima
\begin{thm}\label{thm:corr_func_estimate}
	Sia $\beta \in (-8\pi, +\infty)$, allora $\forall j \geq 1$ abbiamo che
	\begin{equation}
		\rho_j^N(x_1, \ldots, x_j) \leq C^j\exp{\left(-\frac{\beta}{N}\H(x_1,
			\ldots, x_j)\right)}
	\end{equation}
\end{thm}
Concludiamo la sezione con la dimostrazione dei due teoremi
\ref{thm:partition_function} e \ref{thm:corr_func_estimate}.

{\color{red} SCRIVERE LE DUE DIMOSTRAZIONI}
