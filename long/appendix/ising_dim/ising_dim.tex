\chapter{Le disuguaglianze GKS 1-2, di Percus, GHS e FKG}

Mostriamo le disuguaglianze enunciate nel Teorema~\ref{thm:ising-ineq}.

\begin{proof}(GKS-1) 	

	Abbiamo che 
	\[
		\langle \omega_A \rangle = \frac{1}{Z}\int_{\Omega_\L} 
		\omega_A\exp{\left(\frac{\beta}{2}\sum_{i, j \in \L}
		J_{i,j}\omega_i\omega_j + \sum_{i \in \L} \beta h_i
		\omega_i\right)}\pi_\L P_\rho(\d \omega) 
	\]
	Sviluppando l'esponenziale in serie di Taylor e sfruttando l'indipendenza
	degli spin nei singoli stati rispetto alla misura prodotto, osserviamo che è
	sufficiente verificare che integrali della forma
	\[
		\int_{\{-1, 1\}} \omega_i^k \rho(\d \omega_i)
	\]
	siano non negativi. Ma questo integrale è nullo se $k$ è dispari e positivo
	se $k$ è pari.

\end{proof}

Per mostrare le disuguaglianze GKS-2 e Percus abbiamo bisogno di un lemma
preliminare.

\begin{lemma}

	Consideriamo un doppio sistema di spin tra loro non interagenti sullo spazio
	della configurazioni $\Omega_\L \times \Omega_\L$ la cui Hamiltoniana è
	quindi
	\begin{gather*}
		H(\omega, \sigma) = H(\omega) + H(\sigma) = 
		-\frac{1}{2}\sum_{i, j \in \L} J_{i,j} (\omega_i \omega_j +
		\sigma_i\sigma_j) - \sum_{i \in \L} h_i (\omega_i + \sigma_i)
	\end{gather*}
	Da cui abbiamo la misura di Gibbs
	\[
		P(\d \omega \d \sigma) = \frac{1}{Z}\exp{(-\beta H(\omega,
		\sigma))}\pi_\L P_\rho(\d \omega) \pi_\L P_\rho(\d \sigma)
	\]
	e indichiamo con $\langle \cdot \rangle^{(2)}$ il valore atteso rispetto a
	questa misura. Definiamo inoltre il cambio di base $t_i = (\omega_i +
	\sigma_i) / \sqrt{2}$ e $q_i = (\omega_i - \sigma_i) / \sqrt{2}$. Allora vale che
	\begin{itemize}
		\item se $h_i \geq 0$ per ogni $i$ e $A,B$ sono due sottoinsiemi di $\L$
			non vuoti, allora $\langle q_A t_B\rangle^{(2)} \geq 0$;
		\item per $h_i \in \R$ arbitrari ed $A$ non vuoto abbiamo che $\langle
			q_A \rangle^{(2)} \geq 0$. 
	\end{itemize}

\end{lemma}

\begin{proof}

	~\begin{itemize}
		\item Riscrivendo l'Hamiltoniana nella nuova base in termini di $t_i$ e
			$q_i$ abbiamo 
			\begin{gather*}
				H(\omega, \sigma) = -\frac{1}{2}\sum_{i, j} J_{i,j}
				\left(\frac{t_i + q_i}{\sqrt{2}}\frac{t_j +
				q_j}{\sqrt{2}} + \frac{t_i - q_i}{\sqrt{2}}\frac{t_j -
				q_j}{\sqrt{2}}\right) - \sum_i h_i\left(\frac{t_i +
				q_i}{\sqrt{2}} + \frac{t_i - q_i}{\sqrt{2}}\right) = \\
				=  -\frac{1}{2}\sum_{i,j} J_{i,j}(t_it_j + q_iq_j) -
				\sqrt{2}\sum_i h_it_i
			\end{gather*}
			Come prima possiamo sostituire questa formula nel valore atteso
			\[
				\langle q_A t_B\rangle^{(2)} = \frac{1}{Z}\int_{\Omega_\L \times
				\Omega_\L} q_At_B \exp{(-\beta H(\omega, \sigma))}\pi_\L P_\rho(\d \omega) \pi_\L P_\rho(\d \sigma)
			\]
			e sviluppare l'esponenziale in serie di Taylor. Ci siamo quindi
			ricondotti a dimostrare che per ogni $i \in \L$ ed $m, n$ interi
			non negativi
			\[
				\int_{\{-1, 1\}\times\{-1, 1\}}q_i^mt_i^n\rho(\d
				\omega_i)\rho(\d \sigma_i) = \int_{\{-1, 1\}\times\{-1, 1\}}
				\left(\frac{\omega_i +
				\sigma_i}{\sqrt{2}}\right)^m\left(\frac{\omega_i -
				\sigma_i}{\sqrt{2}}\right)^n\rho(\d
				\omega_i)\rho(\d \sigma_i) \geq 0
			\]
			Ma questi sono nulli se sia $m > 0$ ed $n > 0$ e sono
			rispettivamente nulli o positivi se uno dei due è nullo e l'altra è
			rispettivamente dispari o pari.

		\item Analogamente possiamo riscrivere $\langle q_A \rangle^{(2)}$ come 
			\[
				\frac{1}{Z}\int_{\Omega_\L \times
					\Omega_\L} q_A \exp{\left(\frac{\beta}{2}\sum_{i,j} J_{i,j}
						q_iq_j\right)}\exp{\left(\frac{\beta}{2}\sum_{i,j} J_{i,j}t_it_j + \beta
				\sqrt{2}\sum_i h_it_i\right)}\pi_\L P_\rho(\d \omega) \pi_\L P_\rho(\d \sigma)
			\]
			Sviluppando in serie di Taylor solo il primo esponenziale, abbiamo
			che è sufficiente mostrare che per ogni sequenza in interi non
			negativi $m_i, i \in \L$:
			\[
				\int_{\Omega_\L \times \Omega_\L} \prod_{i \in \L} 
				q_i^{m_i} \exp{\left(\frac{\beta}{2}\sum_{i,j} J_{i,j}t_it_j + \beta
				\sqrt{2}\sum_i h_it_i\right)} \prod_{i \in \L} \rho(\d
				\omega_i)\rho(\d \sigma_i)
			\]
			Per simmetria l'integrale è nullo a meno che tutti gli $m_i$ sono
			pari, nel qual caso è positivo.

	\end{itemize}

\end{proof}

\begin{proof}(GKS-2)

	Possiamo riscrivere $\langle \omega_A\omega_B \rangle - \langle \omega_A
	\rangle\langle \omega_B\rangle$ (considerando il sistema $(\omega, \sigma)$
	di spin doppi del lemma) come
	\[
		\langle \omega_A\omega_B - \omega_A \sigma_B\rangle^{(2)} =
		\left\langle\prod_{i \in A} \left(\frac{t_i +
			q_i}{\sqrt{2}}\right)\left[\prod_{i \in B} \left(\frac{t_i +
	q_i}{\sqrt{2}}\right) - \prod_{i \in B} \left(\frac{t_i -
	q_i}{\sqrt{2}}\right)\right] \right\rangle^{(2)} 
	\]
	Espandendo le produttorie in $B$, tutti i termini con coefficiente negativo
	nella produttoria di $\prod_{i \in B}(t_i - q_i)/\sqrt{2}$ vengono
	cancellati con quelli corrispondenti in $\prod_{i \in B} (t_i +
	q_i)/\sqrt{2}$. Il termine di destra è quindi la somma di valori attesi di
	polinomi in $t_i$ e $q_i$ con coefficienti positivi e quindi è positivo per
	il lemma.

\end{proof}

\begin{proof}(Percus)

	La disuguaglianza di Percus segue immediatamente dalla seconda parte del lemma osservando
	che 
	\[
		\langle \omega_i \omega_j \rangle - \langle \omega_i \rangle \langle
		\omega_j \rangle = \frac{1}{2}\langle \omega_i\omega_j +
		\sigma_i\sigma_j - \omega_i \sigma_j - \omega_j \sigma_i\rangle^{(2)} =
		\langle q_iq_j \rangle^{(2)} \geq 0
	\]

\end{proof}

\begin{proof}(GHS)

	La disuguaglianza GHS si dimostra in modo analogo al lemma prendendo questa
	volta un sistema quadruplo di spin non interagenti su $\Omega_\L^4$ con
	Hamiltoniana
	\[
		H(\omega, \sigma, \omega', \sigma') = H(\omega) + H(\sigma) + H(\omega')
		+ H(\sigma')
	\]
	Indichiamo con $\langle \cdot \rangle^{(4)}$ il valore atteso rispetto alla
	misura di Gibbs associata a questa Hamiltoniana. Questa volta considerando il cambio di coordinate ortogonale
	\[
		\begin{pmatrix}
			\alpha_i \\
			\beta_i \\
			\gamma_i \\
			\delta_i 
		\end{pmatrix} = \frac{1}{2}
		\begin{pmatrix}
			1 & 1 & 1 & 1 \\
			1 & 1 & -1 & -1 \\
			1 & -1 & 1 & -1 \\
			-1 & 1 & 1 & -1 
		\end{pmatrix}
		\begin{pmatrix}
			\omega_i \\
			\sigma_i \\
			\omega_i' \\
			\sigma_i'
		\end{pmatrix}
	\]
	è possibile riscrivere l'Hamiltoniana come
	\[
		H(\omega, \sigma, \omega', \sigma') =
		-\frac{1}{2}\sum_{i,j}J_{i,j}(\alpha_i\alpha_j + \beta_i\beta_j +
		\gamma_i\gamma_j + \delta_i\delta_j) - 2\sum_i h_i\alpha_i
	\]
	Analogamente a prima vogliamo mostrare che per ogni $A, B, C, D$ non vuoti
	abbiamo che $\langle \alpha_A \beta_B \gamma_C \delta_D \rangle^{(4)} \geq
	0$. Esattamente come nel lemma, espandendo in serie di potenze
	l'esponenziale, è sufficiente mostrare che sono positivi gli integrali della
	forma
	\[
		\int_{\{-1,1\}^4} \alpha_i^k \beta_i^l \gamma_i^m \delta_i^n \rho(\d
		\omega_i) \rho(\d \sigma_i) \rho(\d \omega_i') \rho(\d \sigma_i')
	\]
	Con $k, l, m, n$ interi non negativi. Sempre per simmetria, se non hanno
	tutti la stessa parità, allora l'integrale è nullo. Se sono tutti pari,
	l'integrando è positivo e questo conclude. Se sono tutti dispari è
	sufficiente osservare che 
	\[
		\alpha_i \beta_i \gamma_i \delta_i = \frac{1}{4}(\omega_i\sigma_i -
		\omega_i'\sigma_i')^2
	\]
	quindi l'integrando è ancora sempre non negativo e questo conclude. La
	disuguaglianza GHS segue dall'osservazione che 
	\[
		\ev{(\omega_i - \ev{\omega_i})(\omega_j - \ev{\omega_j})(\omega_k -
		\ev{\omega_k})} = - \langle (\alpha_k + \beta_k)(\gamma_i\delta_j +
		\gamma_j\delta_i)\rangle^{(4)}
	\]

\end{proof}

\begin{proof}(FKG)

	Mostriamo la prima disuguaglianza ($\ev{fg} - \ev{f}\ev{g}\geq 0$) per
	induzione su $n = |\L|$. 

	Per $n = 1$ osserviamo che la correlazione tra $f$ e $g$ si può riscrivere
	come 
	\[
		\ev{fg} - \ev{f}\ev{g} = \int_{\Omega_\L \times \Omega_\L} 
		(f(\omega) - f(\tilde{\omega})) (g(\omega) - g(\tilde{\omega}))P(\d \omega) P(\d \tilde{\omega})
	\]
	Dato che lo spazio ha dimensione 1, allora $\omega$ e $\tilde{\omega}$ sono
	sempre confrontabili ed inoltre $f(\omega) - f(\tilde{\omega})$ e
	$g(\omega)-g(\tilde{\omega})$ hanno lo stesso segno essendo $f$ e $g$ non
	decrescenti. Quindi l'integrando è non negativo. 

	Consideriamo ora $\L$ con $|\L| = n > 1$ e assumiamo l'ipotesi induttiva per
	$1, \ldots, n-1$. Sia $\alpha \in \L$ un qualunque sito e partizioniamo $\L$
	in $\{\alpha\} \cup (\L \backslash \{\alpha\})$. Scriviamo quindi ogni
	$\omega \in \Omega_\L$ come $\omega = (\omega', \omega_\alpha)$ con $\omega'
	\in \Omega_{\L \backslash \{\alpha\}}$ e $\omega_\alpha \in \{-1, +1\}$.
	Osserviamo che l'Hamiltoniana si può riscrivere come
	\[
		H(\omega', \omega_\alpha) = -\frac{1}{2}\sum_{i,j \in \L
			\backslash \{\alpha\}} J_{i,j}\omega_i\omega_j - \sum_{i \in \L
			\backslash \{\alpha\}} \left(h_i + \frac{1}{2}(J_{i,\alpha} + J_{\alpha,
			i}) \omega_\alpha\right)\omega_i - J_{\alpha, \alpha} -
			h_\alpha\omega_\alpha
	\]

	Fissato allora $\omega_\alpha$ definiamo le misure di probabilità su
	$\Omega_{\L \backslash \{\alpha\}}$ 
	\[
		\nu_{\omega_\alpha}(\d \omega') = \exp{(-H(\omega', \omega_\alpha))}\pi_{\L
		\backslash\{\alpha\}}P_\rho(\d \omega') \frac{1}{Z(\omega_\alpha)} 
	\]
	Abbiamo quindi ottenuto due misure $\nu_1$ e $\nu_{-1}$. Applicando
	l'ipotesi induttiva su $n-1$ otteniamo che 
	\begin{gather}\label{ineq:fkg-1}
		\ev{fg} = \int_{\{-1, +1\}} \rho(\d \omega_\alpha)
		\frac{Z(\omega_\alpha)}{Z}\int_{\Omega_{\L\backslash\{\alpha\}}}
		f(\omega', \omega_\alpha)g(\omega', \omega_\alpha)\nu_{\omega_\alpha}(\d
		\omega') \nonumber\\ \geq 
		\int_{\{-1, +1\}}\rho(\d \omega_\alpha)
		\frac{Z(\omega_\alpha)}{Z}\phi_f(\omega_\alpha)\phi_g(\omega_\alpha)
	\end{gather}
	Dove $\phi_f(\omega_\alpha) = \int f(\omega', \omega_\alpha)
	\nu_{\omega_\alpha}(\d \omega')$ e analogamente per $\phi_g$. Se ora
	sapessimo che $\phi_f$ e $\phi_g$ sono non decrescenti in $\omega_\alpha$,
	allora avremmo che (applicando FKG con $n = 1$ all'ultimo termine della
	\eqref{ineq:fkg-1})
	\[
		\ev{fg} \geq \int_{\{-1, +1\}}\rho(\d \omega_\alpha)
		\frac{Z(\omega_\alpha)}{Z}\phi_f(\omega_\alpha)
		\int_{\{-1, +1\}}\rho(\d \omega_\alpha)
		\frac{Z(\omega_\alpha)}{Z}\phi_g(\omega_\alpha) = \ev{f}\ev{g}
	\]
	Resta quindi da mostrare che $\phi_f$ e $\phi_g$ sono non decrescenti,
	ovvero che $\phi_f(-1) \leq \phi_f(1)$ e analogamente per $\phi_g$.
	Osserviamo che la definizione di $\nu_{\omega_\alpha}$ può essere estesa a
	$\nu_t$ con $t$ reale in $[-1, +1]$. Se ora sapessimo che la funzione $t
	\mapsto \int f(\omega', 1) \nu_t(\omega')$ è non descrescente, allora
	avremmo che 
	\begin{gather*}
		\phi_f(-1) = \int_{\Omega_{\L \backslash\{\alpha\}}} f(\omega',
			-1)\nu_{-1}(\d \omega') \leq \int_{\Omega_{\L \backslash
			\{\alpha\}}} f(\omega', 1) \nu_{-1}(\d \omega') \leq 
			\int_{\Omega_{\L \backslash
			\{\alpha\}}} f(\omega', 1) \nu_1(\d \omega') = \phi_f(1)
	\end{gather*}
	Concludiamo quindi osservando che
	\begin{align*}
		\frac{\d}{\d t} \int_{\Omega_{\L \backslash\{\alpha\}}} f(\omega',
		1)\nu_t(\d \omega') = & \int_{\Omega_{\L \backslash\{\alpha\}}} f(\omega',
		1) \left(-\frac{\partial H(\omega', t)}{\partial t}\right)\nu_t(\d
		\omega') - \\ & -\int_{\Omega_{\L \backslash\{\alpha\}}} f(\omega',
		1)\nu_t(\d \omega') \int_{\Omega_{\L \backslash\{\alpha\}}} \left(-\frac{\partial H(\omega', t)}{\partial t}\right)\nu_t(\d
		\omega')
	\end{align*}
	Dove l'ultimo termine è esattamente la correlazione tra le due funzioni non
	decrescenti $f(\omega', 1)$ e $-\partial H(\omega', 1) / \partial t =
	\sum_{i \in \L \backslash \{\alpha\}} \frac{1}{2}(J_{i, \alpha} +
	J_{\alpha,i}) \omega_i + h_\alpha$ e quindi è non negativa per ipotesi
	induttiva. Questo mostra la prima parte dell'enunciato.

	La seconda parte è $\ev{f}_{h_i} \leq \ev{f}_{h_i'}$ se $f$ è non
	descrescente e $h_i\leq h_i'$ per ogni $i \in \L$. Con un semplice conto si
	ottiene che 
	\[
		\frac{\partial H}{\partial h_i} = -\omega_i \qquad \text{e}\qquad
		\frac{1}{Z}\frac{\partial Z}{\partial h_i} = \ev{\omega_i}
	\]
	Da cui otteniamo che
	\[
		\frac{\partial \ev{f}}{\partial h_i} = \ev{f\cdot \omega_i} -
		\ev{f}\ev{\omega_i} \geq 0
	\]
	Dove la disuguaglianza è vera essendo $f$ e $\omega_i$ non decrescenti.
	Questo conclude anche la seconda parte dell'enunciato.





\end{proof}
