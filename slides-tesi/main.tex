\documentclass[9pt]{beamer}

% packages
\usepackage[utf8]{inputenc}
% \usepackage{animate}
% \usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage{amsmath, amssymb, amsfonts, amsthm}
\usepackage{bbold}
\usepackage{tikz}
\usetikzlibrary{animations}
\usetikzlibrary{positioning}
% \usepackage[
%	backend=biber, 
%	maxbibnames = 10,
% ]{biblatex} 
% \addbibresource{main.bib}
\usepackage{graphicx}
% \usepackage{multimedia}
% \usepackage{cancel}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{subcaption}

% commands
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\renewcommand{\H}{\mathcal{H}}
\renewcommand{\d}{\text{d}}
\renewcommand{\L}{\Lambda}
\definecolor{darkspringgreen}{rgb}{0.09, 0.45, 0.27}
\definecolor{green(html/cssgreen)}{rgb}{0.0, 0.5, 0.0}
\usetheme{Boadilla}

\title[Grandi deviazioni e modello di Ising]{Teoria delle grandi deviazioni \\ e
modello di Ising}
\institute[Unipi]{Università di Pisa \\ Tesi di Laurea Triennale
% \\ \includegraphics[height = 0.25\textheight]{img/sns-stemma.jpg}
}
% \subtitle{Un approccio di meccanica statistica}
\author[Matteo Palmieri]{Matteo Palmieri \\
	Relatori: Prof. Franco Flandoli, \\
	\qquad \qquad \; Dr. Alessandra Caraceni}
\date{23 Settembre 2022}

% \setbeamercovered{transparent}
\setbeamercovered{invisible}
\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{sections/subsections in toc}[circle]

\begin{document}

\tikzstyle{every picture}+=[remember picture]

\begin{frame}
	\titlepage
\end{frame}

% \frame{\tableofcontents}

\section{Teoria delle grandi deviazioni e misura di Gibbs}

\begin{frame}{Presentazione del problema}
	
	Meccanica statistica:
	\begin{itemize}
		\item<2-> sistemi fisici con {\color{red} \textbf{tanti}}
			gradi di libertà;
		\item<3-> ci interessa {\color{blue} \textbf{comportamento
			macroscopico}}.
	\end{itemize}

	\uncover<4->{
		\begin{block}{Esempio fondamentale}
			Gas di $n$ particelle non interagenti confinate a muoversi in un
			parallelepipedo $\L \subset \R^3$. Lo stato del sistema è descritto
			dalle posizioni e dalle velocità delle particelle
			\[
				(x_1, \ldots, x_n, v_1, \ldots, v_n) \in \L^n \times \R^{3n}.
			\]
		\end{block}
	}

\end{frame}

\begin{frame}{Presentazione del problema}
	
	Cosa vogliamo studiare?

	\uncover<2->{%
		\textbf{Osservabili:} funzioni $f(x, v):\L \times \R \to \R$.
		Ad esempio l'energia $U(x, v) = v^2 / 2$.

		\textbf{Comportamento macroscopico:} guardiamo alla media degli
		osservabili
		\[
			F_n = \frac{1}{n}\sum_{i = 1}^n f(x_i, v_i) \qquad 
			\left(\text{ad esempio}\; U_n = \frac{1}{n}\sum_{i = 1}^n
			\frac{v_i^2}{2}\right)
		\]
	}

	\uncover<3->{Possibili approcci:}
	\begin{itemize}
		\item<4-> {\color{red}\textbf{approccio "deterministico":}} misuriamo posizione e
			velocità iniziali di ogni particella e risolviamo le equazioni del
			moto. {\color{red}Non è praticabile ($n \sim 10^{24}$)}
		\item<5-> {\color{green(html/cssgreen)}\textbf{approccio "probabilistico":}} consideriamo le
			condizioni iniziali aleatorie, guardiamo la distribuzione di
			probabilità degli osservabili e vediamo se convergono a qualcosa.
	\end{itemize}

\end{frame}

\begin{frame}{Le distribuzioni di probabilità}

	Pensiamo sempre al gas e fissiamo l'energia media $U_n$ attorno ad un valore $u \in \R^+$.
	\uncover<2->{Come scegliamo a caso la condizione iniziale $\omega = (x_1, \ldots,
		x_n, v_1, \ldots, v_n)$?}


	\begin{itemize}
		\item<3-> \textbf{ensemble microcanonico:} distribuzione uniforme tra quelle di energia fissata $U_n \sim u$ \[ \mathbb{P}_{n, u} \propto \mathbb{1}_{\{U_n \in [u - \epsilon,
				u+ \epsilon]\}}\d x_1 \cdots \d x_n\d v_1 \cdots \d v_n
			\]
		\item<4-> \textbf{ensemble canonico (misura di Gibbs):} scegliamo un
			parametro $\beta = \beta(u)$ e definiamo
			\[
				\mathbb{P}_{n, \beta} \propto \exp{(-\beta U_n(\omega))}\d x_1 \cdots \d x_n\d v_1 \cdots \d v_n
			\]


	\end{itemize}

	\uncover<5->{\begin{block}{Teorema}
			I due ensemble sono equivalenti nel limite per $n \to +\infty$.
	\end{block}}

	\uncover<6->{%
		Per dimostrarlo ci servono alcuni risultati preliminari...
	}

\end{frame}

\begin{frame}{Teoria delle grandi deviazioni}
	
	\begin{block}{Definizione (informale)}
		\begin{itemize}
			\item $\mathcal{X}$ metrico, $(\Omega, \mathcal{F}, \mathbb{P})$ spazio di probabilità;
			\item $L_1, L_2, \ldots$ variabili aleatorie, $L_n:\Omega \to
				\mathcal{X}$.
			\item $I:\mathcal{X} \to [0, +\infty]$ {\color{blue}\textbf{rate
				function}}
		\end{itemize}
		$\{L_n\}_{n \in \N}$ soddisfa una {\color{blue}
		\textbf{proprietà di grandi deviazioni (LDP)}} se
		\[
			\lim_{n \to +\infty} \frac{1}{n}\log\mathbb{P}(L_n \in E) = -\inf_{x \in
			E} I(x).
		\]

	\end{block}

	\uncover<2->{\color{green(html/cssgreen)} \textbf{Moralmente:}}
	\begin{itemize}
		\item<2->[\textcolor{green(html/cssgreen)}{\textbullet}] 
			\[
				\mathbb{P}(L_n \sim x) \sim e^{-nI(x)}
				\tikz[remember picture]{\node[] (a) {};}
				\;\text{o}
				\;
				\mathbb{P}(L_n \in E) \sim e^{-n \inf_{x \in E} I(x)};
			\]
		\item<3->[\textcolor{green(html/cssgreen)}{\textbullet}] i punti che minimizzano $I$ sono detti 
			{\color{green(html/cssgreen)} punti di equilibrio} ($I(x) = 0$), danno il
			comportamento tipico;
		\item<3->[\textcolor{green(html/cssgreen)}{\textbullet}] le variabili $L_n$ tendono a concentrarsi attorno ai punti di
			equilibrio (fenomeno di {\color{green(html/cssgreen)} concentrazione della
			misura});
		\item<4->[\textcolor{green(html/cssgreen)}{\textbullet}] LDP $\Longrightarrow \mathbb{P}(\text{Comportamento lontano da
			quello tipico})$ esponenzialmente bassa.
	\end{itemize}

	\uncover<5->{%
		\begin{tikzpicture}[overlay]
			\node[rectangle, above right = 0.35cm and 0.15cm of a, color =
				black, text width = 4cm, fill=red!30] (b)
				{\color{black}\small $I(x)$ misura quanto $x$ non è un punto di equilibrio};
			\draw[->, red] (6.1, 3.8)..controls (5.7, 3.6).. (5.5, 3.2);
		\end{tikzpicture}
	}
\end{frame}

\begin{frame}{Teoria delle grandi deviazioni - probabilità condizionata}
	
	Setting:

	\begin{itemize}
		\item<2-> $\{L_n\}_{n \in \N}$ soddisfano una LDP (rate function $I(x)$);
		\item<3-> assumiamo di sapere qualcosa in più, ovvero $L_n \in E \subseteq
			\mathcal{X}$. Definiamo
			\[
				\mathbb{Q}_n(\d \omega) = \mathbb{P}(\d \omega | L_n \in E)
			\]
			le probabilità condizionate;
	\end{itemize}

	\uncover<4->{%
		\begin{block}{Teorema: LDP condizionata}
			$\{L_n\}_{n \in \N}$ da $(\Omega, \mathcal{F}, \mathbb{Q}_n)$ a
			$\mathcal{X}$ soddisfano una LDP con rate function $I^E(x)$:
			\[
				I^E(x) = 
				\begin{cases}
					I(x) - \inf_{y \in E} I(y) & \text{se} \; x \in E; \\
					+\infty & \text{altrimenti}.
				\end{cases}
			\]
		\end{block}
	}

	\uncover<5->{%
		\begin{center}
			\begin{tikzpicture}[remember picture]
				\node[rectangle, draw = black, fill = red!30, text width = 5cm,
					anchor = center] () at (0, 0) {%
						\begin{minipage}{\textwidth}
							{\color{red}Punti di equilibrio: }
							\begin{align*}
								I^E(x) = 0 \iff & x \in E \;\text{e}\\ 
									& I(x) = \inf_{y \in E}I(y)
							\end{align*}
						\end{minipage}};
			\end{tikzpicture}
		\end{center}
	}

\end{frame}

\begin{frame}{Teoria delle grandi deviazioni - Teorema di Sanov}

	\begin{itemize}
		\item $X_1, X_2, \ldots$ variabili aleatorie i.i.d.\ (con legge $\rho$), $X_n:\Omega \to
			\mathcal{X}$;
		\item $\mathcal{M}(\mathcal{X})$ insieme delle misure di probabilità su
			$\mathcal{X}$. 
			\[
				\text{\color{blue} \textbf{Misure empiriche:}}\qquad L_n(\omega, \d x) = \frac{1}{n}\sum_{i = 1}^n
				\delta_{X_i(\omega)}(\d x)
			\]
	\end{itemize}

	\uncover<2->{%
		\begin{block}{Teorema: Legge dei Grandi Numeri}
			
			$L_n(\omega, \cdot) \Longrightarrow \rho(\cdot)$ per $\mathbb{P}-$q.o.
			$\omega$ (convergenza debole di misure).

		\end{block}
	}

	\uncover<3->{%
		In realtà vale di più:

		\begin{block}{Teorema: Sanov}

			Le variabili $L_n$ soddisfano una LDP con rate function
			\[
				I(\mu) = D_{KL}(\mu \| \rho) = \int_{\mathcal{X}}\log{\frac{\d
				\mu}{\d \rho}}\d \mu
			\]
			dove $D_{KL}$ è la divergenza di Kullback-Leibler. $\rho$ è l'unico
			punto di equilibrio, quindi $L_n \Longrightarrow \rho$ esponenzialmente.

		\end{block}
	}

\end{frame}

\begin{frame}{Equivalenza tra microcanonico e misura di Gibbs}

	\begin{block}{Teorema}
		I due ensemble sono equivalenti nel limite per $n \to +\infty$.
	\end{block}

	\uncover<2->{\textbf{Dimostrazione}}

	\begin{itemize}
		\item<2-> Prendiamo $(X_i, V_i) \sim \mathcal{L}(K)$ i.i.d.;
		\item<3-> leghiamo le misure empiriche all'energia media
			\begin{align*}
				\text{misure empiriche:} \qquad 
				L_n(\omega, \d x \d v) = \frac{1}{n}\sum_{i = 1}^n \delta_{(X_i,
				V_i)}(\d x\d v)
			\end{align*}
			allora:
			\[
				U_n(\omega) = \frac{1}{n}\sum_{i = 1}^n \frac{V_i(\omega)^2}{2} = 
				\int_{\R^{6n}} \frac{v^2}{2}L_n(\omega, \d x \d v) 
			\]
		\item<4-> definiamo 
			\begin{align*}
				& \tilde{E} = [u - \epsilon, u + \epsilon] \\ 
				& E = \left\{\mu \in \mathcal{M}(\R^{6n}): \int \frac{v^2}{2}\d
				\mu \in \tilde{E}\right\}
			\end{align*}
			\begin{tikzpicture}
				\node[] (q) {Quindi: };
				\node[inner sep = 1mm, outer sep = 0, fill = red!50, right = 0cm of q] () {$U_n
					\in \tilde{E} \iff L_n \in E$};
			\end{tikzpicture}
	\end{itemize}

\end{frame}

\begin{frame}

	\begin{itemize}
		\item<2-> \textbf{canonico:} rispetto alla misura di Gibbs $\mathbb{P}_{n,
			\beta}$ le coppie $(X_i, V_i)$ sono i.i.d.\ con legge
			\[
				\rho_\beta \propto \exp{\left(-\beta
				\frac{v^2}{2}\right)}\mathcal{L}(\d x \d v).
			\]
			Per il teorema di Sanov $L_n \overset{\exp}{\to} \rho_\beta$.
		\item<3-> teorema di Sanov (LDP per le $L_n$ rispetto a $\mathcal{L}(\d
			x\d v)$):
			\[
				L_n = \frac{1}{n}\sum_{i = 1}^n \delta_{(X_i, V_i)}(\d x\d v)
				\sim \mathcal{L}(\d x\d v) |_K
			\]

		\item<4-> \textbf{microcanonico:} assumiamo che $U_n \in \tilde{E}$
			(equivalente a dire $L_n \in E$). 

			Allora ho una LDP per la
			probabilità condizionata, con punto di equilibrio
			\[
				\text{argmin}_{\mu \in E} D_{KL}(\mu \|
				\mathcal{L}) = \rho_\beta \propto \exp{\left(-\beta
				\frac{v^2}{2}\right)}\mathcal{L}(\d x\d v) 
			\]

			ovvero per $n$ grande, anche rispetto al microcanonico, $L_n
			\overset{\exp}{\to} \rho_\beta$.
		\item<5-> in entrambi i casi, guardando gli osservabili, si ha
			\[
				F_n(\omega) = \frac{1}{n}\sum_{i = 1}^n f(X_i(\omega),
				V_i(\omega)) = \int_{\R^{6n}} f(x, v)
				L_n(\omega, \d x\d v) \overset{\exp}{\to} \int_{\R^{6n}} f(x, v) \rho_\beta(\d x
				\d v)
			\]
	\end{itemize}
	\uncover<5->{\qed}

\end{frame}
\section{Modello di Ising}

\begin{frame}{Modello di Ising}

	{\color{red}\textbf{Obiettivo:}} avvicino una calamita ad una graffetta. Questa
	rimane  magnetizzata \\ \hspace{1.6cm} anche dopo l'allontanamento della calamita. Come mai?
	
	\uncover<2->{%
		{\color{blue}\textbf{Modello:}}
		\begin{itemize}
			\item reticolo di elettroni $\Lambda
				\subset \Z^d$ con spin $\pm 1$, stato $\omega \in \{-1,
				+1\}^\Lambda$;
			\item campo magnetico $h \in \R$ diretto verso l'alto; 
			\item condizioni al bordo $\tilde{\omega} \in \{-1, +1\}^{\Lambda^c}$.
		\end{itemize}
		
		\begin{figure}
			\begin{center}
				\begin{tikzpicture}[scale = 0.8, outer sep = 0cm, inner sep = 0cm]
					\draw[dashed, step=.6cm, very thin, gray, shift = {(-2.1, -2.1)}] (0, 0) grid (4.2,
						4.2);
					\node[circle, draw=blue!50, fill=blue!20, inner sep = 0] at (0,0)
						{\color{blue} $-$};
					\foreach \i in {-3, -2, -1, 0, 1, 2, 3}{
						\node at (\i*0.6, 2.4) {\color{black} $+$};
						\node at (\i*0.6, -2.4) {\color{black} $+$};
						\node at (2.4, \i*0.6) {\color{black} $+$};
						\node at (-2.4, \i*0.6) {\color{black} $+$};
					}
					\def\m{
						{1,1,1,-1,-1,1,-1},
						{1,1,-1,1,-1,1,1},
						{-1,-1,1,-1,1,1,1},
						{1,-1,-1,-1,1,1,1},
						{-1,1,1,-1,1,1,1},
						{-1,-1,-1,1,-1,-1,1},
						{1,-1,1,-1,-1,-1,1},
					}
					\foreach[count = \i] \row in \m{
						\foreach[count = \j] \cell in \row{
							\ifnum\cell = 1
								\node[circle, draw=red!50, fill=red!20, inner sep = 0] at 
									(\i*0.6 - 2.4, \j*0.6 - 2.4) {\color{red} $+$};
							\else
								\node[circle, draw=blue!50, fill=blue!20, inner sep = 0] at 
									(\i*0.6 - 2.4, \j*0.6 - 2.4) {\color{blue} $-$};
							\fi
						}
					}
				\end{tikzpicture}
			\end{center}
		\end{figure}

		{\color{blue} \textbf{Hamiltoniana:}} $H(\omega) = -\sum_{i \sim j} \omega_i \omega_j -
			h \sum_{i \in \L} \omega_i - \sum_{i \sim j}\omega_i \tilde{\omega}_j$
	}
	
\end{frame}

\begin{frame}{Modello di Ising}
	
	Definiamo:
	\begin{itemize}
		\item<2-> Misura prodotto uniforme $\mathbb{P}(\omega) = 2^{-|\Lambda|}$
		\item<3-> {\color{blue}Misura di Gibbs} $\mathbb{G}(\omega) \propto \exp{(-\beta
			H(\omega))}\mathbb{P}(\omega)$
		\item<4-> {\color{blue}Magnetizzazione}
			\[
				M = M(\L, \beta, h, \tilde{\omega}) = \mathbb{E}\left(\sum_{i
				\in \Lambda} \omega_i\right)
			\]
		\item<5-> Limite per reticoli {\color{red}grandi}:
			\[
				m(\beta, h) = \lim_{\L \uparrow \Z^d} \frac{1}{|\L|}M(\L, \beta, h,
				\tilde{\omega})
			\]
	\end{itemize}

	\uncover<6->{%
		\begin{figure}
			\begin{center}
				\begin{tikzpicture}[domain=-3:3]
					\draw[->] (-3,0) -- (3,0) node[right] {$h$};
					\draw[->] (0, -1.5) -- (0, 1.5) node[above] {$m$};
					\draw[dashed] (-3, 1) -- (3, 1);
					\draw[dashed] (-3, -1) -- (3, -1);
					\draw[samples = 100, color = red] plot (\x, {(rad atan(\x)) * 2 / pi}) node[right] {};
					\draw[samples = 100, dashed] plot (\x, {(rad atan(2 * \x)) * 2 / pi})
						node[right] {};
					\draw[samples = 100, domain = 0:3, color = blue] plot (\x, {(rad atan(3* \x + 1)) * 2 / pi}) node[right] {};
					\draw[samples = 100, domain = -3:0, color = blue] plot (\x, {(rad atan(3* \x - 1)) * 2 / pi}) node[right] {};
					\node at (-0.2,1.2) {1};
					\node at (-0.3,- 1.2) {$-1$};
					\node at (3.8, 1.2) {\color{blue} $\beta > \beta_c$};
					\node at (3.8, 0.7) {\color{red} $\beta < \beta_c$};
					\node<7->[fill = blue!50] (mb1) at (-1, 0.5) {$m(\beta, +)$};
					\node<7->[] (mb2) at (0, 0.5) {};
					\draw<7->[->] (mb1) -- (mb2);
					\draw<7->[fill = blue!50] (0, 0.5) circle (0.05cm);
				\end{tikzpicture}
			\end{center}
		\end{figure}
	}

\end{frame}

\begin{frame}{Modello di Ising}

	\begin{block}{Teorema}
		Avviene magnetizzazione spontanea a temperature abbastanza basse:
		$\beta_c < +\infty$.
	\end{block}

	\uncover<2->{%
		\begin{block}{Lemma (caratterizzazione di $m(\beta, +)$)}
			Sia $\mathbb{G}_{\L, \beta, 0, +}$ la misura di Gibbs con $h = 0$ e
			condizioni al bordo positive ($\tilde{\omega}_j = +1$).
			\[
				m(\beta, +) = \lim_{\L \uparrow \Z^d} \mathbb{E}_{\mathbb{G}_{\L,
				\beta, 0, +}}(\omega_0)
			\]
		\end{block}
	}

	\uncover<3->{%
		\begin{tikzpicture}[remember picture, overlay]
			\node[rectangle, draw, fill = white] at (current page.center) {%
				\begin{minipage}{0.8\textwidth}
					\begin{figure}
						% \centering
						\begin{subfigure}{0.45\textwidth}
							\centering
							\includegraphics[width=\textwidth]{img/img9-10.png}
							\caption{$\beta < \beta_c$}
							% \vspace{0.5cm}
						\end{subfigure}
						%\hfill
						\begin{subfigure}{0.45\textwidth}
							\centering
							\includegraphics[width=\textwidth]{img/img12-112.png}
							\caption{$\beta > \beta_c$}
							% \vspace{0.5cm}
						\end{subfigure}
					\end{figure}
				\end{minipage}
			};%
		\end{tikzpicture}
	}

\end{frame}

\begin{frame}{Modello di Ising}

	\textbf{Dimostrazione (Peierls):} sia $\mathbb{G} = \mathbb{G}_{\L, \beta,
	0, +}$
	\begin{align*}
		\mathbb{E}(\omega_0) & = \mathbb{G}(\{\omega: \omega_0 = +1\}) -
		\mathbb{G}(\{\omega: \omega_0 = -1\}) = \\ 
		& =  1 - 2\mathbb{G}(\{\omega: \omega_0 = -1\}) \overset{?}{\geq} 1 -
		2\delta \qquad (\iff \mathbb{G}(\{\omega: \omega_0 = -1\})
		\overset{?}{\leq} \delta)
	\end{align*}
	
	\uncover<2->{%
		\begin{figure}
			\begin{center}
				\begin{tikzpicture}[scale = 0.8, outer sep = 0, inner sep = 0]
					\draw[dashed, step=.6cm, very thin, gray, shift = {(-2.1, -2.1)}] (0, 0) grid (4.2,
						4.2);
					\node[circle, draw=blue!50, fill=blue!20, inner sep = 0] at (0,0)
						{\color{blue} $-$};
					\foreach \i in {-3, -2, -1, 0, 1, 2, 3}{
						\node at (\i*0.6, 2.4) {\color{black} $+$};
						\node at (\i*0.6, -2.4) {\color{black} $+$};
						\node at (2.4, \i*0.6) {\color{black} $+$};
						\node at (-2.4, \i*0.6) {\color{black} $+$};
					}
					\def\m{
						{1,1,1,-1,-1,1,-1},
						{1,1,-1,1,-1,1,1},
						{-1,-1,1,-1,1,1,1},
						{1,-1,-1,-1,1,1,1},
						{-1,1,1,-1,1,1,1},
						{-1,-1,-1,1,-1,-1,1},
						{1,-1,1,-1,-1,-1,1},
					}
					\foreach[count = \i] \row in \m{
						\foreach[count = \j] \cell in \row{
							\ifnum\cell = 1
								\node[circle, draw=red!50, fill=red!20, inner sep = 0] at 
									(\i*0.6 - 2.4, \j*0.6 - 2.4) {\color{red} $+$};
							\else
								\node[circle, draw=blue!50, fill=blue!20, inner sep = 0] at 
									(\i*0.6 - 2.4, \j*0.6 - 2.4) {\color{blue} $-$};
							\fi
						}
					}
					\draw<3->[blue, thick] (-0.9, -2.1) -- (-0.3, -2.1) -- (-0.3, -1.5) --
						(0.2, -1.5) -- (0.3, -1.4) -- (0.3, -0.3) --
						(0.8, -0.3) -- (0.9, -0.2) -- (0.9, 0.2) --
						(0.8, 0.3) -- (-0.8, 0.3) -- (-0.9, 0.2) --
						(-0.9, -0.2) -- (-0.8, -0.3) -- (-0.3, -0.3) --
						(-0.3, -0.9) -- (-0.8, -0.9) -- (-0.9, -1.0) --
						(-0.9, -2.1);

					%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					
					\draw<4->[dashed, step=.6cm, very thin, gray, shift = {(-2.1, -2.1)}] (7.2, 0)
						grid (11.2, 4.2);
					\node<4->[circle, draw=blue!50, fill=blue!20, inner sep = 0] at (7.2,0)
						{\color{blue} $-$};
					\foreach \i in {-3, -2, -1, 0, 1, 2, 3}{
						\node<4-> at (7.2 + \i*0.6, 2.4) {\color{black} $+$};
						\node<4-> at (7.2 + \i*0.6, -2.4) {\color{black} $+$};
						\node<4-> at (7.2 + 2.4, \i*0.6) {\color{black} $+$};
						\node<4-> at (7.2 -2.4, \i*0.6) {\color{black} $+$};
					}
					\def\m{
						{1,1,1,-1,-1,1,-1},
						{1,1,-1,1,-1,1,1},
						{1,1,1,1,1,1,1},
						{1,1,1,1,1,1,1},
						{-1,1,1,1,1,1,1},
						{-1,-1,-1,1,-1,-1,1},
						{1,-1,1,-1,-1,-1,1},
					}
					\foreach[count = \i] \row in \m{
						\foreach[count = \j] \cell in \row{
							\ifnum\cell = 1
								\node<4->[circle, draw=red!50, fill=red!20, inner sep = 0] at 
									(7.2 + \i*0.6 - 2.4, \j*0.6 - 2.4) {\color{red} $+$};
							\else
								\node<4->[circle, draw=blue!50, fill=blue!20, inner sep = 0] at 
									(7.2 + \i*0.6 - 2.4, \j*0.6 - 2.4) {\color{blue} $-$};
							\fi
						}
					}
					\draw<4->[blue, thick] (7.2-0.9, -2.1) -- (7.2-0.3, -2.1) -- (7.2-0.3, -1.5) --
						(7.2 + 0.2, -1.5) -- (7.2 + 0.3, -1.4) -- (7.2 + 0.3, -0.3) --
						(7.2 + 0.8, -0.3) -- (7.2 + 0.9, -0.2) -- (7.2 + 0.9, 0.2) --
						(7.2 + 0.8, 0.3) -- (7.2-0.8, 0.3) -- (7.2-0.9, 0.2) --
						(7.2-0.9, -0.2) -- (7.2-0.8, -0.3) -- (7.2-0.3, -0.3) --
						(7.2-0.3, -0.9) -- (7.2-0.8, -0.9) -- (7.2-0.9, -1.0) --
						(7.2-0.9, -2.1);
					%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

					\node<4->[] at (3.6, 0) {$\overset{\omega \to
						\bar{\omega}}{\Longrightarrow}$};
				\end{tikzpicture}
			\end{center}
		\end{figure}
	}
	
	\uncover<5->{%
		\[
			H(\omega) - H(\bar{\omega}) = \ell \Longrightarrow \mathbb{G}(\omega) \leq
			\exp{(-\beta \ell)}\mathbb{G}(\bar{\omega})
		\]
	}	

\end{frame}

\begin{frame}{Modello di Ising}

	Osservazioni:

	\begin{itemize}
		\item<2-> ci sono meno di $\ell\cdot3^\ell$ percorsi lunghi $\ell$ attorno all'origine;
		\item<3-> sia $\gamma$ un percorso lungo $\ell$:
			\[
				\mathbb{G}(\{\omega: \omega \;\text{contiene} \;\gamma\}) \leq 
				\exp{(-\beta \ell)}\mathbb{G}(\{\bar{\omega}: \omega \;
				\text{contiene}\; \gamma\}) \leq \exp{(-\beta \ell)};
			\]
	\end{itemize}
	\uncover<4->{%
		Quindi
		\[
			\mathbb{G}(\{\omega: \omega_0 = -1\}) \leq \sum_{\ell = 1}^{+\infty}
			\ell3^\ell\exp{(-\beta \ell)} \leq \delta 
		\]
		per $\beta$ abbastanza grande.
		\qed
	}

\end{frame}

\begin{frame}{}
	\centering\Huge Grazie per l'attenzione
\end{frame}

\end{document}
