\documentclass[10pt]{beamer}

\input{embed_video.tex}

% packages
\usepackage[utf8]{inputenc}
% \usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage{amsmath, amssymb, amsfonts, amsthm}
\usepackage{tikz}
\usepackage[
	backend=biber, 
	maxbibnames = 10,
]{biblatex} 
\addbibresource{main.bib}
\usepackage{graphicx}
\usepackage{multimedia}
\usepackage{cancel}

% commands
\newcommand{\R}{\mathbb{R}}
\renewcommand{\H}{\mathcal{H}}
\renewcommand{\d}{\text{d}}

\usetheme{Boadilla}

\title[Equazione di Eulero]{Alcune soluzioni dell'equazione di Eulero \\ in dimensione 2}
\subtitle{Un approccio di meccanica statistica}
\author[Matteo Palmieri]{Matteo Palmieri \\
	Relatore: Franco Flandoli}
\institute[SNS]{Scuola Normale Superiore \\ Colloquio della Classe di Scienze
% \\ \includegraphics[height = 0.25\textheight]{img/sns-stemma.jpg}
}
\date{29 Aprile 2022}

\setbeamercovered{transparent}
\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{sections/subsections in toc}[circle]

\begin{document}


\begin{frame}
	\titlepage
\end{frame}

\frame{\tableofcontents}



\section{Introduzione}



\begin{frame}{Il problema}
\only<1>{\begin{figure} %this figure will be at the right
	\centering
	\includegraphics[width=0.5\textwidth]{img/hotspot_cover_1280.jpg}
	\caption{Jupiter’s Great Red Spot}
\end{figure}}
\only<2>{\begin{figure} %this figure will be at the right
	\centering
	\includegraphics[width=0.5\textwidth]{img/hotspot_cover_1280_2.jpg}
	\caption{Jupiter’s Great Red Spot}
\end{figure}}
\begin{itemize}
\item L'obiettivo è capire qualcosa sui fenomeni di turbolenza nei fluidi in due
	dimensioni, in particolare riguardo alla manifestazione di vortici stabili.
\item<2> L'approccio che seguiremo è dovuto inizialmente a L. Onsager.
	Approssimeremo l'equazione dei fluidi (Eulero) con la dinamica di un
	numero finito di punti (\textbf{point vortices}) e a questa
	applicheremo tecniche di meccanica statistica.
\end{itemize}
\end{frame}



\begin{frame}{Il problema}
	\begin{block}{Problema: equazione di Eulero (fluidi incomprimibili).}
		Dato $\Omega \subset \R^2$ aperto limitato regolare, cercare due
		funzioni:
		\begin{itemize}
			\item $u:\Omega \times [0, T)\to\R^2$ campo di velocità di un
				fluido.
			\item $p:\Omega\times [0, T)\to \R$ pressione.
		\end{itemize}
		\uncover<2->{
		che risolvano
		$$\frac{\partial u}{\partial t} + (u\cdot \nabla)u + \nabla p = 0
		\qquad \text{div}(u) = 0$$
		}
		\uncover<3->{con condizioni di Dirichlet al bordo $u\cdot n = 0$, dove $n$ è il
		vettore normale al $\partial \Omega$.}
	\end{block}
\end{frame}



\begin{frame}{La vorticità}
	\begin{block}{Definizione}
		Dato il campo di velocità $u = (u_1, u_2)$ sia $\omega:\Omega\times [0,
		T)\to \R$ la \textbf{vorticità}, definita come
		$$\omega = \text{curl}(u) = \partial_1 u_2 - \partial_2 u_1$$
	\end{block}	
	\only<2>{
	L'equazione di Eulero si può riscrivere in termini della vorticità come:
	$$\tikz[baseline]{\node[draw=red,fill=red!20,anchor=base, rounded
			corners,rectangle,minimum height=1cm, inner sep = 5pt] 
		(d1) {$\displaystyle\frac{\partial\omega}{\partial t} + u\cdot\nabla\omega = 0$}}$$
	}
	\only<3->{
		L'equazione di Eulero si può riscrivere in termini della vorticità come:
		$$\tikz[baseline]{\node[draw=red,fill=red!20,anchor=base, rounded
				corners,rectangle,minimum height=1cm, inner sep = 5pt] 
			(d1) {$\displaystyle\cancel{\frac{\partial\omega}{\partial t}} + u\cdot\nabla\omega = 0$}}$$

		Cercheremo soluzioni stazionarie di questa equazione.
	}
\end{frame}



\begin{frame}{La stream function}
	\begin{block}{Definizione}
		Assumendo $\Omega$ semplicemente connesso, sia $\psi:\Omega\times [0,
		T)\to \R$ la \textbf{stream function}, tale che 
			\[
				u = \nabla^\perp\psi = (-\partial_2\psi, \partial_1\psi) \qquad
				u = \text{curl}(0,0,\psi)
			\] 
	\end{block}
	\begin{itemize}
		\item<2-> Dato che $u \cdot n = 0$, allora $\psi$ è
			costante su $\partial \Omega$ per cui poniamo $\psi = 0$ su $\partial
			\Omega$. 
		\item<3-> $\omega = \Delta\psi$ su $\Omega$ (equazione di Poisson con
			condizioni di Dirichlet).
	\end{itemize}
\end{frame}



\begin{frame}{Vorticità e stream function}
	\begin{itemize}
		\item Vorticità $\omega$ e stream function $\psi$ sono legate dall'equazione di Poisson:
			\[
				\begin{cases}
					\Delta \psi = \omega \;\text{su}\;\Omega \\
					\psi = 0 \;\text{su}\;\partial\Omega
				\end{cases}
			\]
		\item<2-> Da cui:
			\[
				\psi(x, t) = \int_\Omega V(x, y)\omega(y, t)\d y
				\qquad V(x, y) = \frac{1}{2\pi}\log|x - y| + \tilde{\gamma}(x, y)
			\]
			con $V(x, y)$ funzione di Green.
		\item<3-> Definiamo $\gamma(x) = \frac{1}{2}\tilde{\gamma}(x, x)$.
	\end{itemize}
\end{frame}



\section{Point vortices}

\begin{frame}{Dinamica dei point vortices}
	Consideriamo la vorticità concentrata in un numero finito $n$ di punti
	$x_i(t) \in \Omega$, detti \textbf{point vortices} (ognuno con intensità $\frac{1}{n}$):
	\begin{equation}\label{eq:point_vort}
		\omega(x, t) = \sum_{i = 1}^n \frac{1}{n}\delta_{x_i(t)}
	\end{equation}
	\uncover<2->{Imponendo che la vorticità soddisfi l'equazione di Eulero, si ottiene la
		seguente dinamica hamiltoniana per i point vortices $x_i(t)$:
	$$\tikz[baseline]{\node[draw=red,fill=red!20,anchor=base, rounded
			corners,rectangle,minimum height=1.5cm, inner sep = 5pt] 
		(d1) {$x_i(t)' = \displaystyle\sum_{i = 1}^n \frac{1}{n}
		\begin{pmatrix}
			-\partial_2 V(x_i, x_j) \\
			\partial_1 V(x_i, x_j)
		\end{pmatrix} + \frac{1}{n}
		\begin{pmatrix}
			-\partial_2 \gamma(x_i) \\
			\partial_1 \gamma(x_j)
		\end{pmatrix}
	$}}$$
	}
\end{frame}



\section{Misura di Gibbs}

\begin{frame}{Misura di Gibbs}
	\uncover<1->{L'hamiltoniana del sistema dei point vortices è $\mathcal{H}/n$ con:
	\[
		\mathcal{H}(x_1, \ldots, x_n) = \frac{1}{2}\sum_{i \neq j}V(x_i, x_j) +
		\sum_{i = 1}^n \gamma(x_i)
	\]
	}
	\uncover<2->{
		\begin{block}{Definizione}
			Definiamo \textbf{misura di Gibbs} per questa hamiltoniana, la densità
			di probabilità:
			\[
				\mu(x_1, \ldots, x_n) =
				\frac{1}{Z}\exp{\left(-\frac{\beta}{n}\mathcal{H}(x_1, \ldots,
				x_n)\right)}
			\]
			dove la costante di rinormalizzazione
			\[
				Z = \int_{\Omega^n}\exp{\left(-\frac{\beta}{n}\H\right)}\d x_1\cdots \d x_n
			\]
			si dice \textbf{funzione di partizione}.
		\end{block}
	}
\end{frame}



\begin{frame}{Misura di Gibbs}
	\begin{block}{Teorema}
		La misura di Gibbs è ben definita (ovvero $Z < +\infty$) se
		\[
			\beta \in (-8\pi, 4\pi n)
		\]
	\end{block}
	In particolare per $\beta \in (-8\pi, +\infty)$ la misura di Gibbs è ben
	definita per $n$ sufficientemente grande.
	\uncover<2->{
		\begin{block}{Definizione}
			Dato $j < n$ intero, definiamo
			\textbf{funzioni di correlazione} le densità di probabilità:
			\[
				\rho_j^n(x_1, \ldots, x_j) = \int_{\Omega^{n-j}}\mu(x_1, \ldots,
				x_n) \d x_{j+1}\cdots \d x_n
			\]
		\end{block}
	}
\end{frame}



\begin{frame}{Funzioni di correlazione}
	\begin{lemma}
		Vale la seguente disuguaglianza:
		\begin{equation}\label{eq:rho-ineq}
			\rho_j^n(x_1, \ldots, x_j) \leq C^j
			\exp{\left(-\frac{\beta}{n}\H(x_1, \ldots, x_j)\right)}
		\end{equation}
	\end{lemma}
	\uncover<2->{Da cui abbiamo il seguente risultato di compattezza:
	\begin{lemma}
		Esiste una sottosuccessione $n'\to\infty$ e alcune densità di
		probabilità simmetriche $\rho_j$ su $\Omega^j$ per cui
		\begin{equation}
			\forall j \leq 1 \qquad \rho_j^{n'} \rightharpoonup \rho_j
		\end{equation}
		debole come misure e debole $L^p$ per ogni $p \in [1, +\infty)$. Inoltre
		vale che:
		\[
			0 \leq \rho_j(x_1, \ldots, x_j) \leq C^j
		\]
	\end{lemma}
	}
\end{frame}



\begin{frame}{Obiettivo ed euristica}
	Abbiamo ottenuto delle densità di probabilità limite $\rho_j(x_1,\ldots, x_j)$ delle funzioni di
	correlazione.
	\begin{itemize}
		\item<2-> Per ogni $n \geq j$, la funzione $\rho_j^n$ ci dà il
			comportamento secondo la misura di Gibbs dei primi $j$ vortici
			durante la dinamica.
		\item<3-> I limiti $\rho_j$ ci dicono cosa succede a questi $j$ vortici,
			quando il numero totale di vortici $n$ diventa molto maggiore di
			$j$.
		\item<4-> Vedremo come al limite le funzioni $\rho_j$ sono in realtà
			molto semplici e si fattorizzano, ovvero esisterà una $\rho(x)$
			densità di probabilità su $\Omega$ per cui:
			\[
				\forall j \geq 1 \qquad \rho_j(x_1, \ldots, x_j) =
				\rho(x_1)\cdots\rho(x_j)
			\]
		\item<5-> In generale il fatto che le funzioni di correlazione si
			fattorizzino al limite, ci dice che le prime $j$ particelle fissate,
			quando si muovono nel campo di velocità di un numero $n \gg j$
			particelle, hanno un moto praticamente indipendente.
	\end{itemize}
\end{frame}



\begin{frame}{Obiettivo ed euristica}
	\begin{itemize}
		\item Al limite ciascuna particella si muove quindi in modo indipendente da
			ogni altra in un campo di velocità complessivo e secondo una misura
			invariante $\rho(x)$.
		\item<2-> Trovaremo un'equazione per questa densità, detta \textbf{equazione
			di campo medio}.
		\item<3-> $\rho(x)$ non dà informazione solo sul moto del singolo
			vortice, ma si può mostrare che è il limite delle misure empiriche
			date dagli $n$ vortici:
			\[
				\frac{1}{n}\sum_{i = 1}^n \delta_{x_i}(\d x) \to \rho(x)
			\]
			dove la convergenza può essere resa precisa.
		\item<4-> Mostreremo che in effetti $\rho(x)$ è una soluzione
			dell'equazione di Eulero per la vorticità.
	\end{itemize}
\end{frame}



\begin{frame}{Funzioni di correlazione}
	Le funzioni di correlazione limite sono simmetriche e \emph{compatibili} tra loro,
	ovvero per $j < k$ (chiamando $X_j = (x_1, \ldots, x_j)$):
	\[
		\int \rho_k(X_k) \d x_{j+1}\cdots \d x_k = \rho_j(X_j)
	\]
	\uncover<2->{
		Abbiamo detto che quello che vorremmo ottenere è:
		\[
			\int_{\Omega^j}f(X_j)\rho_j(X_j)\d X_j = 
			\int_{\Omega^j}f(X_j)\rho(x_1)\cdots\rho(x_j)\d X_j
		\]
	}
	\uncover<3->{
		\begin{block}{Teorema di Hewitt-Savage}
			Esiste una unica misura di probabilità $\pi$ su $\mathcal{P}(\Omega)$ lo spazio
			delle misure di probabilità su $\Omega$ tale che per ogni $j \geq
			1$ e per ogni $f \in L^\infty(\Omega^j)$
			\[
				\int_{\Omega^j} f(X_j)\rho_j(X_j)\d X_j =
				{\color{red}\int_{\mathcal{P}(\Omega)}\pi(\d \mu)}\int_{\Omega^j}
				f(X_j)\mu(\d x_1)\cdots\mu(\d x_j)
			\]
		\end{block}
	}
\end{frame}



\section{Energia libera}

\begin{frame}{Energia libera}
	Il nostro obiettivo è quindi ridurre il più possibile il supporto di
	$\pi$ fino a che $\pi = \delta_\rho$.
	\uncover<2->{
		Una prima riduzione è la seguente:
		\begin{lemma}
			Il supporto di $\pi$ è contenuto nelle misure assolutamente continue
			rispetto a Lebesgue ($\mu(\d x) = \rho(x) \d x$). In particolare le
			densità $\rho$ nel supporto sono limitate da una comune costante
			$\rho(x) \leq C$.
		\end{lemma}
	}
	\uncover<3->{
	Per ridurre ulteriormente il supporto di $\pi$, cerchiamo un principio
	variazionale che caratterizzi le densità nel supporto.
		\begin{block}{Definizione}
			Dato $N \geq 1, \beta \neq 0$ e $\mu(x_1, \ldots, x_N)$ densità di probabilità su
			$\Omega^N$ con $\mu |\log{\mu}| \in L^1$, definiamo
			l'\textbf{energia libera}
			\[
				F^N(\mu) = \frac{1}{\beta N}\int \mu \log{\mu} \d X_N +
				\frac{1}{N^2}\int \mathcal{H}\mu \d X_N
			\]
		\end{block}
	}
\end{frame}



\begin{frame}{Energia libera}
	Sia $\mu_N$ la misura di Gibbs ($\mu_N = Z^{-1}\exp{-\beta/N\mathcal{H}}$).
	\uncover<2->{
		\begin{block}{Teorema}
			Le misure di Gibbs sono estremali dell'energia libera, ovvero:
			\begin{itemize}
				\item se $0 < \beta < 4\pi N$, allora $\mu_N$ è l'unico punto di
					minimo per $F^N$.
				\item se $-8\pi < \beta < 0$, allora $\mu_N$ è l'unico punto di
					massimo di $F^N$.
			\end{itemize}
		\end{block}
	}
	\uncover<3->{
		\textbf{Dimostrazione}

		Con un semplice conto si ottiene:
		$$F^N(\mu) = F^N(\mu_N) + \frac{1}{\beta N} D_{KL}(\mu || \mu_N)$$
		Dove $D_{KL}(\mu || \mu_N) \geq 0$ è la distanza di Kullback-Leibler.
		Distinguendo in base al segno di $\beta$, si ha la tesi. \qed
	}
\end{frame}



\begin{frame}{Energia libera}
Dato che le funzioni di correlazione $\rho_j$ (da cui deriva la misura $\pi$),
sono ottenute dalle misure di Gibbs, non è strano aspettarsi che anche le
densità di probabilità nel supporto di $\pi$ rispettino un principio
variazionale.
\uncover<2->{
	\begin{block}{Definizione}
		Data $\rho$ misura di probabilità su $\Omega$ con $\rho |\log{\rho}| \in
		L^1$ definiamo una nuova \textbf{energia libera}
		\[
			F(\rho) = \frac{1}{\beta}\int_\Omega \rho \log{\rho}\d x + 
			\frac{1}{2}\int_{\Omega^2}V(x, y) \rho(x) \rho(y)\d x \d y
		\]
	\end{block}
}
\end{frame}



\begin{frame}{Energia libera}
	\begin{block}{Teorema}
		Il supporto di $\pi$ è contenuto nell'insieme dei minimi, se $\beta >
		0$, o massimi, se $\beta < 0$, dell'energia libera $F$.
	\end{block}
	\textbf{Dimostrazione}
	\begin{itemize}
	\item<2->Estendiamo la definizione di energia libera alle misure di
		probabilità $\tilde{\pi}$ sullo spazio delle misure di probabilità su
		$\Omega$ (come la nostra misura $\pi$):
		\[
			\tilde{F}(\tilde{\pi}) = \int_{\mathcal{P}(\Omega)}
			\tilde{\pi}(\d \rho) F(\rho)
		\]
	\item<3-> Sto quindi mediando $F$ sulle misure in $\mathcal{P}(\Omega)$
		tramite $\tilde{\pi}$. $\tilde{F}$ estende $F$ nel senso che se $\tilde{\pi}
		= \delta_\rho$, allora $\tilde{F}(\tilde{\pi}) = F(\rho)$.
	\item<4-> $\tilde{F}$ è un funzionale lineare nelle misure $\tilde{\pi}$
	\end{itemize}
\end{frame}



\begin{frame}
	
	Restringiamoci a $\beta > 0$. Per $\beta < 0$ è analogo con le
	disuguaglianze invertite.
		
	\begin{itemize}
		\item<2-> Sfruttando che $\pi$ deriva dalle misure di Gibbs $\mu_N$, si ottiene
			questa disuguaglianza:
			\[
				\liminf_{N' \to +\infty} F^{N'}(\mu_{N'}) \geq \tilde{F}(\pi)
			\]
		\item<3-> Sfruttando la minimalità delle misure di Gibbs rispetto ai
			funzionali $F^N$, si ottiene che per ogni $\tilde{\pi}$ (compresa
			$\pi$):
			\[
				\limsup_{N'\to +\infty} F^{N'}(\mu_{N'}) \leq
				\tilde{F}(\tilde{\pi})
			\]
		\item<4-> Mettendo insieme le due disuguaglianze si ottiene:
			\[
				\tilde{F}(\pi) = \lim_{N' \to +\infty} F^{N'}(\mu_{N'})
			\]
			\[
				\tilde{F}(\pi) = \min_{\tilde{\pi}} \tilde{F}(\tilde{\pi})
			\]
		\item<5-> In particolare $\tilde{F}(\pi) \leq \tilde{F}(\delta_\rho) =
			F(\rho)$, quindi il supporto di $\pi$ è contenuto nelle misure
			$\rho$ estremali per $F$.
		\qed

	\end{itemize}
\end{frame}



\section{Equazione di Campo Medio (ECM)}



\begin{frame}{Equazione di Campo Medio}
	Sia adesso $\rho$ un minimo (se $\beta > 0$) o massimo (se $\beta < 0$)
	del funzionale $F$. 
	\uncover<2->{
		\begin{block}{Teorema}
			$\rho$ soddisfa l'equazione di
			Eulero-Lagrange associata a $F$, detta \textbf{equazione di campo medio
			(ECM):}
			$$\tikz[baseline]{\node[draw=red,fill=red!20,anchor=base, rounded
			corners,rectangle,minimum height=1cm, inner sep = 5pt] 
			(d1) {$-\Delta U(x) = \rho(x) = \frac{\exp{(-\beta U(x))}}{\int_\Omega
			\exp{(-\beta U(y))}\d y}$}}$$
			dove $U(x) = \int_\Omega V(x, y)\rho(y)\d y$.
		\end{block}
	}
	
	\begin{itemize}
		\item<3->{L'equazione definisce in forma implicita $\rho$ ed è non
			lineare. Il termine di destra dipende da $\rho$ tramite $U$. L'equazione si
			può vedere anche come equazione ellittica in termini della $U$.}
		\item<4->{Di questa equazione abbiamo già dimostrato l'esistenza di
			soluzioni attraverso le misure di Gibbs.}
	\end{itemize}
\end{frame}



\begin{frame}{Osservazioni conclusive}
	\begin{itemize}
		\item Abbiamo detto che le misure nel supporto di $\pi$ sono punti
			estremali del funzionale $F$ e che quindi soddisfano la ECM,
			quindi se dimostriamo l'unicità degli estremali per $F$ o delle
			soluzioni per la ECM, avremmo che il supporto di $\pi$ è banale,
			quindi effettivamente $\pi = \delta_\rho$.
		\item<2-> Per $\beta > 0$, abbiamo unicità degli estremali per $F$,
			essendo un funzionale strettamente convesso.
		\item<3-> Per $\beta < 0$, l'unicità deriva dall'unicità per la ECM che
			è vera se $\Omega$ è semplicemente connesso, ma non la discutiamo 
			(è studiata in \cite{suzuki}).
	\end{itemize}
\end{frame}



\begin{frame}{Osservazioni conclusive}
	\begin{itemize}
		\item Ricordiamo che l'equazione di Eulero è:
			\[
				\cancel{\frac{\partial \omega}{\partial t}} + u\cdot \nabla \omega =
				0
			\]

		\uncover<2->{
			Se chiamiamo $\omega(x) = \rho(x)$ e $\psi(x) = U(x)$,
			otteniamo che
			\[
				\omega(x) = c(\psi)f(\psi(x))
			\]
			con $f(y) = e^{-\beta y}$.
		}

		\uncover<3->{
			Quindi
			\[
				u\cdot \nabla \omega = \nabla^\perp \psi \cdot c(\psi)
				f'(\psi) \nabla \psi = 0
			\]
			per cui $(\rho, U)$ sono una soluzione dell'equazione di Eulero.
		}
	\end{itemize}
\end{frame}



\begin{frame}{Un esempio}

	\onslide<1-3>{
		Le soluzioni più interessanti si ottengono per $\beta < 0$, dove si osserva
		la concentrazione della vorticità attorno ad alcuni punti.
	}

	\uncover<2->{
		Se il dominio $\Omega = B(0,1)$, allora la soluzione $\rho$ della (ECM)
		si può calcolare esplicitamente e si ottiene che ha simmetria radiale:
		\[
			\rho(r) = \frac{1 + A}{\pi}\frac{1}{(1 + Ar^2)^2} \qquad A =
			\frac{-\beta}{8\pi + \beta}, \beta < 0
		\]
	}
	\invisible<1-2>{
		\begin{figure}[!h]
		  	\centering
		  	\begin{minipage}[b]{0.475\textwidth}
				\includegraphics[width=\textwidth]{img/rho_2d.pdf}
		  	\end{minipage}
		  	\begin{minipage}[b]{0.475\textwidth}
				\includegraphics[width=\textwidth]{img/rho_3d.pdf}
		  	\end{minipage}
		\end{figure}
	}
\end{frame}



\begin{frame}[allowframebreaks]{Bibliografia}
	\nocite{*}
	\printbibliography
\end{frame}



\section{Supplementi}



\begin{frame}{Limite delle misure empiriche}
Vale il seguente risultato:
\begin{block}{Teorema}
	Sia $\rho(x)$ la soluzione dell'equazione di campo medio per il parametro
	$\beta \neq 0$. Allora per ogni funzione continua e limitata $\phi \in
	\mathcal{C}_b(\Omega)$ vale che:
	\[
		\lim_{n \to +\infty} \int_{\Omega^n} \d \mu_\beta^N 
		\left|\frac{1}{n}\sum_{i = 1}^n \phi(x_i) -
		\int_{\Omega}\phi(x)\rho(x)\d x\right|^2 = 0
	\]
\end{block}
\end{frame}



\begin{frame}{Concentrazione delle soluzioni}
	Nel caso in cui il dominio $\Omega$ è una palla, le soluzioni sono esplicite
	ed in particolare si osserva che $\rho_\beta$ tende a $\delta_0$ per $\beta$
	che tende a $-8\pi$. 

	Per quali altri domini $\Omega$ si ha questo fenomeno di
	\textbf{concentrazione}?
	
	\uncover<2->{
		\begin{block}{Definizione}
			Dato un dominio $\Omega$ e $\beta \in [-8\pi, 0]$, definiamo
			\[
				I_\beta(\Omega) = \sup_\rho \left\{F_\beta(\rho) +
				\frac{1}{\beta}\log{|\Omega|}\right\}
			\]
		\end{block}
	}
	
	\begin{itemize}
		\item<3-> Il termine $\beta^{-1}\log{|\Omega|}$ rende invariante
			$I_\beta(\Omega)$ per riscalamento ($\Omega \mapsto \alpha \Omega$).
		\item<4-> Per $\beta > -8\pi$ sappiamo già che il $\sup$ è un massimo ed
			è assunto nell'unica soluzione $\rho_\beta$ della ECM.
	\end{itemize}
\end{frame}



\begin{frame}{Concentrazione delle soluzioni}
	Indichiamo con $B$ la palla di raggio 1. Vale il seguente:
	\begin{block}{Teorema}
		Dato un qualunque dominio $\Omega$ semplicemente connesso vale che:
		\[
			I_{-8\pi}(\Omega) \geq I_{-8\pi}(B) + \max_{x \in \Omega}\gamma(x) +
			\frac{1}{8\pi}\log{\frac{|B|}{|\Omega|}}
		\]
		In particolare:
		\begin{itemize}
			\item Se la disuguaglianza è stretta diciamo che $\Omega$ è del
				\textbf{I tipo} e vale che $I_{-8\pi}(\Omega)$ è un massimo e
				non si ha concentrazione.
			\item Se vale l'uguaglianza diciamo che $\Omega$ è del \textbf{II
				tipo} e si ha concentrazione, ovvero $I_{-8\pi}(\Omega)$ non è
				un massimo e ogni successione massimizzante tende, a meno di
			sottosuccessione, a una $\delta_{x_0}$ per $x_0 \in \Omega$.
		\end{itemize}
	\end{block}
\end{frame}



\begin{frame}{Domande?}
	%\movie{Test}{img/point_vortices.avi}
	\begin{center}
		% \includegraphics[height = 0.8\textheight]{img/point_vortices.png}
		\embedvideo{\includegraphics[height = 0.8\textheight]{img/point_vortices.png}}{img/point_vortices.mp4}
	\end{center}
	\begin{center}
		\href{https://matteopalmieri.gitlab.io/tesi-triennale/slides/img/point_vortices.mp4}{Link
		to the simulation}
	\end{center}
\end{frame}

\end{document}
